# Contributing to MyMusicNiche

### Purpose
Making the codebase as if one person is writing the app

---

### Style Guide
Follow the [Android Kotlin Guide](https://android.github.io/kotlin-guides/style.html) or [Kotlin Style Guide](https://kotlinlang.org/docs/reference/coding-conventions.html)

[Configure](https://blog.jetbrains.com/kotlin/2018/01/kotlin-1-2-20-is-out/#more-5700) your project with style guide in Android Studio.

Remove auto-generated author comment
```java
/**
  * Created by juandelacruz on 11/10/14.
  */
class HttpResponse {
    ...
}
```

Optimize imports (Ctrl+Alt+O for Windows, Ctrl+Option+O for Mac)

```java
import com.mymusicniche.api.request.EventFeedsParams // Android Studio can detect unused import
import com.mymusicniche.api.request.PostDeleteParam  // <- unused import, remove it
import com.mymusicniche.api.response.EventResponse

interface EventSource {
    fun getEventFeeds(): Observable<EventResponse>
}
```

Use 4 spaces for indentation. Do not use tabs.
```java
    @GET("user/feed")
    fun getDiscussionFeeds(
        @Header("Authorization") auth: String,      // 4 spaces, correct
        @Query("page") page: Int
    ): Observable<DiscussionsResponse>
```

```java
    @GET("user/feed")
    fun getDiscussionFeeds(
            @Header("Authorization") auth: String,  // 8 spaces, incorrect
            @Query("page") page: Int
    ): Observable<DiscussionsResponse>
```

---

### Pull Request
Name your feature branch according to this format `author/feature`, like `juan/handle-expired-token`. The important thing to note is the author prefix `juan/` 

Pull requests **must have 1 commit**. Before sending PRs, `squash` your commits.


