package com.mymusicniche.api.response.type_adapter

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mymusicniche.api.response.ProfileResponse
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.io.File

class ProfileResponseDeserializerTest {

    private lateinit var gson: Gson

    @Before
    fun setUp() {
        gson = GsonBuilder().run {
            registerTypeAdapter(ProfileResponse::class.java, ProfileResponseDeserializer())
            setLenient()
            create()
        }
    }

    @Test
    fun Should_DeserializeProfileResponse_When_JsonHasNullProperties() {
        val path = javaClass.getResource("/ProfileResponse.json").path
        val reader = File(path).bufferedReader()

        val response = gson.fromJson(reader, ProfileResponse::class.java)

        assertEquals("5a5c8e570672cf2d5a4b2b70", response.user?.uid)
        assertEquals("Davion", response.user?.firstName)
        assertEquals("Cerio", response.user?.lastName)
        assertEquals("chipcerio", response.user?.screenName)
        assertEquals("vinz.cero@gmail.com", response.user?.email)

        assertEquals("", response.user?.website)
        assertEquals("", response.user?.about)
        assertEquals("", response.user?.birthday)
        assertEquals("", response.user?.venueType)

        assertEquals("yes", response.user?.acceptedTerms)
        assertEquals(207, response.user?.addressId)
        assertEquals("2018-01-15 11:19:51", response.user?.createdAt)
    }
}