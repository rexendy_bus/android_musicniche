package com.mymusicniche.api.response.type_adapter

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mymusicniche.api.response.DiscussionsResponse
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.io.File

class DiscussionResponseDeserializerTest {

    private lateinit var gson: Gson

    @Before
    fun setUp() {
        gson = GsonBuilder().run {
            registerTypeAdapter(DiscussionsResponse::class.java, DiscussionResponseDeserializer())
            setLenient()
            create()
        }
    }

    @Test
    fun test_DiscussionResponse_Deserializes_Successfully() {
        val path = javaClass.getResource("/DiscussionResponse.json").path
        val reader = File(path).bufferedReader()

        val response = gson.fromJson(reader, DiscussionsResponse::class.java)

        assertEquals(200, response.status)
        assertEquals(1, response.page)
    }
}