package com.mymusicniche.api.response.type_adapter

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mymusicniche.api.response.EventsResponse
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.io.File

class EventResponseDeserializerTest {

    private lateinit var gson: Gson

    @Before
    fun setUp() {
        gson = GsonBuilder().run {
            registerTypeAdapter(EventsResponse::class.java, EventResponseDeserializer())
            setLenient()
            create()
        }
    }

    @Test
    fun test_EventResponse_Deserializes_Successfully() {
        val path = javaClass.getResource("/EventResponse.json").path
        val reader = File(path).bufferedReader()

        val response = gson.fromJson(reader, EventsResponse::class.java)

        assertEquals(200, response.status)
        assertEquals(1, response.page)
    }
}