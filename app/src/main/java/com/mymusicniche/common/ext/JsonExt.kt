package com.mymusicniche.common.ext

import com.google.gson.JsonObject
import org.json.JSONArray

fun JsonObject.getValuesOrElse(key: String): String {
    if (get(key) == null) return ""

    val jsonArray = JSONArray()
    if (get(key).isJsonNull) return jsonArray.toString()

    return if (get(key).isJsonArray) {
        val arr = get(key).asJsonArray
        for (value in arr) jsonArray.put(value)
        jsonArray.toString()
    } else jsonArray.toString()
}

inline fun <reified T> JsonObject.getOrElse(key: String): T {
    if (has(key)) {
        return when (T::class) {
            String::class -> {
                if (!get(key).isJsonNull) get(key).asString as T else "" as T
            }

            Int::class -> {
                if (!get(key).isJsonNull) get(key).asInt as T else -1 as T
            }

            Double::class -> {
                if (!get(key).isJsonNull) get(key).asDouble as T else -1f as T
            }

            Boolean::class -> {
                if (!get(key).isJsonNull) get(key).asBoolean as T else false as T
            }

            else -> Any() as T
        }
    } else {
        return Any() as T
    }
}