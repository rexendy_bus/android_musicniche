package com.mymusicniche.common.ext

fun String.getFileExtension(path: String): String {
    return path.substring(path.lastIndexOf("."))
}

fun String.toBoolean(): Boolean {
    return when {
        this.equals(other = "YES", ignoreCase = true) -> true
        this.equals(other = "NO", ignoreCase = true) -> false
        this.equals(other = "0", ignoreCase = true) -> false
        this.equals(other = "1", ignoreCase = true) -> true
        this.equals(other = "-1", ignoreCase = true) -> false
        else -> false
    }

}