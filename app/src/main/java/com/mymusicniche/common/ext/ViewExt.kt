package com.mymusicniche.common.ext

import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.mymusicniche.config.AppGlide

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.disable() {
    this.isEnabled = false
}

fun View.enable() {
    this.isEnabled = true
}

fun AppCompatActivity.toast(msg: String) {
    runOnUiThread({
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    })
}

fun ImageView.load(path: String) {
    AppGlide.with(this)
        .load(path)
        .into(this)
}