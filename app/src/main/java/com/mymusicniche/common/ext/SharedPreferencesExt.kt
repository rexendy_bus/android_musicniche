package com.mymusicniche.common.ext

import android.content.SharedPreferences


fun SharedPreferences.put(key: String, value: String) {
    this.edit().putString(key, value).apply()
}

// https://discuss.kotlinlang.org/t/checking-type-in-generic/3100
inline fun <reified T> SharedPreferences.get(key: String): T {
    return when (T::class) {
        String::class  -> this.getString(key, "") as T
        Int::class     -> this.getInt(key, -1) as T
        Boolean::class -> this.getBoolean(key, false) as T
        else           -> Any() as T
    }
}