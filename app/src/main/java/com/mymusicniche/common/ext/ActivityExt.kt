package com.mymusicniche.common.ext

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity


inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
    val transaction = beginTransaction()
    transaction.func()
    transaction.commit()
}

fun AppCompatActivity.add(containerId: Int, fragment: Fragment) {
    supportFragmentManager.inTransaction {
        add(containerId, fragment)
    }
}

fun AppCompatActivity.replace(containerId: Int, fragment: Fragment) {
    supportFragmentManager.inTransaction {
        replace(containerId, fragment)
    }
}