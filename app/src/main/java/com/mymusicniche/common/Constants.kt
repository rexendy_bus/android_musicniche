package com.mymusicniche.common

import com.amazonaws.regions.Regions

object Constants {

    object Aws {
        const val PHOTO_BUCKET = "musicniche"
        const val VIDEO_BUCKET = "mymusicnichestreamin-hosting-mobilehub-675334505"
        val BUCKET_REGION = Regions.US_EAST_1
        const val METADATA_MEDIA_TYPE = "media-type"
        const val METADATA_MEDIA_TYPE_VALUE = "primary"
        const val METADATA_USER_ID = "user-id"
    }

    object Api {
        const val BASE_URL = "https://api.mymusicniche.com/"
    }

    object Key {
        const val TOKEN_TYPE = "token_type"
        const val ACCESS_TOKEN = "access_token"
        const val REFRESH_TOKEN = "refresh_token"
        const val USER_UUID = "user_uuid"
        const val IS_CURRENT_USER = "is_current_user"
    }

    object LoginType {
        const val FACEBOOK = "facebook"
        const val GPLUS = "google_plus"
        const val TWITTER = "twitter"
    }

    object Url {
        const val TOS = "https://www.mymusicniche.com/term-of-use"
    }

    object Database {
        const val NAME = "mymusicniche_db"
        const val VERSION = 1
    }
}