package com.mymusicniche.common.base

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject


abstract class BaseViewModel {

    var loadingIndicator = BehaviorSubject.createDefault(false)

    abstract fun getLoadingIndicator(): Observable<Boolean>

}