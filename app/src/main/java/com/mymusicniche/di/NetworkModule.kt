package com.mymusicniche.di

import android.content.Context
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Region
import com.amazonaws.services.s3.AmazonS3Client
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mymusicniche.BuildConfig
import com.mymusicniche.api.ApiService
import com.mymusicniche.api.response.DiscussionsResponse
import com.mymusicniche.api.response.EventsResponse
import com.mymusicniche.api.response.ProfileResponse
import com.mymusicniche.api.response.type_adapter.DiscussionResponseDeserializer
import com.mymusicniche.api.response.type_adapter.EventResponseDeserializer
import com.mymusicniche.api.response.type_adapter.ProfileResponseDeserializer
import com.mymusicniche.api.response.type_adapter.UserFeedResponseDeserializer
import com.mymusicniche.common.Constants
import com.mymusicniche.common.Constants.Api.BASE_URL
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun providesOkHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.NONE
        return OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()
    }

    @Provides
    @Singleton
    fun providesGson(): Gson {
        return GsonBuilder()
            .registerTypeAdapter(ProfileResponse::class.java, ProfileResponseDeserializer())
            .registerTypeAdapter(EventsResponse::class.java, EventResponseDeserializer())
            .registerTypeAdapter(DiscussionsResponse::class.java, DiscussionResponseDeserializer())
            .registerTypeAdapter(UserFeedResponseDeserializer::class.java, UserFeedResponseDeserializer())
            .setLenient()
            .create()
    }

    @Provides
    @Singleton
    fun providesRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun providesApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun providesAmazonClient(): AmazonS3Client {
        val creds = BasicAWSCredentials(BuildConfig.ACCESS_KEY, BuildConfig.SECRET_KEY)
        val s3client = AmazonS3Client(creds)
        s3client.setRegion(Region.getRegion(Constants.Aws.BUCKET_REGION))
        return s3client
    }

    @Provides
    @Singleton
    fun providesTransferUtility(amazon: AmazonS3Client, context: Context): TransferUtility {
        return TransferUtility.builder().s3Client(amazon).context(context).build()
    }
}