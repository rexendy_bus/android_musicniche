package com.mymusicniche.di

import com.mymusicniche.features.create_profile.CreateProfileActivity
import com.mymusicniche.features.create_profile.CreateProfileRepositoryModule
import com.mymusicniche.features.create_profile.genre.GenreActivity
import com.mymusicniche.features.create_profile.genre.GenreRepositoryModule
import com.mymusicniche.features.create_profile.influencers.InfluencersActivity
import com.mymusicniche.features.create_profile.influencers.InfluencersRepositoryModule
import com.mymusicniche.features.create_profile.skills.SkillsActivity
import com.mymusicniche.features.create_profile.skills.SkillsRepositoryModule
import com.mymusicniche.features.feeds.DiscussionsFragmentProvider
import com.mymusicniche.features.feeds.EventsFragmentProvider
import com.mymusicniche.features.feeds.HomeActivity
import com.mymusicniche.features.feeds.HomeFragmentProvider
import com.mymusicniche.features.login.LoginActivity
import com.mymusicniche.features.login.LoginRepositoryModule
import com.mymusicniche.features.profile.Portfolio.PortfolioFragmentProvider
import com.mymusicniche.features.profile.Post.PostFragmentProvider
import com.mymusicniche.features.profile.ProfileFragmentProvider

import com.mymusicniche.features.profile.View.UserProfileViewActivity
import com.mymusicniche.features.profile.View.UserProfileViewFragmentProvider

import com.mymusicniche.features.profile.Setting.ProfileSetting.ProfileSettingActivity
import com.mymusicniche.features.profile.Setting.ProfileSetting.ProfileSettingFragmentProvider

import com.mymusicniche.features.tos.TOSActivity
import com.mymusicniche.features.tos.TOSRepositoryModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(GenreRepositoryModule::class)])
    abstract fun bindGenreActivity(): GenreActivity

    @ContributesAndroidInjector(modules = [(InfluencersRepositoryModule::class)])
    abstract fun bindInfluencersActivity(): InfluencersActivity

    @ContributesAndroidInjector(modules = [(SkillsRepositoryModule::class)])
    abstract fun bindSkillsActivity(): SkillsActivity

    @ContributesAndroidInjector(modules = [(CreateProfileRepositoryModule::class)])
    abstract fun bindCreateProfileActivity(): CreateProfileActivity

    @ContributesAndroidInjector(modules = [(LoginRepositoryModule::class)])
    abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [(TOSRepositoryModule::class)])
    abstract fun bindTosActivity(): TOSActivity

    @ContributesAndroidInjector(modules = [
        HomeFragmentProvider::class,
        EventsFragmentProvider::class,
        DiscussionsFragmentProvider::class, ProfileFragmentProvider::class,
        PortfolioFragmentProvider::class, PostFragmentProvider::class,
        DiscussionsFragmentProvider::class,
        LoginRepositoryModule::class])
    abstract fun bindHomeActivity(): HomeActivity

    @ContributesAndroidInjector(modules = [UserProfileViewFragmentProvider::class,
        PortfolioFragmentProvider::class, PostFragmentProvider::class])
    abstract fun bindUserProfileViewActivity(): UserProfileViewActivity

    @ContributesAndroidInjector(modules = [ProfileSettingFragmentProvider::class])
    abstract fun bindProfileSettingActivity(): ProfileSettingActivity
}