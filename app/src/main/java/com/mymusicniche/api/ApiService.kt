package com.mymusicniche.api

import com.mymusicniche.api.response.*
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*
import java.util.*

interface ApiService {

    @FormUrlEncoded
    @POST("oauth/token")
    fun login(
        @Field("grant_type") grantType: String,
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("login_type") loginType: String,
        @Field("first_name") firstName: String,
        @Field("last_name") lastName: String,
        @Field("scope") scope: String,
        @Field("platform") platform: String,
        @Field("player_id") playerId: String,
        @Field("device_name") deviceName: String
    ): Observable<LoginResponse>

    @POST("user/accept_terms")
    fun acceptTerms(
        @Header("Authorization") auth: String
    ): Observable<BaseResponse>

    @FormUrlEncoded
    @POST("oauth/token")
    fun refresh(
        @Field("grant_type") grantType: String,
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("refresh_token") refreshToken: String,
        @Field("scope") scope: String
    ): Observable<LoginResponse>

    @GET("skills")
    fun getSkills(
        @Header("Authorization") auth: String
    ): Observable<SkillsResponse>

    @GET("genres")
    fun getGenres(
        @Header("Authorization") auth: String
    ): Observable<GenresResponse>

    @FormUrlEncoded
    @POST("user/update")
    fun userUpdate(
        @Header("Authorization") auth: String,
        @FieldMap map: MutableMap<String, Any>
    ): Single<BaseResponse>

    @GET("user/me")
    fun getProfile(
        @Header("Authorization") auth: String
    ): Observable<ProfileResponse>

    @GET("search/events")
    fun getEventFeeds(
        @Header("Authorization") auth: String,
        @QueryMap params: MutableMap<String, Number>
    ): Observable<EventsResponse>

    @GET("search/discussions")
    fun getDiscussionFeeds(
        @Header("Authorization") auth: String,
        @QueryMap params: MutableMap<String, Number>
    ): Observable<DiscussionsResponse>

    @GET("user/feed")
    fun getDiscussionFeeds(
        @Header("Authorization") auth: String,
        @Query("page") page: Int
    ): Observable<DiscussionsResponse>

    @GET("user/feed")
    fun getUserFeeds(
            @Header("Authorization") auth: String,
            @Query("page") page: Int,
            @Query("perPage") perPage: Int?,
            @Query("user_id") user_id: String?
    ): Observable<UserFeedResponse>


    @FormUrlEncoded
    @POST("posts/delete")
    fun postDelete(
            @Header("Authorization") auth: String,
            @Field("id") id: Int
    ): Observable<PostResponse>

    @GET("user/{user_id}")
    fun getUserProfile(
            @Header("Authorization") auth: String,
            @Path("user_id") user_id: String
    ): Observable<ProfileResponse>

    @FormUrlEncoded
    @POST("user/notifications")
    fun updateNotif(
            @Header("Authorization") auth: String,
            @FieldMap map: MutableMap<String, Any>
    ):Observable<BaseResponse>

    @FormUrlEncoded
    @POST("user/update_privacy")
    fun updatePrivacy(
            @Header("Authorization") auth: String,
            @FieldMap map: MutableMap<String, Any>
    ):Observable<BaseResponse>
}