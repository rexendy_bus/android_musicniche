package com.mymusicniche.api.request

/**
 * Created by rexenjeandy on 3/16/18.
 */
class PostDeleteParam(
        val id: Int
)