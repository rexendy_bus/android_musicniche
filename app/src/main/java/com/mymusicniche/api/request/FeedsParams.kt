package com.mymusicniche.api.request

class FeedsParams(val latitude: Double, val longitude: Double, val page: Int)