package com.mymusicniche.api

open class InvalidRefreshTokenException(msg: String) : Exception(msg)