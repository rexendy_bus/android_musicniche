package com.mymusicniche.api.response

class AuthErrorResponse (val error: String, val status: Int)