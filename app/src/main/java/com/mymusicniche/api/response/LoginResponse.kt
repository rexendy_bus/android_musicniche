package com.mymusicniche.api.response


class LoginResponse (
        val token_type: String,
        val expires_in: Int,
        val access_token: String,
        val refresh_token: String
) { constructor() : this(token_type = "", expires_in = 0, access_token = "", refresh_token = "") }