package com.mymusicniche.api.response

import com.mymusicniche.data.Genre


class GenresResponse (val status: Int, val results: List<Genre>)
