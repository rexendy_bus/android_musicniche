package com.mymusicniche.api.response

import com.mymusicniche.data.poko.UserFeed


/**
 * Created by rexenjeandy on 3/12/18.
 */
class UserFeedResponse(
        val status: Int,
        val page: Int,
        val perPage: Int,
        val results: List<UserFeed>
)