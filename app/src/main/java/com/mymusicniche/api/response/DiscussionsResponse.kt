package com.mymusicniche.api.response

import com.mymusicniche.data.poko.Discussion

class DiscussionsResponse (
    val status: Int,
    val results: List<Discussion>,
    val page: Int
)