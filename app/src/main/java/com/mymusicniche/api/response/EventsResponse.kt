package com.mymusicniche.api.response

import com.mymusicniche.data.poko.Event

data class EventsResponse (
    val status: Int,
    val results: List<Event>,
    val page: Int
)