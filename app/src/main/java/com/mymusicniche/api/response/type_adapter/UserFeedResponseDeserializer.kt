package com.mymusicniche.api.response.type_adapter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.mymusicniche.api.response.UserFeedResponse
import com.mymusicniche.common.ext.getOrElse
import com.mymusicniche.data.AvatarSource
import com.mymusicniche.data.ProfileAvatar
import com.mymusicniche.data.UserLocation
import com.mymusicniche.data.poko.*
import java.lang.reflect.Type

/**
 * Created by rexenjeandy on 3/12/18.
 */
class UserFeedResponseDeserializer : JsonDeserializer<UserFeedResponse> {

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): UserFeedResponse {
        val root = json.asJsonObject
        val status = root.get("status").asInt
        val page = root.get("page").asInt

        val posts = mutableListOf<UserFeed>()
        val postsJsonArray = root.get("results").asJsonArray
        postsJsonArray.forEach {
            // user
            val userJson = it.asJsonObject.get("user").asJsonObject
            val user = UserResult(
                    uid = userJson.getOrElse("uid"),
                    firstName = userJson.getOrElse("first_name"),
                    lastName = userJson.getOrElse("last_name"),
                    screenName = userJson.getOrElse("screen_name"),
                    email = userJson.getOrElse("email")
            )

            // location
            val locationJson = it.asJsonObject.get("location").asJsonObject
            val location = Location(
                    id = locationJson.getOrElse("id"),
                    address = locationJson.getOrElse("address"),
                    address2 = locationJson.getOrElse("address2"),
                    city = locationJson.getOrElse("city"),
                    state = locationJson.getOrElse("state"),
                    zip = locationJson.getOrElse("zip"),
                    fullAddress = locationJson.getOrElse("full_address"),
                    latitude = locationJson.getOrElse("latitude"),
                    longitude = locationJson.getOrElse("longitude")
            )

            // status
            val statusJson = it.asJsonObject.get("status").asJsonObject
            val status = PostStatus(
                    name = statusJson.getOrElse("name")
            )

            val coverJson = it.asJsonObject.get("cover").asJsonObject
            val id = coverJson.asJsonObject.getOrElse<Int>("id")
            val type = coverJson.asJsonObject.getOrElse<String>("type")
            val sourceDetail = coverJson.asJsonObject.getAsJsonObject("source")

            val avatarSource = AvatarSource()
            avatarSource.original = sourceDetail.getOrElse<String>("original")
            avatarSource.large = sourceDetail.getOrElse<String>("large")
            avatarSource.medium = sourceDetail.getOrElse<String>("medium")
            avatarSource.small = sourceDetail.getOrElse<String>("small")

            val cover = ProfileAvatar()
            cover.id = id
            cover.type = type
            cover.source = avatarSource

            // post
            val postJson = it.asJsonObject
            val post = UserFeed(
                    id = postJson.getOrElse("id"),
                    content = postJson.getOrElse("content"),
                    eventDate = postJson.getOrElse("event_date"),
                    createdAt = postJson.getOrElse("created_at"),
                    user = user,
                    location = location,
                    status = status,
                    cover = cover
            )

            posts.add(post)
        }

        return UserFeedResponse(status, page, 20, posts)
    }
}