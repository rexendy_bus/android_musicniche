package com.mymusicniche.api.response.type_adapter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.mymusicniche.api.response.ProfileResponse
import com.mymusicniche.common.ext.getOrElse
import com.mymusicniche.data.*
import java.lang.reflect.Type

class ProfileResponseDeserializer : JsonDeserializer<ProfileResponse> {

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): ProfileResponse {
        val root = json.asJsonObject
        val status = root.getOrElse<Int>("status")

        var isPrivate = false
        var message = ""

        if (root.has("isPrivate")) {
            isPrivate = root.getOrElse<Boolean>("isPrivate")
        }

        if (root.has("message")) {
            message = root.getOrElse<String>("message")
        }

        val userJson = root.get("results").asJsonObject

        val avatarList = userJson.asJsonObject.getAsJsonArray("avatar")
        val avatars = mutableListOf<ProfileAvatar>()

        avatarList.forEach{
            val id = it.asJsonObject.getOrElse<Int>("id")
            val type = it.asJsonObject.getOrElse<String>("type")
            val sourceDetail = it.asJsonObject.getAsJsonObject("source")

            val avatarSource = AvatarSource()
            avatarSource.original = sourceDetail.getOrElse<String>("original")
            avatarSource.large = sourceDetail.getOrElse<String>("large")
            avatarSource.medium = sourceDetail.getOrElse<String>("medium")
            avatarSource.small = sourceDetail.getOrElse<String>("small")

            val avatar = ProfileAvatar()
            avatar.id = id
            avatar.type = type
            avatar.source = avatarSource
            avatars.add(avatar)
        }

        val bgList = userJson.asJsonObject.getAsJsonArray("background")
        val bgPhotos = mutableListOf<ProfileAvatar>()

        bgList.forEach{
            val id = it.asJsonObject.getOrElse<Int>("id")
            val type = it.asJsonObject.getOrElse<String>("type")
            val sourceDetail = it.asJsonObject.getAsJsonObject("source")

            val avatarSource = AvatarSource()
            avatarSource.original = sourceDetail.getOrElse<String>("original")
            avatarSource.large = sourceDetail.getOrElse<String>("large")
            avatarSource.medium = sourceDetail.getOrElse<String>("medium")
            avatarSource.small = sourceDetail.getOrElse<String>("small")

            val avatar = ProfileAvatar()
            avatar.id = id
            avatar.type = type
            avatar.source = avatarSource
            bgPhotos.add(avatar)
        }

        val portfolioList = userJson.asJsonObject.getAsJsonArray("portfolios")
        val portfolios = mutableListOf<ProfileAvatar>()

        portfolioList.forEach{
            val id = it.asJsonObject.getOrElse<Int>("id")
            val type = it.asJsonObject.getOrElse<String>("type")
            val sourceDetail = it.asJsonObject.getAsJsonObject("source")

            val avatarSource = AvatarSource()
            if (type.equals("portfolio")) {
                avatarSource.original = sourceDetail.getOrElse<String>("original")
                avatarSource.large = sourceDetail.getOrElse<String>("large")
                avatarSource.medium = sourceDetail.getOrElse<String>("medium")
                avatarSource.small = sourceDetail.getOrElse<String>("small")
            } else {
                avatarSource.playlist = sourceDetail.getOrElse<String>("playlist")
            }

            val avatar = ProfileAvatar()
            avatar.id = id
            avatar.type = type
            avatar.source = avatarSource
            portfolios.add(avatar)
        }

        var locationJson: JsonObject?
        var location: UserLocation? = null
        if (userJson.has("address") && !userJson.get("address").isJsonNull) {
            // location
            locationJson = userJson.asJsonObject.get("address").asJsonObject
            location = UserLocation(
                    id = locationJson.getOrElse("id"),
                    address = locationJson.getOrElse("address"),
                    address2 = locationJson.getOrElse("address2"),
                    city = locationJson.getOrElse("city"),
                    state = locationJson.getOrElse("state"),
                    zip = locationJson.getOrElse("zip"),
                    fullAddress = locationJson.getOrElse("full_address"),
                    latitude = locationJson.getOrElse("latitude"),
                    longitude = locationJson.getOrElse("longitude")
            )
        }



        val skillList = userJson.asJsonObject.getAsJsonArray("skills")
        val skills = mutableListOf<UserSkill>()

        skillList.forEach{
            val instrument = it.asJsonObject.getOrElse<String>("instrument")
            val title = it.asJsonObject.getOrElse<String>("title")
            val category = it.asJsonObject.getOrElse<String>("category")

            val skill = UserSkill(
                    title = title,
                    instrument = instrument,
                    category = category
            )
            skills.add(skill)
        }


        val genreList = userJson.asJsonObject.getAsJsonArray("genres")
        val genres = mutableListOf<UserGenre>()

        genreList.forEach{
            val genre = it.asJsonObject.getOrElse<String>("genre")
            val category = it.asJsonObject.getOrElse<String>("category")

            val data = UserGenre(
                    genre = genre,
                    category = category
            )
            genres.add(data)
        }


        val inluencesList = userJson.asJsonObject.getAsJsonArray("influences")
        val influences = mutableListOf<UserInfluences>()

        inluencesList.forEach{
            val name = it.asJsonObject.getOrElse<String>("name")
            val type = it.asJsonObject.getOrElse<String>("type")

            val data = UserInfluences(
                    name = name,
                    type = type
            )
            influences.add(data)
        }


        // category
        var categoryJson: JsonObject?
        var category: Category? = null
        if (userJson.has("category") && !userJson.get("category").isJsonNull) {
            categoryJson = userJson.asJsonObject.get("category").asJsonObject
            category = Category(
                    id = 0,
                    name = categoryJson.getOrElse("name"),
                    description = categoryJson.getOrElse("description")
            )
        }


        // phone
        var phoneJson: JsonObject?
        var phone: UserPhone? = null
        if (userJson.has("phone") && !userJson.get("phone").isJsonNull) {
            phoneJson = userJson.asJsonObject.get("phone").asJsonObject
            phone = UserPhone(
                    id = phoneJson.getOrElse("id"),
                    number = phoneJson.getOrElse("number")
            )
        }


        var notifList= mutableListOf<String>()
        var notificationTypes = userJson.getAsJsonArray("notification_types")

        notificationTypes.forEach{
            notifList.add(it.asString)
        }

        val user = User(
            _id = 0L,
            uid = userJson.getOrElse("uid"),
            firstName = userJson.getOrElse("first_name"),
            lastName = userJson.getOrElse("last_name"),
            screenName = userJson.getOrElse("screen_name"),
            email = userJson.getOrElse("email"),
            website = userJson.getOrElse("website"),
            about = userJson.getOrElse("about"),
            venueType = userJson.getOrElse("venue_type"),
            birthday = userJson.getOrElse("birthday"),
            acceptedTerms = userJson.getOrElse("accepted_terms"),
            addressId = userJson.getOrElse("address_id"),
            createdAt = userJson.getOrElse("created_at"),
                avatars = avatars,
                backgrounds = bgPhotos,
                portfolios = portfolios,
                address = location,
                skills = skills,
                genres = genres,
                influences = influences,
                category = category,
                phone = phone,
                privacy = userJson.getOrElse("privacy"),
                notificationTypes = notifList
//            updatedAt = userJson.getOrElse("updated_at")
            /*
            address = userJson.getOrElse("address"),
            phone = userJson.getOrElse("phone"),
            category = userJson.getOrElse("category"),
            skills = userJson.getValuesOrElse("skills"),
            genres = userJson.getValuesOrElse("genres"),
            influences = userJson.getValuesOrElse("influences"),
            avatars = userJson.getValuesOrElse("avatar"),
            portfolios = userJson.getValuesOrElse("portfolios"),
            backgrounds = userJson.getValuesOrElse("background")
            */
        )

        return ProfileResponse(status, user, isPrivate, message)
    }
}