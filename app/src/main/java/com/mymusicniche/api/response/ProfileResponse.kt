package com.mymusicniche.api.response

import com.google.gson.annotations.SerializedName
import com.mymusicniche.data.User

class ProfileResponse (
    val status: Int,

    @SerializedName("results")
    val user: User?,

    val isPrivate: Boolean,

    val message: String
)