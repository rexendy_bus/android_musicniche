package com.mymusicniche.api.response

import com.mymusicniche.data.Skill

class SkillsResponse : BaseResponse() {
    lateinit var results: List<Skill>
}