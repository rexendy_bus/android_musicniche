package com.mymusicniche.api.response.type_adapter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.mymusicniche.api.response.DiscussionsResponse
import com.mymusicniche.common.ext.getOrElse
import com.mymusicniche.data.poko.Comment
import com.mymusicniche.data.poko.Discussion
import com.mymusicniche.data.poko.Location
import com.mymusicniche.data.poko.User
import java.lang.reflect.Type

class DiscussionResponseDeserializer : JsonDeserializer<DiscussionsResponse> {

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): DiscussionsResponse {
        val root = json.asJsonObject
        val status = root.get("status").asInt
        val page = root.get("page").asInt

        val discussions = mutableListOf<Discussion>()
        val eventsJsonArray = root.get("results").asJsonArray
        eventsJsonArray.forEach {
            // comment
            val comments = mutableListOf<Comment>()
            val commentsJsonArray = it.asJsonObject.getAsJsonArray("comments")
            commentsJsonArray.forEach {
                val id = it.asJsonObject.getOrElse<Int>("id")
                val content = it.asJsonObject.getOrElse<String>("content")
                val createdAt = it.asJsonObject.getOrElse<String>("created_at")

                val commentsUserJson = it.asJsonObject.get("user").asJsonObject
                val user = User(
                    uid = commentsUserJson.getOrElse("uid"),
                    firstName = commentsUserJson.getOrElse("first_name"),
                    lastName = commentsUserJson.getOrElse("last_name"),
                    screenName = commentsUserJson.getOrElse("screen_name")
                )

                val comment = Comment(id, content, createdAt, user)
                comments.add(comment)
            }

            // user
            val userJson = it.asJsonObject.get("user").asJsonObject
            val user = User(
                uid = userJson.getOrElse("uid"),
                firstName = userJson.getOrElse("first_name"),
                lastName = userJson.getOrElse("last_name"),
                screenName = userJson.getOrElse("screen_name")
            )

            // location
            val locationJson = it.asJsonObject.get("location").asJsonObject
            val location = Location(
                id = locationJson.getOrElse("id"),
                address = locationJson.getOrElse("address"),
                address2 = locationJson.getOrElse("address2"),
                city = locationJson.getOrElse("city"),
                state = locationJson.getOrElse("state"),
                zip = locationJson.getOrElse("zip"),
                fullAddress = locationJson.getOrElse("full_address"),
                latitude = locationJson.getOrElse("latitude"),
                longitude = locationJson.getOrElse("longitude")
            )

            // discussion
            val discussionJson = it.asJsonObject
            val discussion = Discussion(
                id = discussionJson.getOrElse("id"),
                content = discussionJson.getOrElse("content"),
                eventDate = discussionJson.getOrElse("event_date"),
                createdAt = discussionJson.getOrElse("created_at"),
                distance = discussionJson.getOrElse("distance"),
                comments = comments,
                user = user,
                location = location
            )

            discussions.add(discussion)
        }

        return DiscussionsResponse(status, discussions, page)
    }
}