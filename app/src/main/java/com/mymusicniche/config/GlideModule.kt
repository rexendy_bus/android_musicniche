package com.mymusicniche.config

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule(glideName = "AppGlide")
class GlideModule : AppGlideModule()