package com.mymusicniche

import com.jakewharton.threetenabp.AndroidThreeTen
import com.mymusicniche.di.DaggerAppComponent
import com.mymusicniche.features.notifications.NotificationReceivedHandler
import com.onesignal.OneSignal
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber

class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)

        OneSignal.startInit(this)
            .setNotificationReceivedHandler(NotificationReceivedHandler())
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            OneSignal.setLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.WARN)
        }
    }
}