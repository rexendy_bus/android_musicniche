package com.mymusicniche.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Influence(val title: String) : Parcelable