package com.mymusicniche.data.poko

class Category {

    var id: Int = 0
    var name: String? = null
    var description: String? = null
    var avatar: Int = 0
    var selected: Boolean = false

    enum class Type(val type: Int) {
        SOLO(1), GROUP(2), SPOT(3), JAMMER(4)
    }

    override fun toString(): String {
        return "id = $id name = $name description = $description avatar = $avatar selected = $selected"
    }
}