package com.mymusicniche.data.poko

data class Device(
    val platform: String,
    val playerId: String,
    val name: String
)