package com.mymusicniche.data.poko

import com.google.gson.annotations.SerializedName

class Event(
    val id: Int,

    val content: String,

    @SerializedName("event_date")
    val eventDate: String,

    @SerializedName("created_at")
    val createdAt: String,

    val distance: Double,

    val comments: List<Comment>,

    val user: User,

    // val skills: List<Skill>

    val location: Location

    // val status: Status
)

class Comment(
    val id: Int,

    val content: String,

    @SerializedName("created_at")
    val createdAt: String,

    val user: User
)

class User(
    val uid: String,

    @SerializedName("first_name")
    val firstName: String,

    @SerializedName("last_name")
    val lastName: String,

    @SerializedName("screen_name")
    val screenName: String
)

class Location(
    val id: Int,
    val address: String,
    val address2: String,
    val city: String,
    val state: String,
    val zip: String,

    @SerializedName("full_address")
    val fullAddress: String,

    val latitude: Double,
    val longitude: Double
)

