package com.mymusicniche.data.poko

import com.google.gson.annotations.SerializedName

class Discussion (
    val id: Int,

    val content: String,

    @SerializedName("event_date")
    val eventDate: String,

    @SerializedName("created_at")
    val createdAt: String,

    val distance: Double,

    val comments: List<Comment>,

    val user: User,

    // val skills: List<Skill>

    val location: Location

    // val status: Status
)