package com.mymusicniche.data.poko

import com.google.gson.annotations.SerializedName
import com.mymusicniche.data.ProfileAvatar
import com.mymusicniche.data.UserSkill

/**
 * Created by rexenjeandy on 3/16/18.
 */
class Post(
        val id: Int,

        val content: String,

        @SerializedName("media_id")
        val mediaId: Int,

        @SerializedName("media_hash")
        val mediaHash: String,

        @SerializedName("event_date")
        val eventDate: String,

        @SerializedName("created_at")
        val createdAt: String,

        @SerializedName("updated_at")
        val updatedAt: String,

        val comments: List<PostComment>,

        val user: PostUser,

        val location: Location,

        val skills: List<UserSkill>,

        val status: PostStatus,

        val cover: ProfileAvatar
)

class PostComment (
        val id: Int,

        val content: String,

        val created_at: String,

        val user: PostUser
)

class PostUser (
        val uid: String,

        @SerializedName("first_name")
        val firstName: String,

        @SerializedName("last_name")
        val lastName: String,

        @SerializedName("screen_name")
        val screenName: String,

        val email: String,

        val website: String,

        val about: String,

        @SerializedName("avatar")
        val avatars: List<ProfileAvatar>
)

class PostStatus (
        val name: String
)
