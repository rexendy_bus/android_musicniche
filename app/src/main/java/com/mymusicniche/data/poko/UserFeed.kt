package com.mymusicniche.data.poko

import com.google.gson.annotations.SerializedName
import com.mymusicniche.data.ProfileAvatar

/**
 * Created by rexenjeandy on 3/12/18.
 */

class UserFeed (
        val id: Int,

        val content: String,

        @SerializedName("event_date")
        val eventDate: String,

        @SerializedName("created_at")
        val createdAt: String,

        val user: UserResult,

        val location: Location,

        val status: PostStatus,

        val cover: ProfileAvatar
)

class UserResult(
        val uid: String,

        @SerializedName("first_name")
        val firstName: String,

        @SerializedName("last_name")
        val lastName: String,

        @SerializedName("screen_name")
        val screenName: String,

        val email: String
)