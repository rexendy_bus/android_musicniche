package com.mymusicniche.data.poko

class Credential(
    /**
     * Registered email from social media
     */
    val username: String,

    /**
     * fb_id, gplus_id, twitter_id
     */
    val password: String,
    val firstName: String,
    val lastName: String
)