package com.mymusicniche.data.source

import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.mymusicniche.api.response.LoginResponse
import com.mymusicniche.data.User
import com.mymusicniche.data.poko.Credential
import com.mymusicniche.data.poko.Device
import io.reactivex.Completable
import io.reactivex.Observable

interface LoginSource {

    fun login(credential: Credential, device: Device): Observable<LoginResponse>

    fun saveTokenType(tokenType: String)

    fun saveAccessToken(accessToken: String)

    fun saveRefreshToken(refreshToken: String)

    fun graphRequest(token: AccessToken, cb: GraphRequest.GraphJSONObjectCallback): Completable

    fun getProfile(): Observable<User>

    fun save(user: User)
}