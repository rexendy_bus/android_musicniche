package com.mymusicniche.data.source.remote

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.api.request.FeedsParams
import com.mymusicniche.api.response.DiscussionsResponse
import com.mymusicniche.common.Constants.Key.ACCESS_TOKEN
import com.mymusicniche.common.Constants.Key.TOKEN_TYPE
import com.mymusicniche.common.ext.get
import com.mymusicniche.data.source.DiscussionFeedsSource
import io.reactivex.Observable
import javax.inject.Inject

class DiscussionFeedsRemoteSource @Inject
constructor(private val api: ApiService, private val pref: SharedPreferences) : DiscussionFeedsSource {

    override fun getDiscussionFeeds(params: FeedsParams): Observable<DiscussionsResponse> {
        val auth = "${pref.get<String>(TOKEN_TYPE)} ${pref.get<String>(ACCESS_TOKEN)}"

        // ?latitude=10.3157&longitude=123.8854&page=1
        val queryMap = mutableMapOf<String, Number>().apply {
            put("latitude", params.latitude)
            put("longitude", params.longitude)
            put("page", params.page)
        }

        return api.getDiscussionFeeds(auth, queryMap)
    }
}