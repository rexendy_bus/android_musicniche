package com.mymusicniche.data.source.local

import android.content.SharedPreferences
import com.mymusicniche.data.Skill
import com.mymusicniche.data.source.InfluencerSource
import com.mymusicniche.data.source.SkillsSource
import io.reactivex.Observable
import javax.inject.Inject

class InfluencerLocalSource @Inject
constructor(val sharedPref: SharedPreferences) : InfluencerSource {

    override fun getInfluencers(): Observable<List<String>> {
        return Observable.empty()
    }

}