package com.mymusicniche.data.source

import com.mymusicniche.api.response.BaseResponse
import com.mymusicniche.api.response.ProfileResponse
import com.mymusicniche.data.User
import com.mymusicniche.data.poko.Category
import io.reactivex.Observable
import io.reactivex.Single

interface UserSource {

    fun updateUser(type: Category.Type,
                   email: String,
                   firstName: String,
                   lastName: String,
                   screenName: String,
                   website: String,
                   categoryId: Int,
                   address: String,
                   phone: String,
                   about: String,
                   genres: List<String>,
                   skills: List<String>,
                   influences: List<String>,
                   venueType: String): Single<BaseResponse>

    fun acceptTerms(): Single<BaseResponse>

    fun getProfile(): Observable<User>

    fun getOtherUserProfile(user_id: String): Observable<ProfileResponse>
}