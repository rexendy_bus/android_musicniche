package com.mymusicniche.data.source

import com.mymusicniche.api.response.BaseResponse
import com.mymusicniche.data.User
import io.reactivex.Observable
import io.reactivex.Single

interface ProfileSettingSource {

    fun updateNotification(switch: String, module: String?): Single<BaseResponse>

    fun updatePrivacy(visibility: String): Single<BaseResponse>

    fun getProfile(): Observable<User>
}