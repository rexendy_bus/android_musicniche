package com.mymusicniche.data.source.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.mymusicniche.data.User
import io.reactivex.Maybe

@Dao
interface UserDao {

    @Insert(onConflict = REPLACE)
    fun save(user: User)

    @Query("SELECT * FROM user WHERE uid = :uid LIMIT 1")
    fun getUser(uid: String): Maybe<User>
}