package com.mymusicniche.data.source.repository

import com.mymusicniche.api.response.BaseResponse
import com.mymusicniche.data.User
import com.mymusicniche.data.source.ProfileSettingSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class ProfileSettingRepository @Inject
constructor(@Remote private val remote: ProfileSettingSource, @Local private val local: ProfileSettingSource) : ProfileSettingSource{
    override fun getProfile(): Observable<User> {
        return remote.getProfile()
    }

    override fun updateNotification(switch: String, module: String?): Single<BaseResponse> {
        return remote.updateNotification(switch, module)
    }

    override fun updatePrivacy(visibility: String): Single<BaseResponse> {
        return remote.updatePrivacy(visibility)
    }

}