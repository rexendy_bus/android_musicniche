package com.mymusicniche.data.source

import com.mymusicniche.api.request.FeedsParams
import com.mymusicniche.api.response.DiscussionsResponse
import io.reactivex.Observable

interface DiscussionFeedsSource {

    fun getDiscussionFeeds(params: FeedsParams): Observable<DiscussionsResponse>
}