package com.mymusicniche.data.source.repository

import com.mymusicniche.data.Skill
import com.mymusicniche.data.source.SkillsSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import io.reactivex.Observable
import javax.inject.Inject

class SkillsRepository @Inject
constructor(@Remote private val remote: SkillsSource,
            @Local private val local: SkillsSource) : SkillsSource {


    override fun getSkills(): Observable<List<Skill>> {
        return remote.getSkills()
    }

}