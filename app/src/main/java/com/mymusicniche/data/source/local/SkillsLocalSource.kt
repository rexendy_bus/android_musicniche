package com.mymusicniche.data.source.local

import android.content.SharedPreferences
import com.mymusicniche.data.Skill
import com.mymusicniche.data.source.SkillsSource
import io.reactivex.Observable
import javax.inject.Inject

class SkillsLocalSource @Inject
constructor(val sharedPref: SharedPreferences): SkillsSource {

    override fun getSkills(): Observable<List<Skill>> {
        TODO("not implemented")
    }

}