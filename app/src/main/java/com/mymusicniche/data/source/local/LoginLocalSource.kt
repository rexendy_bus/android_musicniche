package com.mymusicniche.data.source.local

import android.content.SharedPreferences
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.mymusicniche.api.response.LoginResponse
import com.mymusicniche.common.Constants.Key.ACCESS_TOKEN
import com.mymusicniche.common.Constants.Key.REFRESH_TOKEN
import com.mymusicniche.common.Constants.Key.TOKEN_TYPE
import com.mymusicniche.common.ext.put
import com.mymusicniche.data.User
import com.mymusicniche.data.poko.Credential
import com.mymusicniche.data.poko.Device
import com.mymusicniche.data.source.LoginSource
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class LoginLocalSource @Inject
constructor(private val sharedPref: SharedPreferences, private val db: AppDatabase) : LoginSource {
    override fun graphRequest(token: AccessToken, cb: GraphRequest.GraphJSONObjectCallback): Completable {
        TODO("not implemented")
    }

    override fun login(credential: Credential, device: Device): Observable<LoginResponse> {
        TODO("not implemented")
    }

    override fun saveTokenType(tokenType: String) {
        sharedPref.put(TOKEN_TYPE, tokenType)
    }

    override fun saveAccessToken(accessToken: String) {
        sharedPref.put(ACCESS_TOKEN, accessToken)
    }

    override fun saveRefreshToken(refreshToken: String) {
        sharedPref.put(REFRESH_TOKEN, refreshToken)
    }

    override fun getProfile(): Observable<User> {
        TODO("not implemented")
    }

    override fun save(user: User) = db.userDao().save(user)
}
