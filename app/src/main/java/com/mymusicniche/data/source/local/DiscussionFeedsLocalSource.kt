package com.mymusicniche.data.source.local

import android.content.SharedPreferences
import com.mymusicniche.api.request.FeedsParams
import com.mymusicniche.api.response.DiscussionsResponse
import com.mymusicniche.data.source.DiscussionFeedsSource
import io.reactivex.Observable
import javax.inject.Inject

class DiscussionFeedsLocalSource @Inject
constructor(private val db: AppDatabase,
    private val pref: SharedPreferences) : DiscussionFeedsSource {

    override fun getDiscussionFeeds(params: FeedsParams): Observable<DiscussionsResponse> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}