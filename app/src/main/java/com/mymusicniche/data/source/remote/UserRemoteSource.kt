package com.mymusicniche.data.source.remote

import android.content.SharedPreferences
import com.google.gson.Gson
import com.mymusicniche.BuildConfig
import com.mymusicniche.api.ApiService
import com.mymusicniche.api.InvalidRefreshTokenException
import com.mymusicniche.api.response.AuthErrorResponse
import com.mymusicniche.api.response.BaseResponse
import com.mymusicniche.api.response.ProfileResponse
import com.mymusicniche.common.Constants
import com.mymusicniche.common.ext.get
import com.mymusicniche.common.ext.put
import com.mymusicniche.data.User
import com.mymusicniche.data.poko.Category
import com.mymusicniche.data.source.UserSource
import com.mymusicniche.data.source.local.AppDatabase
import io.reactivex.Observable
import io.reactivex.Single
import org.json.JSONArray
import retrofit2.HttpException
import timber.log.Timber
import java.net.HttpURLConnection
import javax.inject.Inject

class UserRemoteSource @Inject
constructor(private val apiService: ApiService,
            private val pref: SharedPreferences,
            private val db: AppDatabase) : UserSource {
    override fun getOtherUserProfile(user_id: String): Observable<ProfileResponse> {

        val auth = "${pref.get<String>(Constants.Key.TOKEN_TYPE)} ${pref.get<String>(Constants.Key.ACCESS_TOKEN)}"

        return apiService.getUserProfile(auth, user_id)
                .onErrorResumeNext { err: Throwable ->
                    if (err is HttpException) handleHttpException(err) else emptyBody()
                }
                .map {
                    it
                }
    }

    override fun getProfile(): Observable<User> {
        val auth = "${pref.get<String>(Constants.Key.TOKEN_TYPE)} ${pref.get<String>(Constants.Key.ACCESS_TOKEN)}"

        return apiService.getProfile(auth)
                .onErrorResumeNext { err: Throwable ->
                    if (err is HttpException) handleHttpException(err) else emptyBody()
                }
                .map {
                    it.user
                }
    }

    override fun updateUser(type: Category.Type, email: String,
                            firstName: String, lastName: String,
                            screenName: String, website: String,
                            categoryId: Int, address: String,
                            phone: String, about: String, genres:
                            List<String>, skills: List<String>,
                            influences: List<String>, venueType: String): Single<BaseResponse> {

        val fieldMap = mutableMapOf<String, Any>().apply {
            put("email", email)
            put("first_name", firstName)
            put("last_name", lastName)
            put("screen_name", screenName)
            put("website", website)
            put("category_id", categoryId)
            put("address[full_address]", address)
            put("phone[number]", phone)
            put("about", about)
            put("phone[name]", "")
        }

        when (type) {
            Category.Type.SOLO, Category.Type.GROUP, Category.Type.JAMMER -> {

                if (type == Category.Type.SOLO) {
                    if (!skills.isEmpty()) {
                        fieldMap.put("skills", JSONArray(skills).toString())
                    }
                }

                if (!genres.isEmpty()) {
                    fieldMap.put("genres", JSONArray(genres).toString())
                }

                if (type == Category.Type.GROUP || type == Category.Type.SOLO) {
                    if (!influences.isEmpty()) {
                        fieldMap.put("influences", JSONArray(influences).toString())
                    }
                }
            }
            Category.Type.SPOT -> {
                fieldMap.put("venue_type", venueType)
            }
        }
        Timber.d("fieldMap = $fieldMap")
        val auth = "${pref.get<String>(Constants.Key.TOKEN_TYPE)} ${pref.get<String>(Constants.Key.ACCESS_TOKEN)}"

        return apiService.userUpdate(auth, fieldMap)
    }

    override fun acceptTerms(): Single<BaseResponse> {
        val auth = "${pref.get<String>(Constants.Key.TOKEN_TYPE)} ${pref.get<String>(Constants.Key.ACCESS_TOKEN)}"
        return apiService.acceptTerms(auth).singleOrError()
    }

    private fun handleHttpException(err: HttpException): Observable<ProfileResponse> {
        // error 401 unauthorised
        // error 607
        return if (err.code() == HttpURLConnection.HTTP_PRECON_FAILED) refreshToken() else emptyBody()
    }

    private fun refreshToken(): Observable<ProfileResponse> {
        val auth = "${pref.get<String>(Constants.Key.TOKEN_TYPE)} ${pref.get<String>(Constants.Key.ACCESS_TOKEN)}"
        return apiService.refresh(
                grantType = "refresh_token",
                clientId = BuildConfig.CLIENT_ID,
                clientSecret = BuildConfig.CLIENT_SECRET,
                refreshToken = pref.get(Constants.Key.REFRESH_TOKEN), // prefs.get(REFRESH_TOKEN)
                scope = "*"
        )
                .doOnNext {
                    pref.put(Constants.Key.TOKEN_TYPE, it.token_type)
                    pref.put(Constants.Key.ACCESS_TOKEN, it.access_token)
                    pref.put(Constants.Key.REFRESH_TOKEN, it.refresh_token)
                }
                .doOnError {
                    if (it is HttpException) if (it.code() == HttpURLConnection.HTTP_PRECON_FAILED) {
                        val responseBody = it.response().errorBody()?.string()
                        val authErrorResponse = Gson().fromJson(responseBody, AuthErrorResponse::class.java)
                        if (authErrorResponse.status == 607) {
                            throw InvalidRefreshTokenException("App requires reauthentication")
                        }
                    }
                }
                .concatMap { apiService.getProfile(auth) }
    }

    private fun emptyBody(): Observable<ProfileResponse> {
        return Observable.just(ProfileResponse(0, null, false, ""))
    }

}