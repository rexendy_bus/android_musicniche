package com.mymusicniche.data.source.repository

import com.mymusicniche.api.response.BaseResponse
import com.mymusicniche.api.response.ProfileResponse
import com.mymusicniche.data.User
import com.mymusicniche.data.poko.Category
import com.mymusicniche.data.source.UserSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class UserRepository @Inject
constructor(@Remote private val remote: UserSource, @Local private val local: UserSource) : UserSource {
    override fun getOtherUserProfile(user_id: String): Observable<ProfileResponse> {
        return remote.getOtherUserProfile(user_id)
    }

    override fun getProfile(): Observable<User> {
        return remote.getProfile()
    }

    override fun updateUser(type: Category.Type, email: String, firstName: String, lastName: String,
                            screenName: String, website: String, categoryId: Int, address: String,
                            phone: String, about: String, genres: List<String>,
                            skills: List<String>, influences: List<String>, venueType: String): Single<BaseResponse> {
        return remote.updateUser(type, email, firstName, lastName, screenName, website, categoryId, address, phone, about, genres, skills, influences, venueType)
    }

    override fun acceptTerms(): Single<BaseResponse> {
        return remote.acceptTerms()
    }


}