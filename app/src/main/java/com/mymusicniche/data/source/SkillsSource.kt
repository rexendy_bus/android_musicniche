package com.mymusicniche.data.source

import com.mymusicniche.data.Skill
import io.reactivex.Observable

interface SkillsSource {

    fun getSkills(): Observable<List<Skill>>
}