package com.mymusicniche.data.source.repository

import com.facebook.AccessToken
import com.facebook.GraphRequest.GraphJSONObjectCallback
import com.mymusicniche.api.response.LoginResponse
import com.mymusicniche.data.User
import com.mymusicniche.data.poko.Credential
import com.mymusicniche.data.poko.Device
import com.mymusicniche.data.source.LoginSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class LoginRepository @Inject
constructor(@Remote private val remote: LoginSource,
            @Local  private val local: LoginSource) : LoginSource {

    override fun login(credential: Credential, device: Device): Observable<LoginResponse> {
        return remote.login(credential, device)
            .doOnNext {
                saveTokenType(it.token_type)
                saveAccessToken(it.access_token)
                saveRefreshToken(it.refresh_token)
            }
    }

    override fun graphRequest(token: AccessToken, cb: GraphJSONObjectCallback): Completable {
        return remote.graphRequest(token, cb)
    }

    // TODO setup in-memory caching here

    override fun getProfile(): Observable<User> = remote.getProfile()//.doOnNext { save(it) }

    override fun saveTokenType(tokenType: String) = local.saveTokenType(tokenType)

    override fun saveAccessToken(accessToken: String) = local.saveAccessToken(accessToken)

    override fun saveRefreshToken(refreshToken: String) = local.saveRefreshToken(refreshToken)

    override fun save(user: User) = local.save(user)
}