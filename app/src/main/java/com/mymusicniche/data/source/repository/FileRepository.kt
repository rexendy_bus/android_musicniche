package com.mymusicniche.data.source.repository

import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.services.s3.model.ObjectMetadata
import com.mymusicniche.features.create_profile.FileType
import com.mymusicniche.data.source.FileSource
import com.mymusicniche.di.scopes.Remote
import javax.inject.Inject


class FileRepository @Inject
constructor(@Remote private val remote: FileSource) : FileSource {

    override fun uploadFileToAws(path: String, fileName: String, fileType: FileType, metadata: ObjectMetadata): TransferObserver {
        return remote.uploadFileToAws(path, fileName, fileType, metadata)
    }


}