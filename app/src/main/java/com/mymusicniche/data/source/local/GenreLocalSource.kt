package com.mymusicniche.data.source.local

import com.mymusicniche.data.Genre
import com.mymusicniche.data.source.GenreSource
import io.reactivex.Observable
import javax.inject.Inject

class GenreLocalSource @Inject
constructor(val db: AppDatabase) : GenreSource {

    override fun getGenres(): Observable<List<Genre>> {
        TODO("not implemented")
    }
}