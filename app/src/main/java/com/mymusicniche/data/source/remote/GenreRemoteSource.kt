package com.mymusicniche.data.source.remote

import android.content.SharedPreferences
import com.google.gson.Gson
import com.mymusicniche.BuildConfig
import com.mymusicniche.api.ApiService
import com.mymusicniche.api.InvalidRefreshTokenException
import com.mymusicniche.api.response.AuthErrorResponse
import com.mymusicniche.api.response.GenresResponse
import com.mymusicniche.common.Constants.Key.ACCESS_TOKEN
import com.mymusicniche.common.Constants.Key.REFRESH_TOKEN
import com.mymusicniche.common.Constants.Key.TOKEN_TYPE
import com.mymusicniche.common.ext.get
import com.mymusicniche.common.ext.put
import com.mymusicniche.data.Genre
import com.mymusicniche.data.source.GenreSource
import io.reactivex.Observable
import retrofit2.HttpException
import java.net.HttpURLConnection.HTTP_PRECON_FAILED
import javax.inject.Inject

class GenreRemoteSource @Inject
constructor(val api: ApiService, val prefs: SharedPreferences) : GenreSource {

    private val expiredAccessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjYzZmE4N2E4N2QyNTQyNDIwNDVhYTI2OWMzMWZkYmRmMDJjMTM5OTVhMGIyNTI2ZjdmYzgzYmRmZjM5NTAzODA1MDc1MWM4MTVjMTA1OTIyIn0.eyJhdWQiOiIyIiwianRpIjoiNjNmYTg3YTg3ZDI1NDI0MjA0NWFhMjY5YzMxZmRiZGYwMmMxMzk5NWEwYjI1MjZmN2ZjODNiZGZmMzk1MDM4MDUwNzUxYzgxNWMxMDU5MjIiLCJpYXQiOjE1MTQwMTczODEsIm5iZiI6MTUxNDAxNzM4MSwiZXhwIjoxNTE0MDE3NTAwLCJzdWIiOiIxMDciLCJzY29wZXMiOlsiKiJdfQ.y8drbhouKBGyKDJ-GSIYpYWChxBC8ywxZt1ijtApP-xkXFUO-T8k5aCefO4iY1-3_11lnU7sxt8ZsNx-jq7BoOKW3VgDxIBo53TNF7fv7C2uNQQqQozt84ta2WgifHz_GicUgF01DcYQ1G6Jc91uym5pIWIvpNDvvVZXMJwW-7woeMVyr_FeLgVsLSANVzbtsw0fkZYwK3lJdxTg8kE7-OjQYZuqttbnCi1McjcFO49wWZG7qcEfLhwceEpCzfg3Fiv3SuSxFUhsN7rlP0CaF-RQdgNEfUIlK2JhmyqVGuzFel_yJ1sNQ1U5zj9dlWjeXochG8k91D_BiaY9STdld8WuYzbMJIEtrh9I5dBE8J0Qz0yHZtVyQooYKgs-c5xI-Ob4wd_ODTN2ul7kyNf1OMBGx42vvX81pmmBQqCbsSd9A7ww8fY8KUMGQuBCtQI5NKdhEYXAoDbMft0MJrohaQf6uhAQi6pxckWrZB8eBlv7jTNzS4Sdai91jJ2pu8Qaay_yLYPVxgpl6hT5-4TPwD7pnEcGj-QsOnlv5qEjjYJ3nS7Zv76etBpFTBYK8D-_T5VxIm5v843_LZLOIar8AWYj6iNplWC-RieZnsRtJak-NNF2uQJ0pLRaPq6FCgYnd3KoESJKT0paGOUjMBdZ3L91WRuFw_oLBzverSz6is8"
    private val expiredRefreshToken = "def502000d6bb391c3992ce188803e94f5955d5aca71d541c4cb37709319436db397b63c7ea1dce3199469b0d28993b033634c794db9b2b5ecef3d8e3392fc942970c1cb096748e883b8550b831b3db75c03e1f55195c7387dad54068724453b9f0f5fbfc771daa43b87ae8f4c67fa689e04c715c1fd9b4d275483560ddcdafafc89158a8f52c39ca095eb46f931db9181fe182a249a145df7ec48e8d1ce48f925f71fdd24c52708177fd3bba248f6ff377cad1cd48a96153de2807a8aeac341de95dbd8e96c37d13fe1322b7111ed4e9ec1cae38b013df95ebe279efedcc8a1b5ee0f088b442bf67950f54219e093fef7db3f059c33b30cd72cce17faab94da162d86921f0bed8b627f6d01f5aa5ec63a6069e45abb6c39564a72bf93b5b621cd960e0ef0db9a348dd76566ae864ea7bf6259f333e4dc7e100fce6d10412430abea02e3fb669bbf9197ebeb14a7133766d745d600211cbc0e531f4d619a8bfb8711316df93d"

    override fun getGenres(): Observable<List<Genre>> {
        val auth = "${prefs.get<String>(TOKEN_TYPE)} ${prefs.get<String>(ACCESS_TOKEN)}"
        return api.getGenres(auth) // val auth = "${pref.get<String>(TOKEN_TYPE)} ${pref.get<String>(ACCESS_TOKEN)}"
            .onErrorResumeNext { err: Throwable ->
                if (err is HttpException) handleHttpException(err) else emptyBody()
            }
            .map { it.results }
    }

    private fun handleHttpException(err: HttpException): Observable<GenresResponse> {
        // error 401 unauthorised
        // error 607
        return if (err.code() == HTTP_PRECON_FAILED) refreshToken() else emptyBody()
    }

    private fun refreshToken(): Observable<GenresResponse> {
        return api.refresh(
            grantType = "refresh_token",
            clientId = BuildConfig.CLIENT_ID,
            clientSecret = BuildConfig.CLIENT_SECRET,
            refreshToken = prefs.get(REFRESH_TOKEN), // prefs.get(REFRESH_TOKEN)
            scope = "*"
        )
            .doOnNext {
                prefs.put(TOKEN_TYPE, it.token_type)
                prefs.put(ACCESS_TOKEN, it.access_token)
                prefs.put(REFRESH_TOKEN, it.refresh_token)
            }
            .doOnError {
                if (it is HttpException) if (it.code() == HTTP_PRECON_FAILED) {
                    val responseBody = it.response().errorBody()?.string()
                    val authErrorResponse = Gson().fromJson(responseBody, AuthErrorResponse::class.java)
                    if (authErrorResponse.status == 607) {
                        throw InvalidRefreshTokenException("App requires reauthentication")
                    }
                }
            }
            .concatMap { api.getGenres("${it.token_type} ${it.access_token}") }
    }

    private fun emptyBody(): Observable<GenresResponse> {
        return Observable.just(GenresResponse(0, emptyList()))
    }
}