package com.mymusicniche.data.source.repository

import com.mymusicniche.api.request.FeedsParams
import com.mymusicniche.api.response.DiscussionsResponse
import com.mymusicniche.data.source.DiscussionFeedsSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import io.reactivex.Observable
import javax.inject.Inject

class DiscussionFeedsRepository @Inject
constructor(
    @Remote private val remote: DiscussionFeedsSource,
    @Local private val local: DiscussionFeedsSource) : DiscussionFeedsSource {

    override fun getDiscussionFeeds(params: FeedsParams): Observable<DiscussionsResponse> {
        return remote.getDiscussionFeeds(params)
    }
}