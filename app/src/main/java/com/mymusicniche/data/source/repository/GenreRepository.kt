package com.mymusicniche.data.source.repository

import com.mymusicniche.data.Genre
import com.mymusicniche.data.source.GenreSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import io.reactivex.Observable
import javax.inject.Inject


class GenreRepository @Inject
constructor(@Remote val remote: GenreSource,
            @Local  val  local: GenreSource) : GenreSource {

    override fun getGenres(): Observable<List<Genre>> {
        return remote.getGenres()
    }

}