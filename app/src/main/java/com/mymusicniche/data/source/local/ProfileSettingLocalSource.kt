package com.mymusicniche.data.source.local

import android.content.SharedPreferences
import com.mymusicniche.api.response.BaseResponse
import com.mymusicniche.data.User
import com.mymusicniche.data.source.ProfileSettingSource
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class ProfileSettingLocalSource @Inject
constructor(private val sharedPref: SharedPreferences, private val db: AppDatabase) : ProfileSettingSource{
    override fun getProfile(): Observable<User> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateNotification(switch: String, module: String?): Single<BaseResponse> {
        return Observable.empty<BaseResponse>().singleOrError()
    }

    override fun updatePrivacy(visibility: String): Single<BaseResponse> {
        return Observable.empty<BaseResponse>().singleOrError()
    }

}