package com.mymusicniche.data.source.remote

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.common.Constants
import com.mymusicniche.common.ext.get
import com.mymusicniche.data.Skill
import com.mymusicniche.data.source.InfluencerSource
import com.mymusicniche.data.source.SkillsSource
import io.reactivex.Observable
import javax.inject.Inject

class InfluencerRemoteSource @Inject
constructor(private val apiService: ApiService,
            private val pref: SharedPreferences) : InfluencerSource {

    override fun getInfluencers(): Observable<List<String>> {
        return Observable.empty()
    }


}