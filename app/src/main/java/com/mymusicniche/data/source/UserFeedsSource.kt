package com.mymusicniche.data.source

import com.mymusicniche.api.request.PostDeleteParam
import com.mymusicniche.api.request.UserFeedsParam
import com.mymusicniche.api.response.PostResponse
import com.mymusicniche.api.response.UserFeedResponse
import io.reactivex.Observable

interface UserFeedsSource {
    fun getUserFeeds(params: UserFeedsParam): Observable<UserFeedResponse>

    fun deleteUserFeed(params: PostDeleteParam): Observable<PostResponse>
}