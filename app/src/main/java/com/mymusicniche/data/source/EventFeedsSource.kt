package com.mymusicniche.data.source

import com.mymusicniche.api.request.FeedsParams
import com.mymusicniche.api.response.EventsResponse
import io.reactivex.Observable

interface EventFeedsSource {

    fun getEventFeeds(params: FeedsParams): Observable<EventsResponse>
}