package com.mymusicniche.data.source.remote

import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.model.ObjectMetadata
import com.mymusicniche.common.Constants
import com.mymusicniche.data.source.FileSource
import com.mymusicniche.features.create_profile.FileType
import java.io.File
import javax.inject.Inject

class FileRemoteSource @Inject
constructor(private val transferUtility: TransferUtility) : FileSource {

    override fun uploadFileToAws(path: String, fileName: String, fileType: FileType, metadata: ObjectMetadata): TransferObserver {

        val bucketName = when (fileType) {
            FileType.PRIMARY,
            FileType.BACKGROUND,
            FileType.PORTFOLIO -> Constants.Aws.PHOTO_BUCKET
            else -> Constants.Aws.VIDEO_BUCKET
        }

        return transferUtility.upload(bucketName, fileName, File(path), metadata)
    }

}