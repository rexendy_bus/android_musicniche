package com.mymusicniche.data.source.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.mymusicniche.common.Constants.Database.VERSION
import com.mymusicniche.data.User
import com.mymusicniche.data.source.TypeConverterData

@Database(entities = [(User::class)], version = VERSION, exportSchema = true)
@TypeConverters(TypeConverterData::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
}