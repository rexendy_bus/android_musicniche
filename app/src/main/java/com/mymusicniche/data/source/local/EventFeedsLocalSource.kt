package com.mymusicniche.data.source.local

import android.content.SharedPreferences
import com.mymusicniche.api.request.FeedsParams
import com.mymusicniche.api.response.EventsResponse
import com.mymusicniche.data.source.EventFeedsSource
import io.reactivex.Observable
import javax.inject.Inject

class EventFeedsLocalSource @Inject
constructor(val db: AppDatabase, val pref: SharedPreferences) : EventFeedsSource {

    override fun getEventFeeds(params: FeedsParams): Observable<EventsResponse> {
        return Observable.empty()
    }
}