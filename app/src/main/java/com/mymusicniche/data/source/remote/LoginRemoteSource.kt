package com.mymusicniche.data.source.remote

import android.content.SharedPreferences
import android.os.Bundle
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.GraphRequest.GraphJSONObjectCallback
import com.google.gson.Gson
import com.mymusicniche.BuildConfig
import com.mymusicniche.api.ApiService
import com.mymusicniche.api.InvalidRefreshTokenException
import com.mymusicniche.api.response.AuthErrorResponse
import com.mymusicniche.api.response.LoginResponse
import com.mymusicniche.api.response.ProfileResponse
import com.mymusicniche.common.Constants
import com.mymusicniche.common.Constants.Key.ACCESS_TOKEN
import com.mymusicniche.common.Constants.Key.TOKEN_TYPE
import com.mymusicniche.common.Constants.LoginType.FACEBOOK
import com.mymusicniche.common.ext.get
import com.mymusicniche.common.ext.put
import com.mymusicniche.data.User
import com.mymusicniche.data.poko.Credential
import com.mymusicniche.data.poko.Device
import com.mymusicniche.data.source.LoginSource
import io.reactivex.Completable
import io.reactivex.Observable
import org.json.JSONObject
import retrofit2.HttpException
import java.net.HttpURLConnection
import java.net.HttpURLConnection.HTTP_OK
import javax.inject.Inject

class LoginRemoteSource @Inject
constructor(private val apiService: ApiService, private val pref: SharedPreferences) : LoginSource {
    override fun login(credential: Credential, device: Device): Observable<LoginResponse> {
        return apiService.login(
            grantType = "password",
            clientId = BuildConfig.CLIENT_ID,
            clientSecret = BuildConfig.CLIENT_SECRET,
            username = credential.username,
            password = credential.password,
            loginType = FACEBOOK,
            firstName = credential.firstName,
            lastName = credential.lastName,
            scope = "*",
            platform = device.platform,
            playerId = device.playerId,
            deviceName = device.name
        )
    }

    override fun saveTokenType(tokenType: String) {}

    override fun saveAccessToken(accessToken: String) {}

    override fun saveRefreshToken(refreshToken: String) {}

    override fun graphRequest(token: AccessToken, cb: GraphJSONObjectCallback): Completable {
        // https://stackoverflow.com/a/29379794/1076574
        val graphRequest = GraphRequest.newMeRequest(token, cb)
        val params = Bundle()
        params.putString("fields", "id,name,email,picture.type(normal),cover, address, last_name, first_name, location")
        graphRequest.parameters = params

        return Completable.create {
            val response = graphRequest.executeAndWait()
            if (response.connection.responseCode == HTTP_OK) {
                cb.onCompleted(response.jsonObject, response)
                it.onComplete()
            } else {
                cb.onCompleted(JSONObject("{}"), response)
                it.onError(response.error.exception)
            }
        }
    }

    override fun getProfile(): Observable<User> {
        val auth = "${pref.get<String>(TOKEN_TYPE)} ${pref.get<String>(ACCESS_TOKEN)}"

        return apiService.getProfile(auth)
                .onErrorResumeNext { err: Throwable ->
                    if (err is HttpException) handleHttpException(err) else emptyBody()
                }
                .map {

                    it.user
                }
    }

    private fun handleHttpException(err: HttpException): Observable<ProfileResponse> {
        // error 401 unauthorised
        // error 607
        return if (err.code() == HttpURLConnection.HTTP_PRECON_FAILED) refreshToken() else emptyBody()
    }

    private fun refreshToken(): Observable<ProfileResponse> {
        return apiService.refresh(
                grantType = "refresh_token",
                clientId = BuildConfig.CLIENT_ID,
                clientSecret = BuildConfig.CLIENT_SECRET,
                refreshToken = pref.get(Constants.Key.REFRESH_TOKEN), // prefs.get(REFRESH_TOKEN)
                scope = "*"
        )
                .doOnNext {
                    pref.put(TOKEN_TYPE, it.token_type)
                    pref.put(ACCESS_TOKEN, it.access_token)
                    pref.put(Constants.Key.REFRESH_TOKEN, it.refresh_token)
                }
                .doOnError {
                    if (it is HttpException) if (it.code() == HttpURLConnection.HTTP_PRECON_FAILED) {
                        val responseBody = it.response().errorBody()?.string()
                        val authErrorResponse = Gson().fromJson(responseBody, AuthErrorResponse::class.java)
                        if (authErrorResponse.status == 607) {
                            throw InvalidRefreshTokenException("App requires reauthentication")
                        }
                    }
                }
                .concatMap { apiService.getProfile("${it.token_type} ${it.access_token}")}
    }

    private fun emptyBody(): Observable<ProfileResponse> {

        var emptyUser = User(0,"","","","","","","","","","",0,"",ArrayList()
        ,ArrayList(),ArrayList(),null,ArrayList(),ArrayList(),null,ArrayList(),null, "", ArrayList())

       return Observable.just(ProfileResponse(0, emptyUser, false, ""))


    }

    override fun save(user: User) {}
}