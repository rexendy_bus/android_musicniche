package com.mymusicniche.data.source.repository

import com.mymusicniche.data.Skill
import com.mymusicniche.data.source.InfluencerSource
import com.mymusicniche.data.source.SkillsSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import io.reactivex.Observable
import javax.inject.Inject

class InfluencerRepository @Inject
constructor(@Remote private val remote: InfluencerSource,
            @Local private val local: InfluencerSource) : InfluencerSource {

    override fun getInfluencers(): Observable<List<String>> {
        return local.getInfluencers()
    }


}