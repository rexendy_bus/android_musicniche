package com.mymusicniche.data.source.remote

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.common.Constants
import com.mymusicniche.common.Constants.Key.ACCESS_TOKEN
import com.mymusicniche.common.ext.get
import com.mymusicniche.data.Skill
import com.mymusicniche.data.source.SkillsSource
import io.reactivex.Observable
import javax.inject.Inject

class SkillsRemoteSource @Inject
constructor(private val apiService: ApiService,
            private val pref: SharedPreferences) : SkillsSource {

    override fun getSkills(): Observable<List<Skill>> {
        val auth = "${pref.get<String>(Constants.Key.TOKEN_TYPE)} ${pref.get<String>(ACCESS_TOKEN)}"
        return apiService.getSkills(auth)
                .map { it.results }
    }

}