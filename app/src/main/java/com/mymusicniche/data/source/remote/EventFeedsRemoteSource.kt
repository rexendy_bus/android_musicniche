package com.mymusicniche.data.source.remote

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.api.request.FeedsParams
import com.mymusicniche.api.response.EventsResponse
import com.mymusicniche.common.Constants.Key.ACCESS_TOKEN
import com.mymusicniche.common.Constants.Key.TOKEN_TYPE
import com.mymusicniche.common.ext.get
import com.mymusicniche.data.source.EventFeedsSource
import io.reactivex.Observable
import javax.inject.Inject

class EventFeedsRemoteSource @Inject
constructor(val api: ApiService, val pref: SharedPreferences) : EventFeedsSource {

    override fun getEventFeeds(params: FeedsParams): Observable<EventsResponse> {
        val auth = "${pref.get<String>(TOKEN_TYPE)} ${pref.get<String>(ACCESS_TOKEN)}"

        // ?latitude=10.3157&longitude=123.8854&page=1
        val queryMap = mutableMapOf<String, Number>().apply {
            put("latitude", params.latitude)
            put("longitude", params.longitude)
            put("page", params.page)
        }

        return api.getEventFeeds(auth, queryMap)
    }
}