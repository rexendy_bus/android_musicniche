package com.mymusicniche.data.source.repository

import com.mymusicniche.api.request.FeedsParams
import com.mymusicniche.api.response.EventsResponse
import com.mymusicniche.data.source.EventFeedsSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import io.reactivex.Observable
import javax.inject.Inject

class EventFeedsRepository @Inject
constructor(
    @Remote private val remote: EventFeedsSource,
    @Local private val local: EventFeedsSource) : EventFeedsSource {

    override fun getEventFeeds(params: FeedsParams): Observable<EventsResponse> {
        return remote.getEventFeeds(params)
    }
}