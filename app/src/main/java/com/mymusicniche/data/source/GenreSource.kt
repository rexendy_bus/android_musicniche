package com.mymusicniche.data.source

import com.mymusicniche.data.Genre
import io.reactivex.Observable


interface GenreSource {

    fun getGenres(): Observable<List<Genre>>
}