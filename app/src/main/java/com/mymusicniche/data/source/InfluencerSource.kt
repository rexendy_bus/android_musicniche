package com.mymusicniche.data.source

import io.reactivex.Observable


interface InfluencerSource {

    fun getInfluencers(): Observable<List<String>>
}