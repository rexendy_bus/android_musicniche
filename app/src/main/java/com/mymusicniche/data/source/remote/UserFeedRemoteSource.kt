package com.mymusicniche.data.source.remote

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.api.request.PostDeleteParam
import com.mymusicniche.api.request.UserFeedsParam
import com.mymusicniche.api.response.PostResponse
import com.mymusicniche.api.response.UserFeedResponse
import com.mymusicniche.common.Constants
import com.mymusicniche.common.ext.get
import com.mymusicniche.data.source.UserFeedsSource
import io.reactivex.Observable
import java.util.*
import javax.inject.Inject

class UserFeedRemoteSource @Inject
constructor(val api: ApiService, val pref: SharedPreferences) : UserFeedsSource {
    override fun deleteUserFeed(params: PostDeleteParam): Observable<PostResponse> {
        val auth = "${pref.get<String>(Constants.Key.TOKEN_TYPE)} ${pref.get<String>(Constants.Key.ACCESS_TOKEN)}"

        return api.postDelete(auth, params.id)
    }

    override fun getUserFeeds(params: UserFeedsParam): Observable<UserFeedResponse> {
        val auth = "${pref.get<String>(Constants.Key.TOKEN_TYPE)} ${pref.get<String>(Constants.Key.ACCESS_TOKEN)}"

        return api.getUserFeeds(auth, params.page, params.perPage, params.user_id)
    }
}