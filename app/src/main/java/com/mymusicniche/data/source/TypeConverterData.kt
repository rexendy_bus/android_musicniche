package com.mymusicniche.data.source

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mymusicniche.data.*
import java.util.*



/**
 * Created by rexenjeandy on 3/6/18.
 */
class TypeConverterData {
    val gson: Gson = Gson()

    @TypeConverter
    fun stringToAvatarList(data: String?): List<ProfileAvatar> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<ProfileAvatar>>() {}.getType()
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun avataristToString(someObjects: List<ProfileAvatar>): String {
        return gson.toJson(someObjects)
    }


    @TypeConverter
    fun stringToUserLocation(data: String?): UserLocation? {
        if (data == null) {
            return null
        }

        val listType = object : TypeToken<UserLocation>() {}.getType()
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun userLocationToString(someObjects: UserLocation): String {
        return gson.toJson(someObjects)
    }

    @TypeConverter
    fun stringToUserSkillList(data: String?): List<UserSkill> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<UserSkill>>() {}.getType()
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun userSkillToString(someObjects: List<UserSkill>): String {
        return gson.toJson(someObjects)
    }

    @TypeConverter
    fun stringToUserGenreList(data: String?): List<UserGenre> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<UserGenre>>() {}.getType()
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun userGenreToString(someObjects: List<UserGenre>): String {
        return gson.toJson(someObjects)
    }

    @TypeConverter
    fun stringToUserInfluencesList(data: String?): List<UserInfluences> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<UserInfluences>>() {}.getType()
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun userInfluencesToString(someObjects: List<UserInfluences>): String {
        return gson.toJson(someObjects)
    }

    @TypeConverter
    fun stringToCategory(data: String?): Category? {
        if (data == null) {
            return null
        }

        val listType = object : TypeToken<Category>() {}.getType()
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun categoryToString(someObjects: Category): String {
        return gson.toJson(someObjects)
    }

    @TypeConverter
    fun stringToUserPhone(data: String?): UserPhone? {
        if (data == null) {
            return null
        }

        val listType = object : TypeToken<UserPhone>() {}.getType()
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun userPhoneToString(someObjects: UserPhone): String {
        return gson.toJson(someObjects)
    }

    @TypeConverter
    fun fromString(value: String): List<String> {
        val listType = object : TypeToken<List<String>>() {}.type

        return gson.fromJson<List<String>>(value, listType)
    }

    @TypeConverter
    fun fromArrayLisr(list: List<String>): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}

