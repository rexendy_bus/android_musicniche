package com.mymusicniche.data.source.local

import android.content.SharedPreferences
import com.mymusicniche.api.response.BaseResponse
import com.mymusicniche.api.response.ProfileResponse
import com.mymusicniche.data.User
import com.mymusicniche.data.poko.Category
import com.mymusicniche.data.source.UserSource
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class UserLocalSource @Inject
constructor(private val sharedPref: SharedPreferences, private val db: AppDatabase) : UserSource {
    override fun getOtherUserProfile(user_id: String): Observable<ProfileResponse> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getProfile(): Observable<User> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun acceptTerms(): Single<BaseResponse> {
        return Observable.empty<BaseResponse>().singleOrError()
    }

    override fun updateUser(type: Category.Type, email: String,
                            firstName: String, lastName: String,
                            screenName: String, website: String,
                            categoryId: Int, address: String,
                            phone: String, about: String,
                            genres: List<String>, skills: List<String>,
                            influences: List<String>, venueType: String): Single<BaseResponse> {
        return Observable.empty<BaseResponse>().singleOrError()
    }
}
