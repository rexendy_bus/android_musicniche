package com.mymusicniche.data.source.remote

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.api.response.BaseResponse
import com.mymusicniche.common.Constants
import com.mymusicniche.common.ext.get
import com.mymusicniche.data.User
import com.mymusicniche.data.source.ProfileSettingSource
import com.mymusicniche.data.source.local.AppDatabase
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class ProfileSettingRemoteSource @Inject
constructor(private val apiService: ApiService,
            private val pref: SharedPreferences,
            private val db: AppDatabase
) : ProfileSettingSource {

    override fun getProfile(): Observable<User> {
        val auth = "${pref.get<String>(Constants.Key.TOKEN_TYPE)} ${pref.get<String>(Constants.Key.ACCESS_TOKEN)}"
        return apiService.getProfile(auth)
                .map {
                    it.user
                }
    }

    override fun updateNotification(switch: String, module: String?): Single<BaseResponse> {
        val auth = "${pref.get<String>(Constants.Key.TOKEN_TYPE)} ${pref.get<String>(Constants.Key.ACCESS_TOKEN)}"

        val fieldMap = mutableMapOf<String, Any>().apply {
            put("switch", switch)
            if (module != null) {
                put("module", module)
            }
        }

        return apiService.updateNotif(auth, fieldMap).singleOrError()
    }

    override fun updatePrivacy(visibility: String): Single<BaseResponse> {
        val auth = "${pref.get<String>(Constants.Key.TOKEN_TYPE)} ${pref.get<String>(Constants.Key.ACCESS_TOKEN)}"

        val fieldMap = mutableMapOf<String, Any>().apply {
            put("visibility", visibility)
        }

        return apiService.updatePrivacy(auth, fieldMap).singleOrError()
    }

}