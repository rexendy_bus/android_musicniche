package com.mymusicniche.data.source.repository

import com.mymusicniche.api.request.PostDeleteParam
import com.mymusicniche.api.request.UserFeedsParam
import com.mymusicniche.api.response.PostResponse
import com.mymusicniche.api.response.UserFeedResponse
import com.mymusicniche.data.source.UserFeedsSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import io.reactivex.Observable
import javax.inject.Inject

class UserFeedsRepository @Inject
constructor(
    @Remote private val remote: UserFeedsSource,
    @Local private val local: UserFeedsSource
) : UserFeedsSource {
    override fun deleteUserFeed(params: PostDeleteParam): Observable<PostResponse> {
        return remote.deleteUserFeed(params)
    }

    override fun getUserFeeds(params: UserFeedsParam): Observable<UserFeedResponse> {
        return remote.getUserFeeds(params)
    }
}