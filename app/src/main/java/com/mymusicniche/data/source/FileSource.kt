package com.mymusicniche.data.source

import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.services.s3.model.ObjectMetadata
import com.mymusicniche.features.create_profile.FileType

interface FileSource {

    fun uploadFileToAws(path: String, fileName: String, fileType: FileType, metadata: ObjectMetadata): TransferObserver

}