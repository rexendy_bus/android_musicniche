package com.mymusicniche.data.source.local

import android.content.SharedPreferences
import com.mymusicniche.api.request.PostDeleteParam
import com.mymusicniche.api.request.UserFeedsParam
import com.mymusicniche.api.response.PostResponse
import com.mymusicniche.api.response.UserFeedResponse
import com.mymusicniche.data.source.UserFeedsSource
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by rexenjeandy on 3/12/18.
 */
class UserFeedsLocalSource @Inject
constructor(val db: AppDatabase, val pref: SharedPreferences) : UserFeedsSource {
    override fun deleteUserFeed(params: PostDeleteParam): Observable<PostResponse> {
        return Observable.empty()
    }

    override fun getUserFeeds(params: UserFeedsParam): Observable<UserFeedResponse> {
        return Observable.empty()
    }
}