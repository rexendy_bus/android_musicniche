package com.mymusicniche.data

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize @SuppressLint("ParcelCreator")
data class Genre (
        val id: Long,
        val name: String,
        val category: Category,
        var selected: Boolean = false
) : Parcelable