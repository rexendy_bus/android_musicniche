package com.mymusicniche.data


enum class Type(val type: Int) {
    SOLO(0), GROUP(1), SPOT(2), JAMMER(3)
}