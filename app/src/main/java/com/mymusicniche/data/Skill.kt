package com.mymusicniche.data

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class Skill(
    val id: Long,
    val title: String,
    val instrument: String,
    val category: Category,
    var selected: Boolean = false
) : Parcelable