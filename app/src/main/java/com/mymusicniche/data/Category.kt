package com.mymusicniche.data

import java.io.Serializable

data class Category (
        val id: Long,
        val name: String,
        val description: String?
) : Serializable