package com.mymusicniche.data

import android.annotation.SuppressLint
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Entity
@Parcelize
data class User(

    @PrimaryKey(autoGenerate = true)
    var _id: Long,

    val uid: String,

    @SerializedName("first_name")
    val firstName: String,

    @SerializedName("last_name")
    val lastName: String,

    @SerializedName("screen_name")
    val screenName: String,

    val email: String,

    val website: String,

    val about: String,

    @SerializedName("venue_type")
    val venueType: String,

    val birthday: String,

    @SerializedName("accepted_terms")
    val acceptedTerms: String,

    @SerializedName("address_id")
    val addressId: Int,

    @SerializedName("created_at")
    val createdAt: String,

    @SerializedName("avatar")
    val avatars: List<ProfileAvatar>,

    @SerializedName("background")
    val backgrounds: List<ProfileAvatar>,

    val portfolios: List<ProfileAvatar>,

    val address: UserLocation?,

    val skills: List<UserSkill>,

    val genres: List<UserGenre>,

    val category: Category?,

    val influences: List<UserInfluences>,

    val phone: UserPhone?,

    val privacy: String,

    @SerializedName("notification_types")
    val notificationTypes: List<String>

//    @SerializedName("updated_at")
//    val updatedAt: String

    /*

    val phone: Phone,

    val status: Status,

    val accounts: List<Account>,
    */

) : Parcelable {
    override fun toString(): String {
        return "uid:$uid, name:$firstName, email:$email, created:$createdAt"
    }
}

class ProfileAvatar() : Parcelable {
    var id: Int = 0;
    var type: String? = null
    var source: AvatarSource = AvatarSource()

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        type = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProfileAvatar> {
        override fun createFromParcel(parcel: Parcel): ProfileAvatar {
            return ProfileAvatar(parcel)
        }

        override fun newArray(size: Int): Array<ProfileAvatar?> {
            return arrayOfNulls(size)
        }
    }
}

class AvatarSource {
    var original: String? = null
    var large: String? = null
    var medium: String? = null
    var small: String? = null
    var playlist: String? = null
}

@SuppressLint("ParcelCreator")
@Parcelize
data class UserLocation (
    var id: Int,
    var address: String,
    var address2: String,
    var city: String,
    var state: String,
    var zip: String,

    @SerializedName("full_address")
    var fullAddress: String,

    var latitude: Double,
    var longitude: Double
): Parcelable


@SuppressLint("ParcelCreator")
@Parcelize
data class UserSkill(
        val title: String,
        val instrument: String,
        val category: String
) : Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class UserGenre(
        val genre: String,
        val category: String
) : Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class UserInfluences(
        val name: String,
        val type: String?
) : Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class UserPhone(
        val id: Int,
        val number: String
) : Parcelable