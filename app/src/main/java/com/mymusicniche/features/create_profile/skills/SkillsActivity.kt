package com.mymusicniche.features.create_profile.skills

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearLayoutManager.VERTICAL
import com.mymusicniche.R
import com.mymusicniche.common.ext.hide
import com.mymusicniche.common.ext.show
import com.mymusicniche.data.Skill
import com.mymusicniche.features.create_profile.CreateProfileActivity.Companion.EXTRA_SKILLS
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_skills.*
import kotlinx.android.synthetic.main.toolbar.*
import timber.log.Timber
import javax.inject.Inject

class SkillsActivity : DaggerAppCompatActivity(), SkillsAdapter.OnSkillSelectedListener {

    @Inject lateinit var viewModel: SkillsViewModel

    private val disposables = CompositeDisposable()

    private var selectedSkills = arrayListOf<Skill>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_skills)
        setupToolbar()

        selectedSkills = intent.getParcelableArrayListExtra<Skill>(EXTRA_SKILLS)

        recyclerView.layoutManager = LinearLayoutManager(this, VERTICAL, false)
        recyclerView.setHasFixedSize(true)
    }

    override fun onStart() {
        super.onStart()
        bindLoadingIndicator()
        bindSkills()
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    private fun bindSkills() {
        disposables.add(viewModel.getSkills(selectedSkills)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ showList(it) }, { Timber.e(it) }))
    }

    private fun bindLoadingIndicator() {
        disposables.add(viewModel.getLoadingIndicator()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ showLoadingIndicator(it) }))
    }

    private fun showLoadingIndicator(showing: Boolean) {
        if (showing) loadingView.show() else loadingView.hide()
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putParcelableArrayListExtra(EXTRA_SKILLS, selectedSkills)
        setResult(Activity.RESULT_OK, intent)
        super.onBackPressed()
    }

    private fun setupToolbar() {
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbarTextView.text = getString(R.string.skills)
        toolbarView.setOnMenuItemClickListener { true }
    }

    private fun showList(skills: List<Skill>) {
        val adapter = SkillsAdapter(skills)
        adapter.setOnSkillSelectedListener(this)
        recyclerView.adapter = adapter
    }

    override fun onSkillSelected(skill: Skill) {
        if (selectedSkills.contains(skill)) {
            selectedSkills.remove(skill)
        } else {
            selectedSkills.add(skill)
        }
    }
}