package com.mymusicniche.features.create_profile


interface CreateProfileContract {

    //SOLO

    fun getFirstName(): String

    fun getLastName(): String

    fun getBandName(): String

    fun getScreenName(): String

    fun getEmail(): String

    fun getLocation(): String

    fun getPhone(): String

    fun getBirthdate(): String

    fun getSkills(): List<String>

    fun getGenres(): List<String>

    fun getInfluencers(): List<String>

    fun getSaySomething(): String


}