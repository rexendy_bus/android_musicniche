package com.mymusicniche.features.create_profile.influencers

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.data.source.InfluencerSource
import com.mymusicniche.data.source.local.InfluencerLocalSource
import com.mymusicniche.data.source.remote.InfluencerRemoteSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import dagger.Module
import dagger.Provides

@Module
class InfluencersRepositoryModule {

    @Provides
    @Remote
    fun providesInfluencerRemoteSource(apiService: ApiService, pref: SharedPreferences): InfluencerSource {
        return InfluencerRemoteSource(apiService, pref)
    }

    @Provides
    @Local
    fun providesInfluencerLocalSource(sharedPref: SharedPreferences): InfluencerSource {
        return InfluencerLocalSource(sharedPref)
    }
}