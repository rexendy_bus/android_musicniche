package com.mymusicniche.features.create_profile.skills

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mymusicniche.R
import com.mymusicniche.common.ext.hide
import com.mymusicniche.common.ext.show
import com.mymusicniche.data.Skill
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_skills.*

class SkillsAdapter(private val skills: List<Skill>) : RecyclerView.Adapter<SkillsAdapter.ViewHolder>() {

    private lateinit var onSkillSelectedListener: OnSkillSelectedListener

    override fun getItemCount(): Int = skills.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(skills[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_skills, parent, false)
        return ViewHolder(view)
    }

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(skill: Skill) {
            tvSkill.text = skill.title

            if (skill.selected) imageCheck.show() else imageCheck.hide()

            containerView.setOnClickListener {
                if (skill.selected) {
                    skill.selected = false
                    imageCheck.hide()
                } else {
                    skill.selected = true
                    imageCheck.show()
                }
                onSkillSelectedListener.onSkillSelected(skill)
            }
        }
    }

    fun setOnSkillSelectedListener(listener: OnSkillSelectedListener) {
        onSkillSelectedListener = listener
    }

    interface OnSkillSelectedListener {
        fun onSkillSelected(skill: Skill)
    }
}