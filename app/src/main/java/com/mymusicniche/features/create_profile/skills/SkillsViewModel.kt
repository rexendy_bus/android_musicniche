package com.mymusicniche.features.create_profile.skills

import com.mymusicniche.common.base.BaseViewModel
import com.mymusicniche.data.Skill
import com.mymusicniche.data.source.repository.SkillsRepository
import io.reactivex.Observable
import javax.inject.Inject

class SkillsViewModel @Inject
constructor(private val repo: SkillsRepository) : BaseViewModel() {

    fun getSkills(list: List<Skill>): Observable<List<Skill>> {
        return repo.getSkills()
            .doOnSubscribe { loadingIndicator.onNext(true) }
            .doOnNext { loadingIndicator.onNext(false) }
            .doFinally { loadingIndicator.onNext(false) }
            .flatMapIterable { it }
            .doOnNext { item ->
                if (list.any { it.id == item.id }) {
                    item.selected = true
                }
            }
            .toList().toObservable()
    }

    override fun getLoadingIndicator(): Observable<Boolean> = loadingIndicator
}