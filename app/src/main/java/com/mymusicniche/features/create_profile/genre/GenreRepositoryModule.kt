package com.mymusicniche.features.create_profile.genre

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.data.source.GenreSource
import com.mymusicniche.data.source.local.AppDatabase
import com.mymusicniche.data.source.local.GenreLocalSource
import com.mymusicniche.data.source.remote.GenreRemoteSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import dagger.Module
import dagger.Provides

@Module
class GenreRepositoryModule {

    @Provides
    @Remote
    fun providesGenreRemoteSource(api: ApiService, prefs: SharedPreferences): GenreSource {
        return GenreRemoteSource(api, prefs)
    }

    @Provides
    @Local
    fun providesGenreLocalSource(db: AppDatabase): GenreSource = GenreLocalSource(db)
}