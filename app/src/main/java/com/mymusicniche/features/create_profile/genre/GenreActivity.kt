package com.mymusicniche.features.create_profile.genre

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearLayoutManager.VERTICAL
import com.mymusicniche.R
import com.mymusicniche.api.InvalidRefreshTokenException
import com.mymusicniche.common.ext.hide
import com.mymusicniche.common.ext.show
import com.mymusicniche.common.ext.toast
import com.mymusicniche.data.Genre
import com.mymusicniche.features.create_profile.CreateProfileActivity.Companion.EXTRA_GENRES
import com.mymusicniche.features.create_profile.genre.GenreAdapter.OnGenreSelectedListener
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.exceptions.CompositeException
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_genre.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class GenreActivity : DaggerAppCompatActivity(), OnGenreSelectedListener {

    @Inject
    lateinit var viewModel: GenreViewModel

    private val disposables = CompositeDisposable()

    private var selectedGenres = arrayListOf<Genre>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_genre)
        setupToolbar()

        selectedGenres = intent.getParcelableArrayListExtra<Genre>(EXTRA_GENRES)

        genreRecyclerView.layoutManager = LinearLayoutManager(this, VERTICAL, false)
        genreRecyclerView.setHasFixedSize(true)
    }

    override fun onStart() {
        super.onStart()
        bindLoadingIndicator()
        bindGenres()
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    private fun bindLoadingIndicator() {
        disposables.add(
            viewModel.getLoadingIndicator()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ showLoadingIndicator(it) })
        )
    }

    private fun showLoadingIndicator(showing: Boolean) {
        if (showing) genreLoaderView.show() else genreLoaderView.hide()
    }

    private fun bindGenres() {
        disposables.add(
            viewModel.loadGenres(selectedGenres)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ displayGenres(it) }, { handleError(it) })
        )
    }

    private fun displayGenres(genres: List<Genre>) {
        val adapter = GenreAdapter(genres)
        adapter.setOnGenreSelectedListener(this)
        genreRecyclerView.adapter = adapter
    }

    private fun handleError(throwable: Throwable) {
        val compositeException = (throwable as CompositeException)
        if (compositeException.size() > 0) {
            val exception = compositeException.exceptions.find { it is InvalidRefreshTokenException }
            if (exception != null) exception.message?.let { toast(it) }
        }
    }

    override fun onGenreSelected(genre: Genre) {
        if (selectedGenres.contains(genre)) {
            selectedGenres.remove(genre)
        } else {
            selectedGenres.add(genre)
        }
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putParcelableArrayListExtra(EXTRA_GENRES, selectedGenres)
        setResult(Activity.RESULT_OK, intent)
        super.onBackPressed()
    }

    private fun setupToolbar() {
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbarTextView.text = getString(R.string.genre)
        toolbarView.setOnMenuItemClickListener { true }
    }
}
