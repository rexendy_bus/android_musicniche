package com.mymusicniche.features.create_profile.genre

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mymusicniche.R
import com.mymusicniche.common.ext.hide
import com.mymusicniche.common.ext.show
import com.mymusicniche.data.Genre
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_skills.tvSkill
import kotlinx.android.synthetic.main.item_skills.imageCheck


class GenreAdapter(private val genres: List<Genre>) : RecyclerView.Adapter<GenreAdapter.ViewHolder>() {

    private lateinit var onGenreSelectedListener: OnGenreSelectedListener

    override fun getItemCount(): Int = genres.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(genres[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val containerView = LayoutInflater.from(parent.context).inflate(R.layout.item_skills, parent, false)
        return ViewHolder(containerView)
    }

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(genre: Genre) {
            tvSkill.text = genre.name

            if (genre.selected) imageCheck.show() else imageCheck.hide()

            containerView.setOnClickListener {
                if (genre.selected) {
                    genre.selected = false
                    tvSkill.hide()
                } else {
                    genre.selected = true
                    imageCheck.show()
                }
                onGenreSelectedListener.onGenreSelected(genre)
            }

        }

    }

    fun setOnGenreSelectedListener(listener: OnGenreSelectedListener) {
        onGenreSelectedListener = listener
    }

    interface OnGenreSelectedListener {
        fun onGenreSelected(genre: Genre)
    }

}