package com.mymusicniche.features.create_profile

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.services.s3.model.ObjectMetadata
import com.mymusicniche.api.response.BaseResponse
import com.mymusicniche.common.Constants
import com.mymusicniche.data.User
import com.mymusicniche.data.poko.Category
import com.mymusicniche.data.source.repository.FileRepository
import com.mymusicniche.data.source.repository.UserRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.apache.commons.io.FilenameUtils
import timber.log.Timber
import java.lang.Exception
import java.util.*
import javax.inject.Inject

class CreateProfileViewModel @Inject
constructor(private val fileRepository: FileRepository, private val userRepository: UserRepository) {

    fun uploadFileToAws(path: String, user: User, fileType: FileType, listener: FileProgressListener) {


        //generate file name
        val fileExtension = FilenameUtils.getExtension(path)
        val randomName = UUID.randomUUID()
        val fileName = "${user.uid}/$randomName.$fileExtension"
        Timber.d("Filename = $fileName")
        //metadata setup for aws
        val metadata = ObjectMetadata()
        metadata.addUserMetadata(Constants.Aws.METADATA_MEDIA_TYPE, fileType.type)
        metadata.addUserMetadata(Constants.Aws.METADATA_USER_ID, user.uid)

        fileRepository.uploadFileToAws(path, fileName, fileType, metadata)
                .setTransferListener(object : TransferListener {
                    override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                        Timber.d("byte current = $bytesCurrent , total = $bytesTotal")
                        listener.progress(id, bytesCurrent, bytesTotal)
                    }

                    override fun onStateChanged(id: Int, state: TransferState?) {
                        Timber.d("state = $state")
                        listener.state(id, state)
                    }

                    override fun onError(id: Int, ex: Exception?) {
                        Timber.e("id = $id , Error = $ex")
                        listener.error(id, ex)
                    }
                })
    }

    fun updateUser(type: Category.Type,
                   email: String,
                   firstName: String,
                   lastName: String,
                   screenName: String,
                   website: String,
                   categoryId: Int,
                   address: String,
                   phone: String,
                   about: String,
                   genres: List<String>,
                   skills: List<String>,
                   influences: List<String>,
                   venueType: String): Single<BaseResponse> {

        return userRepository.updateUser(type, email, firstName, lastName, screenName, website, categoryId, address, phone, about, genres, skills, influences, venueType)

    }

    fun getProfile(): Observable<User> {
        return userRepository.getProfile()
    }
}