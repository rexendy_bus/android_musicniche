package com.mymusicniche.features.create_profile.influencers

import com.mymusicniche.common.base.BaseViewModel
import com.mymusicniche.data.Skill
import com.mymusicniche.data.source.repository.InfluencerRepository
import com.mymusicniche.data.source.repository.SkillsRepository
import io.reactivex.Observable
import javax.inject.Inject

class InfluencersViewModel @Inject
constructor(private val repo: InfluencerRepository) : BaseViewModel() {


    override fun getLoadingIndicator(): Observable<Boolean> {
        return Observable.empty()
    }

}