package com.mymusicniche.features.create_profile.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.mymusicniche.R
import kotlinx.android.synthetic.main.item_portfolio.view.*

class PortfolioAdapter(private val portfolios: List<String>) : RecyclerView.Adapter<PortfolioAdapter.PortfolioViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PortfolioViewHolder? {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_portfolio, parent, false)
        return PortfolioViewHolder(view)
    }

    override fun onBindViewHolder(holder: PortfolioViewHolder, position: Int) {
        PortfolioViewHolder.bind(holder, portfolios[position])
    }

    override fun getItemCount(): Int {
        return portfolios.size
    }

    class PortfolioViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        companion object {
            fun bind(holder: PortfolioViewHolder, path: String) {
                Glide.with(holder.itemView.context)
                        .load(path)
                        .into(holder.itemView.imagePortfolio)
            }
        }

    }
}