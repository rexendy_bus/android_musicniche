package com.mymusicniche.features.create_profile.genre

import com.mymusicniche.common.base.BaseViewModel
import com.mymusicniche.data.Genre
import com.mymusicniche.data.source.repository.GenreRepository
import io.reactivex.Observable
import javax.inject.Inject

class GenreViewModel @Inject
constructor(val repository: GenreRepository) : BaseViewModel() {

    fun loadGenres(list: List<Genre>): Observable<List<Genre>> {
        return repository.getGenres()
            .doOnSubscribe { loadingIndicator.onNext(true) }
            .doOnNext { loadingIndicator.onNext(false) }
            .doFinally { loadingIndicator.onNext(false) }
            .flatMapIterable { it }
            .doOnNext { item ->
                if (list.any { it.id == item.id }) {
                    item.selected = true
                }
            }
            .toList().toObservable()
    }

    override fun getLoadingIndicator(): Observable<Boolean> = loadingIndicator
}