package com.mymusicniche.features.create_profile.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.mymusicniche.R

class NotificationUIManager(private val context: Context) {


    object ViewID {
        val PHOTO = 1
        val VIDEO = 2
    }

    enum class Type {
        PHOTO,
        VIDEO
    }

    enum class State {
        COMPLETED,
        FAILED
    }

    private lateinit var mBuilder: NotificationCompat.Builder
    private lateinit var mNotifyManager: NotificationManager

    //https://stackoverflow.com/questions/45395669/notifications-fail-to-display-in-android-oreo-api-26
    fun setupNotification() {

        mNotifyManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel("my_notification_channel", "My Notifications", NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.description = "Channel description"
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            //notificationChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
            //notificationChannel.enableVibration(false)
            mNotifyManager.createNotificationChannel(notificationChannel)
        }

        mBuilder = NotificationCompat.Builder(context, "my_notification_channel")
                //.setVibrate(longArrayOf(10, 20, 30, 40, 50))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Upload")
                .setContentText("Please wait")
                .setOngoing(true)
    }

    fun setContentTitle(type: Type, context: String) {
        mBuilder.setContentTitle(context)
        build(type)
    }

    fun setNotificationComplete(type: Type, state: State) {
        mBuilder.setContentText(if (state == State.COMPLETED) "Upload Complete" else "Upload Failed")
                .setProgress(0, 0, false)
                .setOngoing(false)
                .setAutoCancel(true)
        remove(type)
    }

    fun setNotificationProgress(type: Type, bytesCurrent: Int, bytesTotal: Int) {
        mBuilder.setProgress(bytesTotal, bytesCurrent, false)
        build(type)
    }

    private fun build(type: Type) {
        mNotifyManager.notify(if (type == Type.PHOTO) ViewID.PHOTO else ViewID.VIDEO, mBuilder.build())
    }

    private fun remove(type: Type) {
        mNotifyManager.cancel(if (type == Type.PHOTO) ViewID.PHOTO else ViewID.VIDEO)
    }


}