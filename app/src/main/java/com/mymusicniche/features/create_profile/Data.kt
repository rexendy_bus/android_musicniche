package com.mymusicniche.features.create_profile

import com.mymusicniche.R
import com.mymusicniche.data.poko.Category
import java.util.ArrayList

object Data {

    val profileCategory: List<Category>
        get() {
            var categories = ArrayList<Category>()
            var category = Category()
            category.id = 1
            category.name = "Go Solo"
            category.description = "solo artist"
            category.avatar = R.drawable.category_solo
            category.selected = false
            categories.add(category)

            category = Category()
            category.id = 2
            category.name = "Group"
            category.description = "band or music group"
            category.avatar = R.drawable.category_group
            category.selected = false
            categories.add(category)

            category = Category()
            category.id = 3
            category.name = "Spot"
            category.description = "bars and cafés"
            category.avatar = R.drawable.category_spot
            category.selected = false
            categories.add(category)

            category = Category()
            category.id = 4
            category.name = "Jammer"
            category.description = "musician at heart"
            category.avatar = R.drawable.category_jammer
            category.selected = false
            categories.add(category)

            return categories
        }
}
