package com.mymusicniche.features.create_profile.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mymusicniche.R
import com.mymusicniche.common.ext.show
import com.mymusicniche.data.poko.Category
import com.mymusicniche.features.create_profile.CategoryCallback
import kotlinx.android.synthetic.main.item_profile_category.view.*
import timber.log.Timber

class CreateProfileCategoryAdapter(private val categories: List<Category>, private val callback: CategoryCallback) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder? {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_profile_category, parent, false)
        return CategoryProfileViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        CategoryProfileViewHolder.bind(holder as CategoryProfileViewHolder, categories[position], callback)
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    fun changeSelectedCategory(category: Category) {
        categories.forEach {
            if (it.id != category.id) {
                it.selected = false
            }
            Timber.d("categories = $it")
        }
        notifyDataSetChanged()
    }

    class CategoryProfileViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        companion object {
            fun bind(holder: CategoryProfileViewHolder, category: Category, callback: CategoryCallback) {
                holder.itemView.imageAvatar.setImageResource(category.avatar)
                holder.itemView.tvCategoryName.text = category.name
                holder.itemView.tvCategoryDescription.text = category.description
                holder.itemView.imageCheckMark.visibility = if (category.selected) View.VISIBLE else View.GONE
                holder.itemView.setOnClickListener {
                    if (!category.selected) {
                        category.selected = true
                        holder.itemView.imageCheckMark.show()
                        callback.onCategoryClicked(category)
                    }
                }
            }
        }
    }
}
