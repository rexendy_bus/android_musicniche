package com.mymusicniche.features.create_profile.influencers

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import com.mymusicniche.R
import com.mymusicniche.common.ext.toast
import com.mymusicniche.data.Influence
import com.mymusicniche.features.create_profile.CreateProfileActivity
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_influencers.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class InfluencersActivity : DaggerAppCompatActivity() {

    @Inject lateinit var viewModel: InfluencersViewModel

    private lateinit var adapter: InfluencersAdapter

    private val disposables = CompositeDisposable()

    private var selectedInfluencers = arrayListOf<Influence>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_influencers)
        setupToolbar()

        selectedInfluencers = intent.getParcelableArrayListExtra<Influence>(CreateProfileActivity.EXTRA_INFLUENCER)
        showInfluencerList()
        setupActions()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }


    override fun onBackPressed() {
        val intent = Intent()
        intent.putParcelableArrayListExtra(CreateProfileActivity.EXTRA_INFLUENCER, selectedInfluencers)
        setResult(Activity.RESULT_OK, intent)
        super.onBackPressed()
    }

    private fun setupToolbar() {
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbarTextView.text = getString(R.string.influencers)
        toolbarView.setOnMenuItemClickListener { true }
    }

    private fun showInfluencerList() {
        adapter = InfluencersAdapter(selectedInfluencers)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter
    }

    private fun setupActions() {

        btnAdd.setOnClickListener {
            val influencer = etInfluencers.text.toString()

            if (TextUtils.isEmpty(influencer)) {
                toast("Please add influencer")
                return@setOnClickListener
            }

            selectedInfluencers.add(Influence(influencer))
            adapter.notifyDataSetChanged()
            etInfluencers.setText("")
        }
    }


}