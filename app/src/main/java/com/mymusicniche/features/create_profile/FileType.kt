package com.mymusicniche.features.create_profile

import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import java.lang.Exception

enum class FileType(val type: String) {
    VIDEO("portfolio-video"), PRIMARY("primary"), BACKGROUND("background"), PORTFOLIO("portfolio")
}

interface FileProgressListener {

    fun progress(id: Int, bytesCurrent: Long, bytesTotal: Long)

    fun state(id: Int, state: TransferState?)

    fun error(id: Int, ex: Exception?)

}