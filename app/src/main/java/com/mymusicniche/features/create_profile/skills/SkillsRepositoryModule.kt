package com.mymusicniche.features.create_profile.skills

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.data.source.SkillsSource
import com.mymusicniche.data.source.local.SkillsLocalSource
import com.mymusicniche.data.source.remote.SkillsRemoteSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import dagger.Module
import dagger.Provides

@Module
class SkillsRepositoryModule {

    @Provides
    @Remote
    fun providesSkillsRemoteSource(apiService: ApiService, pref: SharedPreferences): SkillsSource {
        return SkillsRemoteSource(apiService, pref)
    }

    @Provides
    @Local
    fun providesSkillsLocalSource(sharedPref: SharedPreferences): SkillsSource = SkillsLocalSource(sharedPref)
}