package com.mymusicniche.features.create_profile

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@SuppressLint("ParcelCreator")
data class GraphUser(
    val id: String,
    val name: String,
    val email: String
) : Parcelable