package com.mymusicniche.features.create_profile.influencers

import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mymusicniche.R
import com.mymusicniche.data.Influence

class InfluencersAdapter(private val influencers: MutableList<Influence>) : RecyclerView.Adapter<InfluencersAdapter.ViewHolder>() {


    override fun getItemCount(): Int = influencers.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(influencers[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_influencers, parent, false))

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        private var influencerView = view.findViewById<TextView>(R.id.tvInfluencer)
        private var closeView = view.findViewById<AppCompatImageView>(R.id.imageClose)

        fun bind(influence: Influence) {
            influencerView.text = influence.title

            closeView.setOnClickListener {
                influencers.remove(influence)
                notifyDataSetChanged()
            }

        }

    }

}