package com.mymusicniche.features.create_profile

import com.mymusicniche.data.poko.Category

interface CategoryCallback {
    fun onCategoryClicked(category: Category)
}