@file:Suppress("DEPRECATION")

package com.mymusicniche.features.create_profile

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.widget.GridLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.jakewharton.rxbinding2.view.RxView
import com.mymusicniche.R
import com.mymusicniche.common.ext.disable
import com.mymusicniche.common.ext.enable
import com.mymusicniche.common.ext.hide
import com.mymusicniche.common.ext.load
import com.mymusicniche.common.ext.show
import com.mymusicniche.common.ext.toast
import com.mymusicniche.data.Genre
import com.mymusicniche.data.Influence
import com.mymusicniche.data.Skill
import com.mymusicniche.data.User
import com.mymusicniche.data.poko.Category
import com.mymusicniche.features.create_profile.adapter.CreateProfileCategoryAdapter
import com.mymusicniche.features.create_profile.adapter.PortfolioAdapter
import com.mymusicniche.features.create_profile.genre.GenreActivity
import com.mymusicniche.features.create_profile.influencers.InfluencersActivity
import com.mymusicniche.features.create_profile.notification.NotificationUIManager
import com.mymusicniche.features.create_profile.skills.SkillsActivity
import com.mymusicniche.features.feeds.HomeActivity
import com.tbruyelle.rxpermissions2.RxPermissions
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_create_profile.*
import kotlinx.android.synthetic.main.layout_about_me.*
import kotlinx.android.synthetic.main.layout_contact.*
import kotlinx.android.synthetic.main.layout_my_niche.*
import kotlinx.android.synthetic.main.layout_portfolio.*
import kotlinx.android.synthetic.main.layout_profile_category.*
import org.threeten.bp.LocalDateTime
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.Calendar
import javax.inject.Inject
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.custom_help.*
import kotlinx.android.synthetic.main.layout_contact_header.*
import kotlinx.android.synthetic.main.layout_portfolio_header.*

class CreateProfileActivity : DaggerAppCompatActivity(), CreateProfileContract, CategoryCallback {

    private lateinit var rxPermissions: RxPermissions

    private var skills = arrayListOf<Skill>()

    private var genres = arrayListOf<Genre>()

    private var influencers = arrayListOf<Influence>()

    private var portfolios = arrayListOf<String>()

    private lateinit var user: User

    private lateinit var notificationUiManager: NotificationUIManager

    private var fileType = FileType.PRIMARY
    private var userType = Category.Type.SOLO
    private lateinit var categoryAdapter: CreateProfileCategoryAdapter
    private lateinit var portfolioAdapter: PortfolioAdapter
    private lateinit var datePicker: DatePickerDialog
    private var categoryID = 1
    private var isPortfolioUploading: Boolean = false
    private val disposable = CompositeDisposable()
    private var firstName: String? = null
    private var lastName: String? = null
    private var location: String? = null

    companion object {
        private val REQUEST_TAKE_PHOTO = 0
        private val REQUEST_SELECT_IMAGE_IN_ALBUM = 1
        private val REQUEST_SELECT_VIDEO_IN_ALBUM = 2
        private val RC_SKILL = 100
        private val RC_GENRE = 101
        private val RC_INFLUENCER = 102
        private val AVATAR_CLICK = 4
        val EXTRA_GENRES = "extra:genres"
        val EXTRA_SKILLS = "extra:skills"
        val EXTRA_INFLUENCER = "extra:influencer"
        val EXTRA_GRAPH_USER = "extra:graph_user"
        val EXTRA_USER = "extra:user"
        val EXTRA_FIRSTNAME = "extra:first_name"
        val EXTRA_LASTNAME = "extra:last_name"
        val EXTRA_LOCATION = "extra:location"
    }

    @Inject lateinit var viewModel: CreateProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        user = intent.getParcelableExtra(EXTRA_USER)
        setContentView(R.layout.activity_create_profile)
        setUpHelpGuide()
        setUpProfile()
        setupToolbar()
        setupCategory()
        setupPortfolio()
        setupButtonClickListeners()
        setupDatePicker()

        inputScreenName.setText(user.screenName)
        inputEmail.setText(user.email)
    }


    private fun setUpHelpGuide() {
        rel_help_guide.visibility = View.VISIBLE

        lin_profile_data.visibility = View.GONE

        rel_help_guide.setOnClickListener(View.OnClickListener {
            rel_help_guide.visibility = View.GONE
            lin_profile_data.visibility = View.VISIBLE
        })
    }

    override fun onPause() {
        super.onPause()
        disposable.clear()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_SELECT_IMAGE_IN_ALBUM -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.let {
                        setImageBitmap(it)
                    }
                }
            }

            REQUEST_TAKE_PHOTO -> {
                if (resultCode == Activity.RESULT_OK) {
                }
            }

            REQUEST_SELECT_VIDEO_IN_ALBUM -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.let {
                        setVideoData(it)
                    }

                }
            }

            RC_SKILL -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.let {
                        skills = it.getParcelableArrayListExtra<Skill>(EXTRA_SKILLS)
                        inputSkills.setText(skills.asSequence().joinToString { it.title })
                    }

                }
            }

            RC_GENRE -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.let {
                        genres = it.getParcelableArrayListExtra<Genre>(EXTRA_GENRES)
                        inputGenre.setText(genres.asSequence().joinToString { it.name })
                    }

                }
            }

            RC_INFLUENCER -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.let {
                        influencers = it.getParcelableArrayListExtra<Influence>(EXTRA_INFLUENCER)
                        inputMusicInfluencer.setText(influencers.asSequence().joinToString { it.title })
                    }

                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.profile, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menuDone -> {

                if (TextUtils.isEmpty(getEmail())) {
                    toast("Email must not empty")
                    return false
                }

                if (TextUtils.isEmpty(getEmail())) {
                    toast("Email must not be empty")
                    return false
                }

                if (TextUtils.isEmpty(getFirstName()) || TextUtils.isEmpty(getLastName())) {
                    toast("First name or Last name must not be empty")
                    return false
                }

                if (TextUtils.isEmpty(getLocation())) {
                    toast("Address must not be empty")
                    return false
                }

                if (TextUtils.isEmpty(getPhone())) {
                    toast("Contact phone number must not be empty")
                    return false
                }

                val progressDialog = ProgressDialog(this)
                progressDialog.setTitle(getString(R.string.update_profile))
                progressDialog.setMessage(getString(R.string.please_wait))
                progressDialog.show()

                val update = viewModel.updateUser(userType, getEmail(), getFirstName(), getLastName(), getScreenName(), "",
                        categoryID, getLocation(), getPhone(), "", getGenres(), getSkills(), getInfluencers(), "")
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            progressDialog.dismiss()
                            showFeedsActivity()
                        }, {
                            progressDialog.dismiss()
                            toast("Something went wrong! ${it.message}")
                        })
                disposable.add(update)
            }
        }
        return true
    }

    private fun showFeedsActivity() {
        startActivity(Intent(this@CreateProfileActivity, HomeActivity::class.java))
        finish()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbarView)
        supportActionBar?.apply {
            setDisplayShowTitleEnabled(false)
            setHomeButtonEnabled(true)
            title = getString(R.string.profile)
        }
        skills = arrayListOf()
        generatedData()
    }

    private fun generatedData() {
        firstName =  intent.getStringExtra(EXTRA_FIRSTNAME)
        lastName =  intent.getStringExtra(EXTRA_LASTNAME)
        location =  intent.getStringExtra(EXTRA_LOCATION)

        inputFirstName.setText(firstName)
        inputLastName.setText(lastName)
        inputLocation.setText(location)
        inputPhone.setText("")
    }

    private fun setupCategory() {
        recyclerViewCategory.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        categoryAdapter = CreateProfileCategoryAdapter(Data.profileCategory, this)
        recyclerViewCategory.adapter = categoryAdapter
        recyclerViewCategory.setHasFixedSize(true)
    }

    private fun setupPortfolio() {
        recyclerViewPortfolio.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        portfolioAdapter = PortfolioAdapter(portfolios)
        recyclerViewPortfolio.adapter = portfolioAdapter
        recyclerViewPortfolio.setHasFixedSize(true)
    }


    private fun setupButtonClickListeners() {
        rxPermissions = RxPermissions(this@CreateProfileActivity)
        notificationUiManager = NotificationUIManager(this@CreateProfileActivity)
        notificationUiManager.setupNotification()


        RxView.clicks(profileEditImageView)
                .compose(rxPermissions.ensure(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                .subscribe({ granted ->
                    if (granted) {
                        galleryIntent(FileType.PRIMARY)
                    } else {
                        Toast.makeText(this, "At least one permission is denied", Toast.LENGTH_SHORT).show()
                    }
                })

        RxView.clicks(cameraView)
                .compose(rxPermissions.ensure(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                .subscribe({ granted ->
                    if (granted) {
                        galleryIntent(FileType.BACKGROUND)
                    } else {
                        Toast.makeText(this, "At least one permission is denied", Toast.LENGTH_SHORT).show()
                    }
                })

        //SOLO upload video fame
        if (uploadBtn != null) {
            RxView.clicks(uploadBtn)
                    .compose(rxPermissions.ensure(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                    .subscribe({ granted ->
                        if (granted) {
                            takeVideo()
                        } else {
                            Toast.makeText(this, "At least one permission is denied", Toast.LENGTH_SHORT).show()
                        }
                    })
        }

        RxView.clicks(uploadImagePortfolio)
                .compose(rxPermissions.ensure(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                .subscribe({ granted ->
                    if (granted) {
                        galleryIntent(FileType.PORTFOLIO)
                    } else {
                        Toast.makeText(this, "At least one permission is denied", Toast.LENGTH_SHORT).show()
                    }
                })


        inputSkills.setOnClickListener {
            val intent = Intent(this@CreateProfileActivity, SkillsActivity::class.java)
            intent.putParcelableArrayListExtra(EXTRA_SKILLS, skills)
            startActivityForResult(intent, RC_SKILL)
        }

        inputGenre.setOnClickListener {
            val intent = Intent(this@CreateProfileActivity, GenreActivity::class.java)
            intent.putParcelableArrayListExtra(EXTRA_GENRES, genres)
            startActivityForResult(intent, RC_GENRE)
        }

        inputMusicInfluencer.setOnClickListener {
            val intent = Intent(this@CreateProfileActivity, InfluencersActivity::class.java)
            intent.putParcelableArrayListExtra(EXTRA_INFLUENCER, influencers)
            startActivityForResult(intent, RC_INFLUENCER)
        }

        inputBirthdayDate.setOnClickListener { datePicker.show() }

    }

    private fun setupDatePicker() {
        val now = LocalDateTime.now()
        datePicker = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            val newDate = Calendar.getInstance()
            newDate.set(year, monthOfYear, dayOfMonth)
            val simpleDateFormat = SimpleDateFormat("MM/dd/yyyy")
            val formattedDate = simpleDateFormat.format(newDate.time)
            Timber.d("newDate $newDate , simpleDateFormat $simpleDateFormat")
            inputBirthdayDate.setText(formattedDate)
        }, now.year, now.monthValue, now.dayOfMonth)

    }

    private fun galleryIntent(fileType: FileType) {
        this@CreateProfileActivity.fileType = fileType
        val galleryIntent = Intent(Intent.ACTION_GET_CONTENT)
        galleryIntent.type = "image/*"
        if (galleryIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(galleryIntent, REQUEST_SELECT_IMAGE_IN_ALBUM)
        }
    }

    private fun takePhoto() {
        val intent1 = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent1.resolveActivity(packageManager) != null) {
            startActivityForResult(intent1, REQUEST_TAKE_PHOTO)
        }
    }

    private fun takeVideo() {
        val intent = Intent()
        intent.type = "video/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_SELECT_VIDEO_IN_ALBUM)
    }

    private fun setVideoData(intent: Intent) {
        val video: NotificationUIManager.Type = NotificationUIManager.Type.VIDEO
        notificationUiManager.setContentTitle(video, getString(R.string.uploading_video))

        intent.let {
            val url = FilePath.getFilePath(this@CreateProfileActivity, it.data) ?: return
            uploadBtn.disable()
            tvVideoLocation.text = url

            viewModel.uploadFileToAws(url, user, FileType.VIDEO, object : FileProgressListener {
                override fun progress(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                    Log.d("progress", "byte current = $bytesCurrent , total = $bytesTotal")
                    notificationUiManager.setNotificationProgress(video, bytesCurrent.toInt(), bytesTotal.toInt())
                }

                override fun state(id: Int, state: TransferState?) {
                    when (state) {
                        TransferState.COMPLETED -> {
                            uploadBtn.disable()
                            notificationUiManager.setNotificationComplete(video, NotificationUIManager.State.COMPLETED)
                        }
                        TransferState.FAILED, TransferState.CANCELED -> {
                            uploadBtn.enable()
                            tvVideoLocation.text = ""
                            notificationUiManager.setNotificationComplete(video, NotificationUIManager.State.FAILED)
                        }
                        else -> {}
                    }

                }

                override fun error(id: Int, ex: Exception?) {
                    uploadBtn.enable()
                    notificationUiManager.setNotificationComplete(video, NotificationUIManager.State.FAILED)
                }

            })
        }

    }

    private fun setImageBitmap(intent: Intent) {
        intent.let {
            val url = FilePath.getFilePath(this@CreateProfileActivity, it.data) ?: return

            val imageView: Any = when (fileType) {
                FileType.PRIMARY -> {
                    profileImageView
                }
                FileType.BACKGROUND -> {
                    bgImageView
                }
                FileType.PORTFOLIO -> {
                    FileType.PORTFOLIO
                }
                else -> throw IllegalArgumentException(Throwable("No FileType.${fileType.type} found"))
            }

            when (imageView) {
                is ImageView -> {
                    imageView.load(url)
                }
                else -> {
                    val size = portfolios.size
                    if (size >= 10) {
                        toast("Portfolio must not be greater than 10")
                        return
                    }

                    isPortfolioUploading = true
                    portfolios.add(url)
                    portfolioAdapter.notifyDataSetChanged()
                }
            }

            val photo: NotificationUIManager.Type = NotificationUIManager.Type.PHOTO

            notificationUiManager.setContentTitle(photo, getString(R.string.uploading_image))

            viewModel.uploadFileToAws(url, user, fileType, object : FileProgressListener {
                override fun progress(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                    Log.d("progress", "byte current = $bytesCurrent , total = $bytesTotal")
                    notificationUiManager.setNotificationProgress(photo, bytesCurrent.toInt(), bytesTotal.toInt())
                }

                override fun state(id: Int, state: TransferState?) {
                    when (state) {
                        TransferState.COMPLETED -> {
            isPortfolioUploading = false
                            notificationUiManager.setNotificationComplete(photo, NotificationUIManager.State.COMPLETED)
                        }
                        TransferState.FAILED, TransferState.CANCELED -> {
                        isPortfolioUploading = false
                            notificationUiManager.setNotificationComplete(photo, NotificationUIManager.State.FAILED)
                        }
                        else -> {}
                    }

                }

                override fun error(id: Int, ex: Exception?) {
                        isPortfolioUploading = false
                    notificationUiManager.setNotificationComplete(photo, NotificationUIManager.State.FAILED)
                }

            })

        }

        fileType = FileType.PRIMARY

    }

    private fun setUpProfile() {
        relCreaeteProfilePbar.visibility = View.GONE
        linear_category_fields.visibility = View.GONE

        viewModel.getProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError({ Timber.e(it) })
                .subscribe(
                        { mapUser(it) }
                )
    }

    private fun mapUser(user: User) {
        if (user.avatars.size > 0 && user.avatars.get(0).source?.original != null) {
            //profileAvatar.set
            relCreaeteProfilePbar.visibility = View.VISIBLE
            Glide.with(this).load(user.avatars.get(0).source?.original)
                    .listener(object: RequestListener<Drawable> {
                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            relCreaeteProfilePbar.visibility = View.GONE
                            return false
                        }

                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            relCreaeteProfilePbar.visibility = View.GONE
                            return false
                        }

                    }).into(profileImageView)
        }

        if (user.backgrounds.size > 0 && user.backgrounds.get(0).source?.original != null) {
            //profileCover.set
            relCreaeteProfilePbar.visibility = View.VISIBLE
            Glide.with(this).load(user.backgrounds.get(0).source?.original)
                    .listener(object: RequestListener<Drawable>{
                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            relCreaeteProfilePbar.visibility = View.GONE
                            return false
                        }

                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            relCreaeteProfilePbar.visibility = View.GONE
                            return false
                        }

                    }).into(bgImageView)
        }

    }

    override fun onCategoryClicked(category: Category) {
        linear_category_fields.visibility = View.VISIBLE
        con_header_portfolio.visibility = View.VISIBLE
        con_body_portfolio.visibility = View.VISIBLE
        con_header_contact.visibility = View.VISIBLE
        con_body_contact.visibility = View.VISIBLE

        categoryAdapter.changeSelectedCategory(category)
        inputLayoutBirthdayDate.hide()
        inputLayoutSkills.hide()
        tvInstrumentPlayed.hide()
        tvTypeOfMusic.hide()
        tvBands.hide()
        inputLayoutGenre.hide()
        inputLayoutMusicInfluence.hide()
        inputLayoutBarType.hide()
        tvBarType.hide()
        inputLayoutFirstName.hide()
        inputLayoutLastName.hide()
        inputScreenName.hint = getString(R.string.name)
        categoryID = category.id

        when (category.id) {
            Category.Type.GROUP.type -> {
                textViewNameDescription.text = getString(R.string.name_of_your_band)
                inputLayoutGenre.show()
                inputLayoutMusicInfluence.show()
                tvTypeOfMusic.show()
                tvBands.show()
                userType = Category.Type.GROUP
            }
            Category.Type.SPOT.type -> {
                textViewNameDescription.text = getString(R.string.name_of_venue_or_bar)
                inputSaySomething.setText(getString(R.string.something_about_your_bar))
                inputLayoutBarType.show()
                tvBarType.show()
                userType = Category.Type.SPOT
            }
            Category.Type.JAMMER.type -> {
                textViewNameDescription.text = getString(R.string.how_do_u_want_to_be_called)
                inputSaySomething.setText(getString(R.string.something_about_your_music))
                inputLayoutGenre.show()
                tvTypeOfMusic.show()
                con_header_portfolio.visibility = View.GONE
                con_body_portfolio.visibility = View.GONE
                con_header_contact.visibility = View.GONE
                con_body_contact.visibility = View.GONE
                userType = Category.Type.JAMMER
            }
            else -> {
                //SOLO
                inputLayoutBirthdayDate.show()
                inputLayoutSkills.show()
                tvInstrumentPlayed.show()
                inputLayoutGenre.show()
                tvBands.show()
                inputLayoutMusicInfluence.show()
                tvTypeOfMusic.show()
                inputLayoutFirstName.show()
                inputLayoutLastName.show()
                textViewNameDescription.text = getString(R.string.how_do_u_want_to_be_called)
                inputScreenName.hint = getString(R.string.screen_name)
                userType = Category.Type.SOLO
            }
        }
    }

    override fun getScreenName(): String = inputScreenName.text.toString()

    override fun getEmail(): String = inputEmail.text.toString()

    override fun getFirstName(): String = inputFirstName.text.toString()

    override fun getLastName(): String = inputLastName.text.toString()

    override fun getBandName(): String = inputScreenName.text.toString()

    override fun getLocation(): String = inputLocation.text.toString()

    override fun getPhone(): String = inputPhone.text.toString()

    override fun getBirthdate(): String = inputBirthdayDate.text.toString()

    override fun getSkills(): List<String> = skills.map { it.title }

    override fun getGenres(): List<String> = genres.map { it.name }

    override fun getInfluencers(): List<String> = influencers.map { it.title }

    override fun getSaySomething(): String = inputSaySomething.text.toString()


}
