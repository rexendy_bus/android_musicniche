package com.mymusicniche.features.create_profile

import android.content.SharedPreferences
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.mymusicniche.api.ApiService
import com.mymusicniche.data.source.FileSource
import com.mymusicniche.data.source.UserSource
import com.mymusicniche.data.source.local.AppDatabase
import com.mymusicniche.data.source.local.UserLocalSource
import com.mymusicniche.data.source.remote.FileRemoteSource
import com.mymusicniche.data.source.remote.UserRemoteSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import dagger.Module
import dagger.Provides

@Module
class CreateProfileRepositoryModule {

    @Provides
    @Remote
    fun providesCreateProfileFileRemoteSource(transferUtility: TransferUtility): FileSource {
        return FileRemoteSource(transferUtility)
    }

    @Provides
    @Remote
    fun providesCreateProfileRemoteSource(apiService: ApiService,
        preferences: SharedPreferences, db: AppDatabase): UserSource {
        return UserRemoteSource(apiService, preferences, db)
    }

    @Provides
    @Local
    fun providesCreateProfileLocaleSource(sharedPref: SharedPreferences, db: AppDatabase): UserSource {
        return UserLocalSource(sharedPref, db)
    }
}