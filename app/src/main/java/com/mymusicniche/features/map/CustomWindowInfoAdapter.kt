package com.mymusicniche.features.map

import android.app.Activity
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import android.widget.ImageView
import android.widget.TextView
import com.mymusicniche.R

class CustomWindowInfoAdapter(private val context: Activity) : GoogleMap.InfoWindowAdapter {

    private var mWindow = context.layoutInflater.inflate(R.layout.custom_info_window, null)

    private var mContents =
        context.layoutInflater.inflate(R.layout.custom_info_content, null)

    override fun getInfoWindow(marker: Marker): View? {
        render(marker, mWindow)
        return mWindow
    }

    override fun getInfoContents(marker: Marker): View? {
        render(marker, mContents)
        return mContents
    }

    private fun render(marker: Marker, view: View) {
        // Use the equals() method on a Marker to check for equals.  Do not use ==.
        (view.findViewById<View>(R.id.badge) as ImageView).setImageResource(R.mipmap.ic_launcher_round)

        val title = marker.title
        val titleUi = view.findViewById<View>(R.id.title) as TextView
//        if (title != null) {
//            // Spannable string allows us to edit the formatting of the text.
//            val titleText = SpannableString(title)
//            titleText.setSpan(ForegroundColorSpan(Color.RED), 0, titleText.length, 0)
//            titleUi.text = titleText
//        } else {
//            titleUi.text = ""
//        }
//        val snippet = marker.snippet
//        val snippetUi = view.findViewById<View>(R.id.description) as TextView
//        if (snippet != null && snippet.length > 12) {
//            val snippetText = SpannableString(snippet)
//            snippetText.setSpan(ForegroundColorSpan(Color.MAGENTA), 0, 10, 0)
//            snippetText.setSpan(ForegroundColorSpan(Color.BLUE), 12, snippet.length, 0)
//            snippetUi.text = snippetText
//        } else {
//            snippetUi.text = ""
//        }
    }
}