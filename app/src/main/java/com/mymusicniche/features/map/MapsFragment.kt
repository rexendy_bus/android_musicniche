package com.mymusicniche.features.map

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.maps.OnMapReadyCallback
import com.mymusicniche.R
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_maps.*
import kotlinx.android.synthetic.main.include_maps_date.*
import org.threeten.bp.LocalDateTime
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class MapsFragment : Fragment(), OnMapReadyCallback {

    private lateinit var googleMap: GoogleMap
    private lateinit var datePicker: DatePickerDialog

    companion object {
        fun newInstance(): MapsFragment {
            val fragment = MapsFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, state: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_maps, parent, false)
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupDatePicker()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap

//        this.googleMap.addMarker(
//            MarkerOptions().position(LatLng(0.0, 0.0)).title("Marker")
//        )
        this.googleMap.addMarker(
            MarkerOptions().position(LatLng(10.260871, 31.464844)).title("Marker 1").icon(
                BitmapDescriptorFactory.fromResource(android.R.drawable.ic_menu_mapmode)
            )
        )
        this.googleMap.addMarker(
            MarkerOptions().position(LatLng(1.878326, 27.949219)).title("Marker 2").icon(
                BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_round)
            )
        )
        this.googleMap.addMarker(
            MarkerOptions().position(LatLng(16.077486, 8.613281)).title("Marker 3").icon(
                BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_round)
            )
        )
        this.googleMap.addMarker(
            MarkerOptions().position(LatLng(-17.528821, 20.917969)).title("Marker 4").icon(
                BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_round)
            )
        )

        googleMap.setInfoWindowAdapter(CustomWindowInfoAdapter(this.activity as Activity))
    }

    private fun setupDatePicker() {
        val now = LocalDateTime.now()
        datePicker = DatePickerDialog(
            activity,
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate.set(year, monthOfYear, dayOfMonth)
                val simpleDateFormat = SimpleDateFormat("MM/dd/yyyy")
                val formattedDate = simpleDateFormat.format(newDate.time)
                Timber.d("newDate $newDate , simpleDateFormat $simpleDateFormat")
                date_time.text = formattedDate
            },
            now.year,
            now.monthValue,
            now.dayOfMonth
        )

        container.setOnClickListener { datePicker.show() }
    }


}