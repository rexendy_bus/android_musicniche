package com.mymusicniche.features.notifications

import com.onesignal.OSNotification
import com.onesignal.OneSignal
import timber.log.Timber

class NotificationReceivedHandler : OneSignal.NotificationReceivedHandler {

    override fun notificationReceived(notification: OSNotification) {

        val jsonObject = notification.payload.additionalData

        Timber.d("notification_message: $jsonObject")
    }
}