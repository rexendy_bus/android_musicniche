package com.mymusicniche.features.login

import android.content.SharedPreferences
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.mymusicniche.api.ApiService
import com.mymusicniche.data.source.FileSource
import com.mymusicniche.data.source.LoginSource
import com.mymusicniche.data.source.local.AppDatabase
import com.mymusicniche.data.source.local.LoginLocalSource
import com.mymusicniche.data.source.remote.FileRemoteSource
import com.mymusicniche.data.source.remote.LoginRemoteSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import dagger.Module
import dagger.Provides

@Module
class LoginRepositoryModule {

    @Provides
    @Remote
    fun providesLoginRemoteSource(apiService: ApiService, pref: SharedPreferences): LoginSource {
        return LoginRemoteSource(apiService, pref)
    }

    @Provides
    @Local
    fun providesLoginLocalSource(pref: SharedPreferences, db: AppDatabase): LoginSource {
        return LoginLocalSource(pref, db)
    }

    @Provides
    @Remote
    fun providesLoginFileRemoteSource(transferUtility: TransferUtility): FileSource {
        return FileRemoteSource(transferUtility)
    }
}
