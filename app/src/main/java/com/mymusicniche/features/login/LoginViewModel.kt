package com.mymusicniche.features.login

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.services.s3.model.ObjectMetadata
import com.facebook.AccessToken
import com.facebook.GraphRequest.GraphJSONObjectCallback
import com.mymusicniche.common.Constants
import com.mymusicniche.common.base.BaseViewModel
import com.mymusicniche.data.User
import com.mymusicniche.data.poko.Credential
import com.mymusicniche.data.poko.Device
import com.mymusicniche.data.source.repository.FileRepository
import com.mymusicniche.data.source.repository.LoginRepository
import com.mymusicniche.features.create_profile.FileProgressListener
import com.mymusicniche.features.create_profile.FileType
import io.reactivex.Completable
import io.reactivex.Observable
import org.apache.commons.io.FilenameUtils
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class LoginViewModel @Inject
constructor(private val fileRepository: FileRepository,private val repository: LoginRepository) : BaseViewModel() {

    fun attemptLogin(
        username: String,
        password: String,
        firstName: String,
        lastName: String,
        platform: String,
        playerId: String,
        deviceName: String
    ): Observable<User> {

        // TODO checking here
        val credential = Credential(
            username = username,
            password = password,
            firstName = firstName,
            lastName = lastName
        )

        val device = Device(
            platform = platform,
            playerId = playerId,
            name = deviceName
        )

        return repository.login(credential, device)
            .doOnSubscribe { loadingIndicator.onNext(true) }
            .flatMap { repository.getProfile() }
            .doOnNext { loadingIndicator.onNext(false) }
            .doFinally { loadingIndicator.onNext(false) }
    }

    fun graphRequest(accessToken: AccessToken, callback: GraphJSONObjectCallback): Completable {
        return repository.graphRequest(accessToken, callback)
    }

    override fun getLoadingIndicator(): Observable<Boolean> = loadingIndicator

    fun uploadFileToAws(path: String, user: User, fileType: FileType, listener: FileProgressListener) {
        //generate file name
        var fileExtension = FilenameUtils.getExtension(path)
        if (fileExtension.isEmpty() || fileExtension == null) {
            fileExtension = "jpg"
        }
        val randomName = UUID.randomUUID()
        val fileName = "${user.uid}/$randomName.$fileExtension"
        Timber.d("Filename = $fileName")
        //metadata setup for aws
        val metadata = ObjectMetadata()
        metadata.addUserMetadata(Constants.Aws.METADATA_MEDIA_TYPE, fileType.type)
        metadata.addUserMetadata(Constants.Aws.METADATA_USER_ID, user.uid)

        fileRepository.uploadFileToAws(path, fileName, fileType, metadata)
                .setTransferListener(object : TransferListener {
                    override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                        Timber.d("byte current = $bytesCurrent , total = $bytesTotal")
                        listener.progress(id, bytesCurrent, bytesTotal)
                    }

                    override fun onStateChanged(id: Int, state: TransferState?) {
                        Timber.d("state = $state")
                        listener.state(id, state)
                    }

                    override fun onError(id: Int, ex: Exception?) {
                        Timber.e("id = $id , Error = $ex")
                        listener.error(id, ex)
                    }
                })
    }
}