package com.mymusicniche.features.login

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest.GraphJSONObjectCallback
import com.facebook.GraphResponse
import com.facebook.login.LoginResult
import com.google.gson.Gson
import com.mymusicniche.R
import com.mymusicniche.common.ext.hide
import com.mymusicniche.common.ext.show
import com.mymusicniche.common.ext.toBoolean
import com.mymusicniche.data.User
import com.mymusicniche.features.create_profile.CreateProfileActivity
import com.mymusicniche.features.create_profile.FileProgressListener
import com.mymusicniche.features.create_profile.FileType
import com.mymusicniche.features.create_profile.GraphUser
import com.mymusicniche.features.feeds.HomeActivity
import com.mymusicniche.features.profile.ProfileFragment
import com.mymusicniche.features.walkthrough.WalkthroughActivity
import com.onesignal.OSSubscriptionObserver
import com.onesignal.OSSubscriptionStateChanges
import com.onesignal.OneSignal
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.activity_login.*
import org.apache.commons.io.FilenameUtils
import org.json.JSONObject
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import javax.inject.Inject

class LoginActivity : DaggerAppCompatActivity(), GraphJSONObjectCallback, OSSubscriptionObserver {

    @Inject
    lateinit var viewModel: LoginViewModel

    private lateinit var callbackManager: CallbackManager

    /**
     * OneSignal player_id/user_id
     */
    private var playerId = ""

    private val graphUserStream = BehaviorSubject.create<GraphUser>()

    private val playerIdStream = BehaviorSubject.create<Boolean>()

    private val disposables = CompositeDisposable()

    private var firstName: String? = null

    private var lastName: String? = null

    private var location: String? = null

    /**
     * for profile and cover photo
     */
    private var fileCreateProfile= File("temp.jpg")

    private var fileCreateCover= File("temp.jpg")

    private var hasProfilePhoto: Boolean = false

    private var hasCoverPhoto: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        graphUserStream.onNext(GraphUser("", "", ""))
        playerIdStream.onNext(false)
        ProfileFragment.CURRENT_USER_ID = ""

        callbackManager = CallbackManager.Factory.create()

        facebookBtn.setReadPermissions(mutableListOf("email", "public_profile", "user_location"))
        facebookBtn.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                result?.accessToken?.let {
                    disposables.add(
                        viewModel.graphRequest(it, this@LoginActivity)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnError({ Timber.e(it) })
                            .subscribe()
                    )
                }
            }

            override fun onCancel() {}
            override fun onError(error: FacebookException?) {}
        })

        OneSignal.addSubscriptionObserver(this)
    }

    override fun onOSSubscriptionChanged(stateChanges: OSSubscriptionStateChanges) {
        if (!stateChanges.from.subscribed && stateChanges.to.subscribed) {
            playerId = stateChanges.to.userId
        }
        playerIdStream.onNext(playerId.isNotEmpty())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCompleted(json: JSONObject?, response: GraphResponse?) {
        try {
            if (json?.has("location")!!) {
                location = json?.getJSONObject("location").getString("name")
            }

            if (json?.has("first_name")!!) {
                firstName = json?.getString("first_name")
            }

            if (json?.has("last_name")!!) {
                lastName = json?.getString("last_name")
            }

            if (json?.has("picture")!!) {
                hasProfilePhoto = true
                val profPic = json?.getJSONObject("picture").getJSONObject("data").getString("url")

                fileCreateProfile = File(cacheDir, getFile(profPic))
                fileCreateProfile.createNewFile()
                writeFile(fileCreateProfile, profPic)
            }

            if (json?.has("cover")) {
                hasCoverPhoto = true
                var coverPic = json?.getJSONObject("cover").getString("source")

                fileCreateCover = File(cacheDir, getFile(coverPic))
                fileCreateCover.createNewFile()
                writeFile(fileCreateCover, coverPic)
            }

            val graphUser = Gson().fromJson(json.toString(), GraphUser::class.java)
            graphUserStream.onNext(graphUser)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    private fun getFile(path: String): String? {
        val filename = FilenameUtils.getName(path)
        return  filename.substring(filename.lastIndexOf("=") + 1)
    }

    private fun writeFile(fileData: File, path: String) {
        val url = URL(path)
        val bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())

        //Convert bitmap to byte array
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos)
        val bitmapdata = bos.toByteArray()

        //write the bytes in file
        val fos = FileOutputStream(fileData)
        fos.write(bitmapdata)
        fos.flush()
        fos.close()
    }

    private fun loginViaFacebook(graphUser: GraphUser?) {
        graphUser?.let {
            disposables.add(viewModel.attemptLogin(
                username = it.email,
                password = it.id,
                firstName = firstName!!,
                lastName = lastName!!,
                platform = "Android",
                playerId = playerId,
                deviceName = android.os.Build.MODEL)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    checkTerms(it)
                }, { Timber.e(it) })
            )
        }
    }

    private fun checkTerms(user: User) {
        ProfileFragment.CURRENT_USER_ID = user.uid
        if (hasProfilePhoto) {
            uploadPhoto(fileCreateProfile, user, FileType.PRIMARY)
        }

        if (hasCoverPhoto) {
            uploadPhoto(fileCreateCover, user, FileType.BACKGROUND)
        }
    }

    private fun uploadPhoto(file: File, user: User, fileType: FileType) {
        viewModel.uploadFileToAws(file.absolutePath, user, fileType, object : FileProgressListener {
            override fun progress(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                showLoadingIndicator(true)
            }

            override fun state(id: Int, state: TransferState?) {
                when (state) {
                    TransferState.COMPLETED -> {
                        if ((file == fileCreateProfile && hasCoverPhoto == false) || (file == fileCreateCover)) {
                            showLoadingIndicator(false)
                            if (user.acceptedTerms.toBoolean()) gotoHomeScreen() else gotoWalkthroughScreen(user)
                        }

                    }
                    TransferState.FAILED, TransferState.CANCELED -> {
                        Timber.d("upload", " failed ")
                    }
                    else -> {}
                }

            }

            override fun error(id: Int, ex: Exception?) {
                Timber.d("upload", " exception  : " + ex?.message.toString())
            }

        })

    }

    private fun gotoHomeScreen() {
        startActivity(Intent(this, HomeActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        })
        finish()
    }

    private fun gotoWalkthroughScreen(user: User) {
        startActivity(Intent(this, WalkthroughActivity::class.java).apply {
            putExtra(CreateProfileActivity.EXTRA_USER, user)
            putExtra(CreateProfileActivity.EXTRA_FIRSTNAME, firstName)
            putExtra(CreateProfileActivity.EXTRA_LASTNAME, lastName)
            putExtra(CreateProfileActivity.EXTRA_LOCATION, location)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        })
        finish()
    }

    override fun onStart() {
        super.onStart()
        OneSignal.setSubscription(true)

        val status = OneSignal.getPermissionSubscriptionState()
        if (status.subscriptionStatus.subscribed) {
            playerId = status.subscriptionStatus.userId
            playerIdStream.onNext(playerId.isNotEmpty())
        }

        bindGraphUserAndPlayerId()
        bindLoadingIndicator()
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    private fun bindGraphUserAndPlayerId() {
        disposables.add(
            Observable.combineLatest(
                graphUserStream,
                playerIdStream,
                BiFunction<GraphUser, Boolean, GraphUser> { graphUser, playerIdNotEmpty ->
                    if (playerIdNotEmpty) graphUser else GraphUser("", "", "")
                })
                .subscribe({
                    if (it.id.isNotEmpty()) loginViaFacebook(it)
                })
        )
    }

    private fun bindLoadingIndicator() {
        disposables.add(viewModel.getLoadingIndicator()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ showLoadingIndicator(it) })
        )
    }

    private fun showLoadingIndicator(showing: Boolean) {
        if (showing) loadingView.show() else loadingView.hide()
    }
}
