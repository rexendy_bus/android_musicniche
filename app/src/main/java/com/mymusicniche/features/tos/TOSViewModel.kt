package com.mymusicniche.features.tos

import com.mymusicniche.api.response.BaseResponse
import com.mymusicniche.common.base.BaseViewModel
import com.mymusicniche.data.source.repository.UserRepository
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class TOSViewModel @Inject
constructor(private val repository: UserRepository) : BaseViewModel() {

    fun acceptTerms(): Single<BaseResponse> {
        return repository.acceptTerms()
    }

    override fun getLoadingIndicator(): Observable<Boolean> {
        return Observable.just(true)
    }

}