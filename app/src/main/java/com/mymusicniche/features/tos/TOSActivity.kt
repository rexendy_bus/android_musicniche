package com.mymusicniche.features.tos

import android.content.Intent
import android.os.Bundle
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.mymusicniche.R
import com.mymusicniche.common.Constants
import com.mymusicniche.common.ext.disable
import com.mymusicniche.common.ext.enable
import com.mymusicniche.common.ext.invisible
import com.mymusicniche.common.ext.show
import com.mymusicniche.data.User
import com.mymusicniche.features.create_profile.CreateProfileActivity
import com.mymusicniche.features.create_profile.CreateProfileActivity.Companion.EXTRA_USER
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_tos.*
import kotlinx.android.synthetic.main.layout_loading.*
import timber.log.Timber
import javax.inject.Inject

class TOSActivity : DaggerAppCompatActivity() {

    private lateinit var user: User
    private var fName: String? = null
    private var lName: String? = null
    private var location: String? = null

    @Inject lateinit var viewModel: TOSViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tos)
        user = intent.getParcelableExtra(EXTRA_USER)
        fName = intent.getStringExtra(CreateProfileActivity.EXTRA_FIRSTNAME)
        lName = intent.getStringExtra(CreateProfileActivity.EXTRA_LASTNAME)
        location = intent.getStringExtra(CreateProfileActivity.EXTRA_LOCATION)
        initialize()
    }

    private fun initialize() {
        val webview = findViewById<WebView>(R.id.webView)

        webview.settings.loadWithOverviewMode = true
        webview.settings.useWideViewPort = true
        webview.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                progressView.show()
                btnAgree.disable()
                view?.loadUrl(Constants.Url.TOS)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                btnAgree.enable()
                progressView.invisible()
            }
        }
        webview.loadUrl(Constants.Url.TOS)

        btnAgree.setOnClickListener {
            viewModel.acceptTerms()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ showCreateProfileActivity() }, { Timber.e(it) })
        }
    }

    private fun showCreateProfileActivity() {
        val intent = Intent(this@TOSActivity, CreateProfileActivity::class.java)
        intent.putExtra(EXTRA_USER, user)
        intent.putExtra(CreateProfileActivity.EXTRA_FIRSTNAME, fName)
        intent.putExtra(CreateProfileActivity.EXTRA_LASTNAME, lName)
        intent.putExtra(CreateProfileActivity.EXTRA_LOCATION, location)
        startActivity(intent)
        finish()
    }

}