package com.mymusicniche.features.tos

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.data.source.UserSource
import com.mymusicniche.data.source.local.AppDatabase
import com.mymusicniche.data.source.local.UserLocalSource
import com.mymusicniche.data.source.remote.UserRemoteSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import dagger.Module
import dagger.Provides

@Module
class TOSRepositoryModule {

    @Provides
    @Remote
    fun providesUserRemoteSource(apiService: ApiService, pref: SharedPreferences, db: AppDatabase): UserSource {
        return UserRemoteSource(apiService, pref, db)
    }

    @Provides
    @Local
    fun providesUserLocalSource(pref: SharedPreferences, db: AppDatabase): UserSource {
        return UserLocalSource(pref, db)
    }
}