package com.mymusicniche.features.profile.Setting.ProfileSetting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import com.mymusicniche.R
import com.mymusicniche.data.User
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_profile_setting.*
import timber.log.Timber
import javax.inject.Inject

class ProfileSettingFragment : DaggerFragment(), CompoundButton.OnCheckedChangeListener {
    @Inject lateinit var viewModel: ProfileSettingViewModel

    var notifications: List<String> = ArrayList<String>()
    var privacy: String = "private"
    var isFirstLoad: Boolean = true

    companion object {
        fun newInstance(): ProfileSettingFragment {
            val fragment = ProfileSettingFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, state: Bundle?): View? {
        var root: View = inflater.inflate(R.layout.fragment_profile_setting, parent, false)

        return root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        relProfileSettingPbar.visibility = View.VISIBLE

        viewModel.getProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError({ Timber.e(it) })
                .subscribe(
                        { mapUser(it) }
                )
    }

    private fun mapUser(user: User) {
        relProfileSettingPbar.visibility = View.GONE
        privacy = user.privacy
        notifications = user.notificationTypes
        initializeAllToggle()
    }

    private fun initializeAllToggle() {
        tglPushNotif.setOnCheckedChangeListener(this)
        tglReactPost.setOnCheckedChangeListener(this)
        tglUpcomingEvent.setOnCheckedChangeListener(this)
        tglCommentPost.setOnCheckedChangeListener(this)
        tglPrivacySetting.setOnCheckedChangeListener(this)

        tglPushNotif.isChecked = false
        tglReactPost.isChecked = false
        tglUpcomingEvent.isChecked = false
        tglCommentPost.isChecked = false
        tglPrivacySetting.isChecked = false

        linNotifOptions.visibility = View.GONE
        if (notifications.size > 0) {
            tglPushNotif.isChecked = true
            linNotifOptions.visibility = View.VISIBLE
        }

        if (privacy.toLowerCase().equals("public")) {
            tglPrivacySetting.isChecked = true
        }

        if (notifications.contains("reactions")) {
            tglReactPost.isChecked = true
        }

        if (notifications.contains("events")) {
            tglUpcomingEvent.isChecked = true
        }

        if (notifications.contains("comments")) {
            tglCommentPost.isChecked = true
        }
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        try {
            if (isFirstLoad == false) {
                relProfileSettingPbar.visibility = View.VISIBLE
                var offValue = "off"
                var privacy = "private"

                if (buttonView == tglPushNotif) {
                    linNotifOptions.visibility = View.GONE
                    if (isChecked == true) {
                        offValue = "on"
                        linNotifOptions.visibility = View.VISIBLE
                        tglReactPost.isChecked = true
                        tglUpcomingEvent.isChecked = true
                        tglCommentPost.isChecked = true
                    }

                    viewModel.updateNotif(offValue, null)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ closeLoading() }, { Timber.e(it) })
                } else if (buttonView == tglReactPost) {
                    if (isChecked == true) {
                        offValue = "on"
                    }
                    viewModel.updateNotif(offValue, "reactions")
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ closeLoading() }, { Timber.e(it) })
                } else if (buttonView == tglUpcomingEvent) {
                    if (isChecked == true) {
                        offValue = "on"
                    }
                    viewModel.updateNotif(offValue, "events")
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ closeLoading() }, { Timber.e(it) })
                } else if (buttonView == tglCommentPost) {
                    if (isChecked == true) {
                        offValue = "on"
                    }
                    viewModel.updateNotif(offValue, "comments")
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ closeLoading() }, { Timber.e(it) })
                } else if (buttonView == tglPrivacySetting) {
                    if (isChecked == true) {
                        privacy = "public"
                    }
                    viewModel.updatePrivacy(privacy)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ closeLoading() }, { Timber.e(it) })
                }
            } else {
                isFirstLoad = false
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun closeLoading() {
        if (relProfileSettingPbar != null) {
            relProfileSettingPbar.visibility = View.GONE
        }
    }
}