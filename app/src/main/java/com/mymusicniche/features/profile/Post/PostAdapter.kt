package com.mymusicniche.features.profile.Post

import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mymusicniche.R
import com.mymusicniche.common.ext.load
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_post.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class PostAdapter(private val posts: MutableList<DisplayablePost>, private val postFragment: PostFragment, private val isMine: Boolean) : RecyclerView.Adapter<PostAdapter.ViewHolder>() {

    public var postData = posts
    public var isCurrentUser = isMine

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(postData[position])

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_post, parent, false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int = postData.size

    fun add(position: Int, item: DisplayablePost) {
        postData.add(position, item)
        notifyItemChanged(position)
    }

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(data: DisplayablePost) {
            try {
                containerView.nameView.visibility = View.GONE
                containerView.descriptionView.text = data.content

                if (isTodayDate(data.createdAt) == true) {
                    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    val todayDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(Date())


                    val date1 = simpleDateFormat.parse(data.createdAt)
                    val date2 = simpleDateFormat.parse(todayDate)
                    val timeObj = getDifference(date1, date2)
                    var hrString: String = timeObj.hour + " hours ago"

                    if (timeObj.hour.length > 0) {
                        if (timeObj.hour.equals("1")) {
                            hrString = "1 hour ago"
                        }
                    } else if (timeObj.minutes.length > 0) {
                        hrString = timeObj.minutes +  " minutes ago"
                        if (timeObj.minutes.equals("1")) {
                            hrString = "1 minute ago"
                        }
                    } else if (timeObj.seconds.length > 0) {
                        hrString = timeObj.seconds + " seconds ago"
                        if (timeObj.seconds.equals("1")) {
                            hrString = "1 second ago"
                        }
                    }

                    containerView.minsAgoView.text = hrString

                } else {
                    containerView.minsAgoView.text = data.createdAt
                }


                containerView.datetimeView.text = data.eventDate

                containerView.eventImgView.visibility = View.GONE
                if (data.cover != null && data.cover.length > 0) {
                    containerView.eventImgView.visibility = View.VISIBLE
                    containerView.eventImgView.load(data.cover)
                }

                if (isCurrentUser == true || data.isCurrentUser == true) {
                    containerView.imgPostMenu.setOnClickListener{
                        val popUpMenu = PopupMenu(containerView.context, it)
                        popUpMenu.setOnMenuItemClickListener { item ->
                            when(item.itemId){
                                R.id.menuPostEdit -> {
                                    Log.d("menupost", " : edit :")
                                    true
                                }
                                R.id.menuPostPublishUnPublish -> {
                                    Log.d("menupost", " : publish")
                                    true
                                }
                                R.id.menuPostDelete -> {
                                    Log.d("menupost", " : delete : " + data.id + " : " + position)
                                    postFragment.deletePost(data.id, position)
                                    true
                                }
                                else -> false
                            }
                        }
                        popUpMenu.inflate(R.menu.post_menu)
                        popUpMenu.show()
                    }
                } else {
                    containerView.imgPostMenu.setOnClickListener{
                        val popUpMenu = PopupMenu(containerView.context, it)
                        popUpMenu.setOnMenuItemClickListener { item ->
                            when(item.itemId){
                                R.id.menuPostReport -> {
                                    Log.d("menupost", " : report :")
                                    true
                                }
                                else -> false
                            }
                        }
                        popUpMenu.inflate(R.menu.user_profile)
                        popUpMenu.show()
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    fun isTodayDate(dateTime: String) : Boolean {
        val todayDate = Date()

        try {
            // convert param to date
            val dateFormatString = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val dateString = dateFormatString.parse(dateTime)

            // get the date from converted data with the format y-m-d
            val paramDateString = SimpleDateFormat("yyyy-MM-dd").format(dateString)

            // get the date from todays date with the format y-m-d
            val todayDateString = SimpleDateFormat("yyyy-MM-dd").format(todayDate)

            if (todayDateString == paramDateString) {
                return true
            }

        } catch (e: ParseException) {
            e.printStackTrace()
        }


        return false
    }

    fun getDifference(startDate: Date, endDate: Date) : TimeDiff {
        var timeDiff = TimeDiff("1", "20", "0")

        //milliseconds
        var different = endDate.time - startDate.time

        val secondsInMilli: Long = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val daysInMilli = hoursInMilli * 24

        val elapsedDays = different / daysInMilli
        different = different % daysInMilli

        val elapsedHours = different / hoursInMilli
        different = different % hoursInMilli

        val elapsedMinutes = different / minutesInMilli
        different = different % minutesInMilli

        val elapsedSeconds = different / secondsInMilli

        timeDiff = TimeDiff(elapsedHours.toString(), elapsedMinutes.toString(), elapsedSeconds.toString())

        return timeDiff
    }
}

class TimeDiff (
    var hour: String,
    var minutes: String,
    var seconds: String
)