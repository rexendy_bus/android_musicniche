package com.mymusicniche.features.profile.View

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import com.mymusicniche.R
import com.mymusicniche.common.Constants
import com.mymusicniche.common.ext.add
import dagger.android.support.DaggerAppCompatActivity

class UserProfileViewActivity : DaggerAppCompatActivity() {

    lateinit var toolbar: Toolbar;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile_view)

        toolbar = findViewById(R.id.toolbarView)
        setSupportActionBar(toolbar);

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()?.setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                onBackPressed()
            }

        })

        var uid = intent.getStringExtra(Constants.Key.USER_UUID)
        add (R.id.frameLayoutProfileView, UserProfileViewFragment.newInstance(uid))
    }
}