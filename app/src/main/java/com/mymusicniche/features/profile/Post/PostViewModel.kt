package com.mymusicniche.features.profile.Post

import com.mymusicniche.api.request.PostDeleteParam
import com.mymusicniche.api.request.UserFeedsParam
import com.mymusicniche.data.source.repository.UserFeedsRepository
import io.reactivex.Observable
import javax.inject.Inject

class PostViewModel @Inject
constructor(private val repository: UserFeedsRepository) {

    fun loadUserFeeds(page: Int, perPage: Int, userID: String?): Observable<MutableList<DisplayablePost>> {

        val params = UserFeedsParam(page, perPage, userID)

        return repository.getUserFeeds(params)
                .flatMapIterable {
                    it.results
                }
                .map {
                    DisplayablePost(
                            id = it.id,
                            content = it.content,
                            firstName = it.user.firstName,
                            lastName = it.user.lastName,
                            screenName = it.user.screenName,
                            eventDate = it.eventDate,
                            createdAt = it.createdAt,
                            userUID = it.user.uid,
                            location = it.location?.fullAddress,
                            status = it.status.name,
                            cover = it.cover?.source?.original,
                            isCurrentUser = false
                    )
                }.toList().toObservable()
    }

    fun deletePost (postID: Int): Observable<Int> {
        val params = PostDeleteParam(postID)

        return repository.deleteUserFeed(params)
                .map { it.status.toInt() }
    }
}