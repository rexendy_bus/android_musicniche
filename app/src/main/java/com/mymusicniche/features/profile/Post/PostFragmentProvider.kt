package com.mymusicniche.features.profile.Post

import com.mymusicniche.features.feeds.events.EventFeedsModule
import com.mymusicniche.features.profile.Portfolio.PortfolioFragment
import com.mymusicniche.features.profile.ProfileModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by rexenjeandy on 3/10/18.
 */
@Module
abstract class PostFragmentProvider {

    @ContributesAndroidInjector(modules = [UserFeedsModule::class])
    abstract fun providesPostFragmentFactory(): PostFragment
}