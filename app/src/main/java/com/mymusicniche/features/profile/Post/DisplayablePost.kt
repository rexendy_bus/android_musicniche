package com.mymusicniche.features.profile.Post

class DisplayablePost (
        val id: Int,
        val content: String,
        val eventDate: String?,
        val firstName: String,
        val lastName: String,
        val screenName: String?,
        val createdAt: String,
        val userUID: String,
        val location: String?,
        val status: String?,
        val cover: String?,
        var isCurrentUser: Boolean

)