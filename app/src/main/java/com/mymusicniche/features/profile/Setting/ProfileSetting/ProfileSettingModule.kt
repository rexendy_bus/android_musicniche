package com.mymusicniche.features.profile.Setting.ProfileSetting

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.data.source.ProfileSettingSource
import com.mymusicniche.data.source.local.AppDatabase
import com.mymusicniche.data.source.local.ProfileSettingLocalSource
import com.mymusicniche.data.source.remote.ProfileSettingRemoteSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import dagger.Module
import dagger.Provides

@Module
class ProfileSettingModule {
    @Provides
    @Remote
    fun providesProfileSettingRemoteSource(api: ApiService, pref: SharedPreferences, db: AppDatabase): ProfileSettingSource {
        return ProfileSettingRemoteSource(api, pref, db)
    }

    @Provides
    @Local
    fun providesProfileSettingLocalSource(db: AppDatabase, pref: SharedPreferences): ProfileSettingSource {
        return ProfileSettingLocalSource(pref, db)
    }
}