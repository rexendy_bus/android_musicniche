package com.mymusicniche.features.profile.Setting

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.mymusicniche.R
import com.mymusicniche.features.profile.Setting.ProfileSetting.ProfileSettingActivity
import kotlinx.android.synthetic.main.activity_profile_setting_list.*

class SettingListActivity: AppCompatActivity() {

    var listItems = arrayListOf<String>()
    lateinit var toolbar: Toolbar;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_setting_list)

        toolbar = findViewById(R.id.toolbarView)
        setSupportActionBar(toolbar);

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()?.setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                onBackPressed()
            }

        })

        setListData()
    }

    private fun setListData() {
        listItems.add("Edit Profile")
        listItems.add("Profile Setting")

        loadListView()
    }

    private fun loadListView() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems)
        recyclerViewProfileSettingList.adapter = adapter

        recyclerViewProfileSettingList.setOnItemClickListener(object: AdapterView.OnItemClickListener{
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Log.d("positionpos", " : " + listItems.get(position))
                if (position == 1) {
                    val intent = Intent(this@SettingListActivity, ProfileSettingActivity::class.java)
                    startActivity(intent)
                }
            }

        })
    }
}