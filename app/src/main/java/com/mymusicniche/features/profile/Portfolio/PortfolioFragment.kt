package com.mymusicniche.features.profile.Portfolio

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mymusicniche.R
import com.mymusicniche.common.Constants
import com.mymusicniche.data.ProfileAvatar
import com.mymusicniche.data.User
import com.mymusicniche.features.profile.ProfileViewModel
import com.mymusicniche.features.profile.View.UserProfileViewModel
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_portfolios.*
import timber.log.Timber
import javax.inject.Inject

class PortfolioFragment: DaggerFragment() {

    @Inject lateinit var viewModel: UserProfileViewModel

    private var argUserID: String = "";

    companion object {
        fun newInstance(userID: String): PortfolioFragment {
            val fragment = PortfolioFragment()
            val args = Bundle()
            args.putString(Constants.Key.USER_UUID, userID)
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var portfolioAdapter: PortfolioAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val args = arguments
        argUserID = args?.getString(Constants.Key.USER_UUID).toString()
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, state: Bundle?): View {
        return inflater.inflate(R.layout.fragment_portfolios, parent, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerViewPortfolio.apply {
            val manager = GridLayoutManager(context,2, GridLayoutManager.VERTICAL, false)
            layoutManager = manager
            addItemDecoration(DividerItemDecoration(context, manager.orientation))
            portfolioAdapter = PortfolioAdapter(mutableListOf())
            adapter = portfolioAdapter
        }

        relPortfolioPbar.visibility = View.VISIBLE
        viewModel.getOtherUserProfile(argUserID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError({ Timber.e(it) })
                .subscribe(
                        { it.user?.let { it1 -> mapUser(it1) } }
                )
    }

    private fun mapUser(user: User) {
        relPortfolioPbar.visibility = View.GONE
        if (user.portfolios.size > 0) {
            user.portfolios.forEach {
                var display: DisplayablePortfolio;
                if(it.type.equals("portfolio")) {
                    display = DisplayablePortfolio(it.source.original!!, true)
                } else {
                    display = DisplayablePortfolio(it.source.playlist!!, false)
                }

                portfolioAdapter.add(user.portfolios.indexOf(it), display)
            }
        }
    }
}