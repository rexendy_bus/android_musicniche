package com.mymusicniche.features.profile

import com.mymusicniche.features.feeds.events.EventFeedsModule
import com.mymusicniche.features.feeds.events.EventsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ProfileFragmentProvider {

    @ContributesAndroidInjector(modules = [ProfileModule::class])
    abstract fun providesProfileFragmentFactory(): ProfileFragment
}