package com.mymusicniche.features.profile.Portfolio

import com.mymusicniche.features.profile.ProfileModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class PortfolioFragmentProvider {

    @ContributesAndroidInjector(modules = [ProfileModule::class])
    abstract fun providesPortfolioragmentFactory(): PortfolioFragment
}