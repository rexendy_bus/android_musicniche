package com.mymusicniche.features.profile.View

import com.mymusicniche.features.profile.ProfileModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UserProfileViewFragmentProvider {

    @ContributesAndroidInjector(modules = [ProfileModule::class])
    abstract fun providesUserProfileFragmentFactory(): UserProfileViewFragment
}