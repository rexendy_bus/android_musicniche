package com.mymusicniche.features.profile.Post


import android.app.AlertDialog
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.internal.Mutable
import com.mymusicniche.R
import com.mymusicniche.common.Constants
import com.mymusicniche.common.EndlessRecyclerViewScrollListener
import com.mymusicniche.features.profile.ProfileFragment
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_posts.*
import timber.log.Timber
import javax.inject.Inject


class PostFragment: DaggerFragment() {

    //https://github.com/codepath/android_guides/wiki/Endless-Scrolling-with-AdapterViews-and-RecyclerView
    @Inject
    lateinit var viewModel: PostViewModel

    private lateinit var postsAdapter: PostAdapter
    private lateinit var postList: Mutable<DisplayablePost>

    private var argUserID: String = "";

    private var pageCtr: Int = 1
    private val perPage: Int = 20
    private var isMine: Boolean = false


    private lateinit var scrollListener: EndlessRecyclerViewScrollListener;

    companion object {
        fun newInstance(useriD: String, isMine: Boolean): PostFragment {
            val fragment = PostFragment()
            val args = Bundle()
            args.putString(Constants.Key.USER_UUID, useriD)
            args.putBoolean(Constants.Key.IS_CURRENT_USER, isMine)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val args = arguments
        argUserID = args?.getString(Constants.Key.USER_UUID).toString()
        if (args != null) {
            isMine = args?.getBoolean(Constants.Key.IS_CURRENT_USER, false)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, state: Bundle?): View {
        return inflater.inflate(R.layout.fragment_posts, parent, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerViewPosts.apply {
            val manager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            layoutManager = manager
            addItemDecoration(DividerItemDecoration(context, manager.orientation))
            postsAdapter = PostAdapter(mutableListOf(), this@PostFragment, isMine)
            adapter = postsAdapter

            scrollListener = object: EndlessRecyclerViewScrollListener(manager) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                    pageCtr = page + 1
                    loadUserFeeds()
                }
            }

            recyclerViewPosts.addOnScrollListener(scrollListener)
        }

        loadUserFeeds()
    }

    override fun onStop() {
        super.onStop()
    }


    private fun loadUserFeeds() {
        relPostPbar.visibility = View.VISIBLE
        viewModel
                .loadUserFeeds(pageCtr, perPage, argUserID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showItems(it)
                }, { Timber.e(it) })
    }

    private fun showItems(items: MutableList<DisplayablePost>) {
        var ctr = 0

        items.forEach {
            if (it.userUID.equals(ProfileFragment.CURRENT_USER_ID) && !it.status.equals("Deleted")) {
                it.isCurrentUser = true
                postsAdapter.add(ctr, it)
                ctr++
            } else if (it.status.equals("Active")) {
                it.isCurrentUser = false
                postsAdapter.add(ctr, it)
                ctr++
            }
        }

        relPostPbar.visibility = View.GONE
    }

    public fun deletePost(posID: Int, position: Int) {

        val alertDilog = AlertDialog.Builder(activity).create()
        alertDilog.setTitle("Delete Post")
        alertDilog.setMessage("Are you sure you want to delete this post?")
        alertDilog.setCancelable(false)

        alertDilog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", {
            dialogInterface, i ->

            relPostPbar.visibility = View.VISIBLE

            viewModel.deletePost(posID)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        reloadPost(it, position)
                    }, { Timber.e(it) })
        })

        alertDilog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO", {
            dialogInterface, i ->
            alertDilog.dismiss()
        })

        alertDilog.show()
    }

    private fun reloadPost(status: Int, position: Int){
        relPostPbar.visibility = View.GONE
        if (status == 200) {
            // reload
            postsAdapter.postData.removeAt(position)
            postsAdapter.notifyItemRemoved(position)
        } else {
            deleteDialogError()
        }
    }

    private fun deleteDialogError(){
        val alertDilog = AlertDialog.Builder(activity).create()
        alertDilog.setTitle("Delete Post")
        alertDilog.setMessage("Unable to delete this post.")
        alertDilog.setCancelable(false)

        alertDilog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", {
            dialogInterface, i ->
           alertDilog.dismiss()
        })

        alertDilog.show()
    }

}