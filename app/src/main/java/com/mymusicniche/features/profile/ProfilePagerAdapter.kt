package com.mymusicniche.features.profile

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

import com.mymusicniche.features.profile.Portfolio.PortfolioFragment
import com.mymusicniche.features.profile.Post.PostFragment

class ProfilePagerAdapter(fm: FragmentManager, isJammer: Boolean, userID: String, isMine: Boolean) : FragmentStatePagerAdapter(fm) {

    val getIsJammer = isJammer
    val getUserID = userID
    val getIsMine = isMine

    override fun getItem(position: Int): Fragment {
        if (getIsJammer == true) {
            return PostFragment.newInstance(getUserID, getIsMine)
        } else {
            return if (position == 0) PortfolioFragment.newInstance(getUserID) else PostFragment.newInstance(getUserID, getIsMine)
        }

    }

    override fun getCount(): Int = if (getIsJammer == true) 1 else 2

    override fun getPageTitle(position: Int): CharSequence {
        if (getIsJammer == true) {
            return when (position) {
                0 -> "Post"
                else -> ""
            }
        } else {
            return when (position) {
                0 -> "Portfolio"
                1 -> "Post"
                else -> ""
            }
        }

    }
}