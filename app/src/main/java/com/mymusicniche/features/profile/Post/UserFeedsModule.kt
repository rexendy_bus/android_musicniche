package com.mymusicniche.features.profile.Post

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.data.source.EventFeedsSource
import com.mymusicniche.data.source.UserFeedsSource
import com.mymusicniche.data.source.local.AppDatabase
import com.mymusicniche.data.source.local.EventFeedsLocalSource
import com.mymusicniche.data.source.local.UserFeedsLocalSource
import com.mymusicniche.data.source.remote.EventFeedsRemoteSource
import com.mymusicniche.data.source.remote.UserFeedRemoteSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import dagger.Module
import dagger.Provides

/**
 * Created by rexenjeandy on 3/12/18.
 */
@Module
class UserFeedsModule {
    @Provides
    @Remote
    fun providesUserFeedsRemoteSource(api: ApiService, pref: SharedPreferences): UserFeedsSource {
        return UserFeedRemoteSource(api, pref)
    }

    @Provides
    @Local
    fun providesUserFeedsLocalSource(db: AppDatabase, pref: SharedPreferences): UserFeedsSource {
        return UserFeedsLocalSource(db, pref)
    }
}