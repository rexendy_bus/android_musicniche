package com.mymusicniche.features.profile.Setting.ProfileSetting

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import com.mymusicniche.R
import com.mymusicniche.common.ext.add
import dagger.android.support.DaggerAppCompatActivity

class ProfileSettingActivity: DaggerAppCompatActivity() {

    lateinit var toolbar: Toolbar;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_settings)

        toolbar = findViewById(R.id.toolbarView)
        setSupportActionBar(toolbar);

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()?.setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                onBackPressed()
            }

        })

        add (R.id.frameLayoutProfileSetting, ProfileSettingFragment.newInstance())
    }
}