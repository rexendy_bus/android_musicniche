package com.mymusicniche.features.profile

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.data.source.UserSource
import com.mymusicniche.data.source.local.AppDatabase
import com.mymusicniche.data.source.local.UserLocalSource
import com.mymusicniche.data.source.remote.UserRemoteSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import dagger.Module
import dagger.Provides

@Module
class ProfileModule {

    @Provides
    @Remote
    fun providesProfileRemoteSource(api: ApiService, pref: SharedPreferences, db: AppDatabase): UserSource {
        return UserRemoteSource(api, pref, db)
    }

    @Provides
    @Local
    fun providesProfileLocalSource(db: AppDatabase, pref: SharedPreferences): UserSource {
        return UserLocalSource(pref, db)
    }
}