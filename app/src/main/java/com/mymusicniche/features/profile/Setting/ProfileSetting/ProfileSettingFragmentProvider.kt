package com.mymusicniche.features.profile.Setting.ProfileSetting

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract  class ProfileSettingFragmentProvider {

    @ContributesAndroidInjector(modules = [ProfileSettingModule::class])
    abstract fun providesProfileSettingFragmentFactory(): ProfileSettingFragment
}