package com.mymusicniche.features.profile.Portfolio

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.mymusicniche.R
import com.mymusicniche.common.ext.load
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_portfolio.*
import kotlinx.android.synthetic.main.item_portfolio.view.*

/**
 * Created by lenovo on 3/8/2018.
 */
class PortfolioAdapter(private val portfolios: MutableList<DisplayablePortfolio>) : RecyclerView.Adapter<PortfolioAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(portfolios[position])

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_portfolio, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = portfolios.size

    fun add(position: Int, item: DisplayablePortfolio) {
        portfolios.add(position, item)
        notifyItemChanged(position)
    }

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(data: DisplayablePortfolio) {
            if (data.isImg == true) {
                containerView.imagePortfolio.visibility = View.VISIBLE
                containerView.imagePortfolio.load(data.path)
                containerView.imagePortfolioVid.visibility = View.GONE

                Glide.with(containerView.context)
                        .load(data.path)
                        .into(containerView.imagePortfolio)
            } else {
                containerView.imagePortfolio.visibility = View.GONE
                containerView.imagePortfolioVid.visibility = View.VISIBLE
                containerView.imagePortfolioVid.setVideoPath(data.path)
            }
        }
    }

}