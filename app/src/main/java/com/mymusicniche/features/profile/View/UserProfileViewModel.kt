package com.mymusicniche.features.profile.View

import com.mymusicniche.api.response.ProfileResponse
import com.mymusicniche.common.base.BaseViewModel
import com.mymusicniche.data.source.repository.UserRepository
import io.reactivex.Observable
import javax.inject.Inject

class UserProfileViewModel @Inject
constructor(private val userRepository: UserRepository): BaseViewModel() {
    override fun getLoadingIndicator(): Observable<Boolean> = loadingIndicator

    fun getOtherUserProfile(user_id: String): Observable<ProfileResponse> {
        return userRepository.getOtherUserProfile(user_id)
    }

}