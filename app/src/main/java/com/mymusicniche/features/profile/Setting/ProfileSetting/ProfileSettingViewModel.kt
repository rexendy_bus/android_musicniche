package com.mymusicniche.features.profile.Setting.ProfileSetting

import com.mymusicniche.api.response.BaseResponse
import com.mymusicniche.common.base.BaseViewModel
import com.mymusicniche.data.User
import com.mymusicniche.data.source.repository.ProfileSettingRepository
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class ProfileSettingViewModel @Inject
constructor(private val profileSettingRepository: ProfileSettingRepository): BaseViewModel(){
    override fun getLoadingIndicator(): Observable<Boolean> = loadingIndicator

    fun updateNotif(switch: String, module: String?): Single<BaseResponse> {
        return profileSettingRepository.updateNotification(switch, module)
    }

    fun updatePrivacy(visibility: String): Single<BaseResponse> {
        return profileSettingRepository.updatePrivacy(visibility)
    }

    fun getProfile(): Observable<User> {
        return profileSettingRepository.getProfile()
    }

}