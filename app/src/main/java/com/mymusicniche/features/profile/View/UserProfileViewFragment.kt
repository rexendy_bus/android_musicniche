package com.mymusicniche.features.profile.View

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.mymusicniche.R
import com.mymusicniche.api.response.ProfileResponse
import com.mymusicniche.common.Constants
import com.mymusicniche.features.profile.ProfileFragment
import com.mymusicniche.features.profile.ProfilePagerAdapter
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_user_view.*
import kotlinx.android.synthetic.main.in_profile_view_contact.*
import kotlinx.android.synthetic.main.in_profile_view_niche.*
import kotlinx.android.synthetic.main.in_tab_portfolio_post.*
import timber.log.Timber
import javax.inject.Inject

class UserProfileViewFragment: DaggerFragment() {

    @Inject lateinit var viewModel: UserProfileViewModel
    var isJammer: Boolean = false
    private var argUserID: String = "";

    companion object {
        fun newInstance(useriD: String): UserProfileViewFragment {
            val fragment = UserProfileViewFragment()
            val args = Bundle()
            args.putString(Constants.Key.USER_UUID, useriD)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val args = arguments
        argUserID = args?.getString(Constants.Key.USER_UUID).toString()
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, state: Bundle?): View? {
        var root: View = inflater.inflate(R.layout.fragment_user_view, parent, false)

        return root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpProfile()
    }

    fun setUpProfile() {
        relProfilePbar.visibility = View.VISIBLE
        viewModel.getOtherUserProfile(argUserID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError({ Timber.e(it) })
                .subscribe(
                        { mapUser(it) }
                )

    }

    private fun mapUser(profileRes: ProfileResponse) {
        var user_id = ""
        val user = profileRes.user
        if (user != null) {
            if (user.avatars.size > 0 && user.avatars.get(0).source?.original != null) {
                relProfilePbar.visibility = View.VISIBLE
                Glide.with(this).load(user.avatars.get(0).source?.original)
                        .listener(object: RequestListener<Drawable> {
                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                relProfilePbar.visibility = View.GONE
                                return false
                            }

                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                relProfilePbar.visibility = View.GONE
                                return false
                            }

                        })
                        .into(profileImgView)
            }

            if (user.backgrounds.size > 0 && user.backgrounds.get(0).source?.original != null) {
                relProfilePbar.visibility = View.VISIBLE
                Glide.with(this).load(user.backgrounds.get(0).source?.original)
                        .listener(object: RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                relProfilePbar.visibility = View.GONE
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                relProfilePbar.visibility = View.GONE
                                return false
                            }

                        })
                        .into(bgProfileImageView)
            }

            if (user.category?.name.equals("Jammer")) {
                isJammer = true
            }

            profileLinearContact.visibility = View.VISIBLE
            txtNameView.visibility = View.VISIBLE
            profLinMyNiche.visibility = View.VISIBLE
            profLinSkill.visibility = View.VISIBLE
            profLinGenre.visibility = View.VISIBLE
            profLinInfluences.visibility = View.VISIBLE
            profileLinearContact.visibility = View.VISIBLE
            profLinPhone.visibility = View.VISIBLE
            profLinWebsite.visibility = View.VISIBLE
            txtAddressView.visibility = View.VISIBLE
            txtDescriptionView.visibility = View.VISIBLE

            if (user.category?.name.equals("Jammer")) {
                profileLinearContact.visibility = View.GONE
            }

            txtCategoryView.text = user.category?.name
            if (user.category?.name.equals("Solo")) {
                txtNameView.text = user.firstName + " " + user.lastName
            } else {
                txtNameView.text = user.screenName
            }
            txtAddressView.text = user.address?.fullAddress
            txtDescriptionView.text = user.about
            inputSkillsProfileView.setText(user.skills.asSequence().joinToString { it.title })
            inputGenreProfileView.setText(user.genres.asSequence().joinToString { it.genre })
            inputMusicInfluencesProfileView.setText(user.influences.asSequence().joinToString { it.name })
            inputPhoneProfileView.setText(user.phone?.number)
            inputWebsiteProfileView.setText(user.website)

            checkViewVisility()

            user_id = user.uid
        }

        if (profileRes.isPrivate == false) {
            fragmentManager?.let {
                viewPagerProfile.adapter = ProfilePagerAdapter(it, isJammer, user_id, false)
                tabLayoutProfile.setupWithViewPager(viewPagerProfile)
            }
        } else {
            txtAddressView.text = "This profile is private."
        }

        btnReportProfile.visibility = View.VISIBLE
        if (ProfileFragment.CURRENT_USER_ID.equals(user_id)) {
            btnReportProfile.visibility = View.GONE
        }
        relProfilePbar.visibility = View.GONE
    }

    fun checkViewVisility() {
        if (txtNameView.length() == 0) {
            txtNameView.visibility = View.GONE
        }

        if (txtAddressView.length() == 0) {
            txtAddressView.visibility = View.GONE
        }

        if (txtDescriptionView.length() == 0) {
            txtDescriptionView.visibility = View.GONE
        }

        if (inputSkillsProfileView.length() == 0 && inputGenreProfileView.length() == 0 && inputMusicInfluencesProfileView.length() == 0) {
            profLinMyNiche.visibility = View.GONE
        }

        if (inputSkillsProfileView.length() == 0) {
            profLinSkill.visibility = View.GONE
        }

        if (inputGenreProfileView.length() == 0) {
            profLinGenre.visibility = View.GONE
        }

        if (inputMusicInfluencesProfileView.length() == 0) {
            profLinInfluences.visibility = View.GONE
        }

        if (inputPhoneProfileView.length() == 0 && inputWebsiteProfileView.length() == 0) {
            profileLinearContact.visibility = View.GONE
        }

        if (inputPhoneProfileView.length() == 0) {
            profLinPhone.visibility = View.GONE
        }

        if (inputWebsiteProfileView.length() == 0) {
            profLinWebsite.visibility = View.GONE
        }
    }
}