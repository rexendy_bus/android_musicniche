package com.mymusicniche.features.profile

import com.mymusicniche.common.base.BaseViewModel
import com.mymusicniche.data.User
import com.mymusicniche.data.source.repository.UserRepository
import io.reactivex.Observable
import javax.inject.Inject

class ProfileViewModel @Inject
constructor(private val userRepository: UserRepository): BaseViewModel() {
    override fun getLoadingIndicator(): Observable<Boolean> = loadingIndicator

    fun getProfile(): Observable<User> {
        return userRepository.getProfile()
    }

}