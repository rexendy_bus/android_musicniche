package com.mymusicniche.features.feeds.events

import com.mymusicniche.api.request.FeedsParams
import com.mymusicniche.data.source.repository.EventFeedsRepository
import io.reactivex.Observable
import javax.inject.Inject

class EventsViewModel @Inject
constructor(private val repository: EventFeedsRepository) {

    fun loadEventFeeds(latitude: Double, longitude: Double, page: Int): Observable<MutableList<DisplayableEvent>> {

        // check lat, lng, page if valid

        val params = FeedsParams(latitude, longitude, page)

        return repository.getEventFeeds(params)
            .flatMapIterable {
                it.results
            }
            .map {
                DisplayableEvent(
                    name = "${it.user.firstName} ${it.user.lastName}",
                    description = it.content,
                    minsAgo = it.createdAt,
                    image = "https://i.imgur.com/24BF9ga.jpg",
                    location = it.location.fullAddress,
                    datetime = it.eventDate,
                    commentsCount = it.comments.size,
                    likesCount = 0
                )
            }.toList().toObservable()
    }
}