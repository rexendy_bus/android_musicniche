package com.mymusicniche.features.feeds.discussions

import com.mymusicniche.api.request.FeedsParams
import com.mymusicniche.data.source.repository.DiscussionFeedsRepository
import io.reactivex.Observable
import javax.inject.Inject

class DiscussionsViewModel @Inject
constructor(private val repository: DiscussionFeedsRepository) {

    fun loadDiscussionFeeds(latitude: Double, longitude: Double, page: Int): Observable<MutableList<DisplayableDiscussion>> {

        // check lat, lng, page if valid

        val params = FeedsParams(latitude, longitude, page)

        return repository.getDiscussionFeeds(params)
            .flatMapIterable {
                it.results
            }
            .map {
                DisplayableDiscussion(
                    name = "${it.user.firstName} ${it.user.lastName}",
                    description = it.content,
                    minsAgo = it.createdAt,
                    image = "https://i.imgur.com/hLWOVL2.jpg",
                    location = it.location.fullAddress,
                    commentsCount = it.comments.size,
                    likesCount = 0,
                    id = it.user.uid
                )
            }.toList().toObservable()
    }
}
