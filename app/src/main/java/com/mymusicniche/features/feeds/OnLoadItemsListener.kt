package com.mymusicniche.features.feeds

interface OnLoadItemsListener {
    fun onLoadItems()
}