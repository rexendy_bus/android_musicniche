package com.mymusicniche.features.feeds.events

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.data.source.EventFeedsSource
import com.mymusicniche.data.source.local.AppDatabase
import com.mymusicniche.data.source.local.EventFeedsLocalSource
import com.mymusicniche.data.source.remote.EventFeedsRemoteSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import dagger.Module
import dagger.Provides

@Module
class EventFeedsModule {

    @Provides
    @Remote
    fun providesEventFeedsRemoteSource(api: ApiService, pref: SharedPreferences): EventFeedsSource {
        return EventFeedsRemoteSource(api, pref)
    }

    @Provides
    @Local
    fun providesEventFeedsLocalSource(db: AppDatabase, pref: SharedPreferences): EventFeedsSource {
        return EventFeedsLocalSource(db, pref)
    }
}