package com.mymusicniche.features.feeds.discussions

data class DisplayableDiscussion (
    val name: String,
    val description: String,
    val minsAgo: String,
    val image: String,
    val location: String,
    val commentsCount: Int,
    val likesCount: Int,
    val id: String
)