package com.mymusicniche.features.feeds

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.mymusicniche.features.feeds.discussions.DiscussionsFragment
import com.mymusicniche.features.feeds.events.EventsFragment

class HomePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) EventsFragment.newInstance() else DiscussionsFragment.newInstance()
    }

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Events"
            1 -> "Discussions"
            else -> ""
        }
    }
}