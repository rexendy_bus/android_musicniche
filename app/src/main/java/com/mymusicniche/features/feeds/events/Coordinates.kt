package com.mymusicniche.features.feeds.events

class Coordinates(val lat: Double, val lng: Double)