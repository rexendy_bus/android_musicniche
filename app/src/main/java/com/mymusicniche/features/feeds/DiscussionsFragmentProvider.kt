package com.mymusicniche.features.feeds

import com.mymusicniche.features.feeds.discussions.DiscussionFeedsModule
import com.mymusicniche.features.feeds.discussions.DiscussionsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DiscussionsFragmentProvider {

    @ContributesAndroidInjector(modules = [DiscussionFeedsModule::class])
    abstract fun providesDiscussionsFragmentFactory(): DiscussionsFragment
}