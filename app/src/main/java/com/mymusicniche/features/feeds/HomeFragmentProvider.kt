package com.mymusicniche.features.feeds

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeFragmentProvider {

    @ContributesAndroidInjector
    abstract fun providesHomeFragmentFactory(): HomeFragment
}