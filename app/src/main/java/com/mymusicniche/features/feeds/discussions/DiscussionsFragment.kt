package com.mymusicniche.features.feeds.discussions

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearLayoutManager.VERTICAL
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mymusicniche.R
import com.mymusicniche.common.Constants
import com.mymusicniche.features.feeds.OnLoadItemsListener
import com.mymusicniche.features.feeds.OnProfileClickListener
import com.mymusicniche.features.profile.View.UserProfileViewActivity
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_discussions.*
import timber.log.Timber
import javax.inject.Inject

class DiscussionsFragment : DaggerFragment(), OnLoadItemsListener, OnProfileClickListener {

    companion object {
        fun newInstance(): DiscussionsFragment {
            val fragment = DiscussionsFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var viewModel: DiscussionsViewModel

    private lateinit var disposables: CompositeDisposable

    private lateinit var discussionsAdapter: DiscussionsAdapter

    private var page = 1

    private val pageStream = PublishSubject.create<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        disposables = CompositeDisposable()
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, state: Bundle?): View {
        return inflater.inflate(R.layout.fragment_discussions, parent, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        discussionsAdapter = DiscussionsAdapter(mutableListOf(), this, this)

        recyclerView.apply {
            val manager = LinearLayoutManager(context, VERTICAL, false)
            layoutManager = manager
            addItemDecoration(DividerItemDecoration(context, manager.orientation))
            adapter = discussionsAdapter
        }

        loadDiscussions()
        pageStream.onNext(page)
    }

    override fun onLoadItems() = pageStream.onNext(page)

    override fun onStart() {
        super.onStart()
        Timber.d("onStart")
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    private fun loadDiscussions() {
        disposables.add(
            pageStream.observeOn(Schedulers.io())
                .concatMap {
                    viewModel.loadDiscussionFeeds(10.12345, 123.12345, page)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    Timber.d("size: ${it.size}")
                }
                .doOnNext {
                    showItemsAndIncrementPage(it)
                }
                .subscribe())
    }

    private fun showItemsAndIncrementPage(items: MutableList<DisplayableDiscussion>) {
        items.forEach {
            discussionsAdapter.add(items.indexOf(it), it)
        }
        page += 1
    }

    override fun onProfileClick(userId: String) {
        val intent = Intent(activity, UserProfileViewActivity::class.java)
        intent.putExtra(Constants.Key.USER_UUID, userId)
        startActivity(intent)
    }

    public fun goToProfile(user_id: String) {
        val intent = Intent(activity, UserProfileViewActivity::class.java)
        intent.putExtra(Constants.Key.USER_UUID, user_id)
        startActivity(intent)
    }
}