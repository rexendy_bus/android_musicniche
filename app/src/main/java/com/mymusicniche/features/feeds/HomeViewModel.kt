package com.mymusicniche.features.feeds

import com.mymusicniche.common.base.BaseViewModel
import com.mymusicniche.data.User
import com.mymusicniche.data.source.repository.LoginRepository
import io.reactivex.Observable
import javax.inject.Inject

class HomeViewModel @Inject
constructor(private val repository: LoginRepository) : BaseViewModel() {

    fun getProfile(): Observable<User> {

        // TODO checking here
        return repository.getProfile()
                .doOnNext { loadingIndicator.onNext(false) }
                .doFinally { loadingIndicator.onNext(false) }
    }

    override fun getLoadingIndicator(): Observable<Boolean> = loadingIndicator
}