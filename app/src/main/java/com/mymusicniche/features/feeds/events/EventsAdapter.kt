package com.mymusicniche.features.feeds.events

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mymusicniche.R
import com.mymusicniche.common.ext.load
import com.mymusicniche.features.feeds.OnLoadItemsListener
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_events.*

class EventsAdapter (
    private val events: MutableList<DisplayableEvent>,
    private val listener: OnLoadItemsListener?
) : RecyclerView.Adapter<EventsAdapter.ViewHolder>() {

    override fun getItemCount(): Int = events.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(events[position])
        listener?.let {
            if (position == events.lastIndex) it.onLoadItems()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_events, parent, false)
        return ViewHolder(view)
    }

    fun add(position: Int, item: DisplayableEvent) {
        events.add(position, item)
        notifyItemChanged(position)
    }

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(event: DisplayableEvent) {
            nameView.text = event.name
            descriptionView.text = event.description
            minsAgoView.text = event.minsAgo
            locationView.text = event.location
            datetimeView.text = event.datetime
            eventImgView.load(event.image)
        }
    }
}