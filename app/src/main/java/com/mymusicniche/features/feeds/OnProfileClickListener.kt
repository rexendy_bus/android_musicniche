package com.mymusicniche.features.feeds

interface OnProfileClickListener {
    fun onProfileClick(userId: String)
}