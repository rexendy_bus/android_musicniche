package com.mymusicniche.features.feeds

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

object RequestPermissionStream {

    private val permissionStream = PublishSubject.create<Boolean>()

    fun publish(isGranted: Boolean) = permissionStream.onNext(isGranted)

    fun subscribe(): Observable<Boolean> = permissionStream
}