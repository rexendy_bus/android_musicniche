package com.mymusicniche.features.feeds.events

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.os.IBinder
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

class EventsService : Service(), ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

    companion object {
        private const val TEN_MINS = 600000L
        private const val TWO_SECS = 2000L
    }

    private lateinit var googleApiClient: GoogleApiClient

    private lateinit var locationRequest: LocationRequest

    private val disposable = CompositeDisposable()

    override fun onBind(p0: Intent?): IBinder? = null

    override fun onCreate() {
        super.onCreate()
        buildGoogleApiClient()
        createLocationRequest()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        googleApiClient.connect()
        return super.onStartCommand(intent, flags, startId)
    }

    @Suppress("DEPRECATION")
    @SuppressLint("MissingPermission")
    override fun onConnected(bundle: Bundle?) {
        val location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient)
        location?.let {
            LocationChangedEvent.publish(Coordinates(location.latitude, location.longitude))
            return@onConnected
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this)
    }

    override fun onLocationChanged(location: Location?) {
        location?.let {
            LocationChangedEvent.publish(Coordinates(location.latitude, location.longitude))
        }
    }

    override fun onConnectionSuspended(status: Int) {
        Timber.w("connection suspended: $status")
    }

    override fun onConnectionFailed(result: ConnectionResult) {
        Timber.w("connection failed: ${result.errorCode} ${result.errorMessage}")
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest()
            .setInterval(TEN_MINS) // mins
            .setFastestInterval(TWO_SECS)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
    }

    private fun buildGoogleApiClient() {
        googleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
    }
}