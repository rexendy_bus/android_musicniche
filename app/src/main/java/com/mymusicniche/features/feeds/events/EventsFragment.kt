package com.mymusicniche.features.feeds.events

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.mymusicniche.R
import com.mymusicniche.features.feeds.HomeActivity.Companion.RC_PERMISSION
import com.mymusicniche.features.feeds.OnLoadItemsListener
import com.mymusicniche.features.feeds.RequestPermissionStream
import dagger.android.support.DaggerFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_events.*
import timber.log.Timber
import javax.inject.Inject

class EventsFragment : DaggerFragment(), OnLoadItemsListener {

    companion object {
        fun newInstance(): EventsFragment {
            val fragment = EventsFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var viewModel: EventsViewModel

    private lateinit var disposables: CompositeDisposable

    private lateinit var locationService: Intent

    private lateinit var eventsAdapter: EventsAdapter

    private var statusCode: Int = 0

    private var latitude = 0.0

    private var longitude = 0.0

    private var page = 1

    private val pageStream = PublishSubject.create<Int>()

    private val gpsIsEnabledStream = BehaviorSubject.create<Boolean>()

    private val coordinatesStream = BehaviorSubject.create<Coordinates>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!isPlayServicesAvailable) {
            showPlayServicesDialog(statusCode)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, state: Bundle?): View {
        return inflater.inflate(R.layout.fragment_events, parent, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        disposables = CompositeDisposable()
        eventsAdapter = EventsAdapter(mutableListOf(), this)

        recyclerView.apply {
            val manager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            layoutManager = manager
            addItemDecoration(DividerItemDecoration(context, manager.orientation))
            adapter = eventsAdapter
        }

        disposables.add(
            pageStream.observeOn(Schedulers.io())
                .concatMap {
                    viewModel.loadEventFeeds(latitude, longitude, page)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    showItemsAndIncrementPage(it)
                }
                .subscribe()
        )
    }

    override fun onLoadItems() = pageStream.onNext(page)

    override fun onStart() {
        super.onStart()
        locationService = Intent(context, EventsService::class.java)
        checkLocationPermission()
        bind()
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    private fun checkLocationPermission() {
        val p = ContextCompat.checkSelfPermission(checkNotNull(context), ACCESS_FINE_LOCATION)
        if (p == PERMISSION_GRANTED) checkGpsEnabled() else requestLocationPermission()
    }

    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(checkNotNull(activity), ACCESS_FINE_LOCATION)) {
            // show explanation?
        } else {
            ActivityCompat.requestPermissions(checkNotNull(activity), arrayOf(ACCESS_FINE_LOCATION), RC_PERMISSION)
            // do not show explanation
            // just grant permission already
        }
    }

    private fun checkGpsEnabled() {
        val lm = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        gpsIsEnabledStream.onNext(lm.isProviderEnabled(LocationManager.GPS_PROVIDER))
    }

    private fun bind() {
        disposables.add(gpsIsEnabledStream.observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                Toast.makeText(activity, "Loading your feeds", Toast.LENGTH_SHORT).show()
            }
            .subscribe({
                if (it) startLocationUpdate() else showLocationSettingsAlert()
            })
        )

        disposables.add(LocationChangedEvent.subscribe()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                coordinatesStream.onNext(it)
                coordinatesStream.onComplete()
            })
        )

        disposables.add(coordinatesStream.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                latitude = it.lat
                longitude = it.lng
            }
            .doOnComplete { stopLocationUpdate() }
            .subscribe({
                pageStream.onNext(page)
            })
        )

        disposables.add(
            RequestPermissionStream.subscribe()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it) checkGpsEnabled()
                })
        )
    }

    private fun showItemsAndIncrementPage(items: MutableList<DisplayableEvent>) {
        items.forEach {
            eventsAdapter.add(items.indexOf(it), it)
        }
        page += 1
    }

    private fun startLocationUpdate() {
        context?.startService(locationService)
    }

    private fun stopLocationUpdate() {
        context?.stopService(locationService)
    }

    private fun showLocationSettingsAlert() {
        with(AlertDialog.Builder(context)) {
            setTitle(getString(R.string.gps_enable))
            setMessage(getString(R.string.gps_enable_msg))
            setPositiveButton(getString(R.string.show_me), { _, _ ->
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            })
            setCancelable(false)
            create()
        }.show()
    }

    private fun showPlayServicesDialog(statusCode: Int) {
        val availability = GoogleApiAvailability.getInstance()
        if (availability.isUserResolvableError(statusCode)) {
            availability.getErrorDialog(activity, statusCode, 0).show()
        }
    }

    private val isPlayServicesAvailable: Boolean
    get() {
        val googleApi = GoogleApiAvailability.getInstance()
        val statusCode = googleApi.isGooglePlayServicesAvailable(context)
        return when (statusCode) {
            ConnectionResult.SUCCESS -> {
                Timber.d("Google Play Services available")
                true
            }

            else -> {
                Timber.d("Google Play Services NOT available")
                this.statusCode = statusCode
                false
            }
        }
    }
}
