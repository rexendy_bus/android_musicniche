package com.mymusicniche.features.feeds.events

class DisplayableEvent(
    val name: String,
    val description: String,
    val minsAgo: String,
    val image: String,
    val location: String,
    val datetime: String,
    val commentsCount: Int,
    val likesCount: Int
) {
    override fun toString(): String = "name:$name,created_at:$minsAgo"
}