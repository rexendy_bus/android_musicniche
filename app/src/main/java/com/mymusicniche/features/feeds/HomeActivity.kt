package com.mymusicniche.features.feeds

import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Bundle
import android.support.design.widget.BottomNavigationView.OnNavigationItemSelectedListener
import android.view.MenuItem
import com.mymusicniche.R
import com.mymusicniche.common.Constants
import com.mymusicniche.common.ext.add
import com.mymusicniche.common.ext.get
import com.mymusicniche.common.ext.replace
import com.mymusicniche.common.ext.toBoolean
import com.mymusicniche.data.User
import com.mymusicniche.features.create_profile.CreateProfileActivity
import com.mymusicniche.features.login.LoginActivity
import com.mymusicniche.features.map.MapsFragment
import com.mymusicniche.features.profile.ProfileFragment
import com.mymusicniche.features.search.SearchFragment
import com.mymusicniche.features.walkthrough.WalkthroughActivity
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_home.*
import timber.log.Timber
import javax.inject.Inject

class HomeActivity : DaggerAppCompatActivity(), OnNavigationItemSelectedListener {

    @Inject
    lateinit var pref: SharedPreferences

    companion object {
        const val RC_PERMISSION = 10
    }

    @Inject
    lateinit var viewModel: HomeViewModel

    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        navigation.setOnNavigationItemSelectedListener(this)
        add(R.id.contentLayout, HomeFragment.newInstance())
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.navigation_home -> {
                replace(R.id.contentLayout, HomeFragment.newInstance())
                true
            }
            R.id.navigation_map -> {
                replace(R.id.contentLayout, MapsFragment.newInstance())
                true
            }
            R.id.navigation_search -> {
                replace(R.id.contentLayout, SearchFragment.newInstance())
                true
            }
            R.id.navigation_profile -> {
                replace(R.id.contentLayout, ProfileFragment.newInstance())
                true
            }
            else -> false
        }
    }

    override fun onStart() {
        super.onStart()
        checkSavedTokens()
        checkUserState()
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    private fun checkUserState() {

        // check if user accepted terms

        disposables.add(viewModel.getProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    checkTerms(it)
                }, { Timber.e(it) })

        )

    }

    private fun checkSavedTokens() {
        // should we clear db?
        val accessToken = pref.get<String>(Constants.Key.ACCESS_TOKEN)
        val refreshToken = pref.get<String>(Constants.Key.REFRESH_TOKEN)
        if (accessToken.isEmpty() || refreshToken.isEmpty()) {
            pref.edit().clear().apply()
            startActivity(Intent(this, LoginActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            })
            finish()
            return
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == RC_PERMISSION) {
            RequestPermissionStream.publish(
                grantResults.isNotEmpty() && grantResults[0] == PERMISSION_GRANTED
            )
        }
    }

    private fun checkTerms(user: User) {


        if (user.firstName.isEmpty()) {
            pref.edit().clear().apply()
            startActivity(Intent(this, LoginActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            })
            finish()
        }
        else if (user.acceptedTerms.toBoolean()) {
            // TODO change when profile module is done
            if(user.addressId < 1) {
                showCreateProfileActivity(user)
            }
        } else gotoWalktroughScreen(user)
    }

    private fun gotoWalktroughScreen(user: User) {
        startActivity(Intent(this, WalkthroughActivity::class.java).apply {
            putExtra(CreateProfileActivity.EXTRA_USER, user)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        })
        finish()
    }

    private fun showCreateProfileActivity(user: User) {
        val intent = Intent(this@HomeActivity, CreateProfileActivity::class.java)
        intent.putExtra(CreateProfileActivity.EXTRA_USER, user)
        startActivity(intent)
        finish()
    }

}
