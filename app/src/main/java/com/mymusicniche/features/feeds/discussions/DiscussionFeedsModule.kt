package com.mymusicniche.features.feeds.discussions

import android.content.SharedPreferences
import com.mymusicniche.api.ApiService
import com.mymusicniche.data.source.DiscussionFeedsSource
import com.mymusicniche.data.source.local.AppDatabase
import com.mymusicniche.data.source.local.DiscussionFeedsLocalSource
import com.mymusicniche.data.source.remote.DiscussionFeedsRemoteSource
import com.mymusicniche.di.scopes.Local
import com.mymusicniche.di.scopes.Remote
import dagger.Module
import dagger.Provides

@Module
class DiscussionFeedsModule {

    @Provides
    @Remote
    fun providesDiscussionFeedsRemoteSource(api: ApiService, pref: SharedPreferences): DiscussionFeedsSource {
        return DiscussionFeedsRemoteSource(api, pref)
    }

    @Provides
    @Local
    fun providesDiscussionFeedsLocalSource(db: AppDatabase, pref: SharedPreferences): DiscussionFeedsSource {
        return DiscussionFeedsLocalSource(db, pref)
    }
}