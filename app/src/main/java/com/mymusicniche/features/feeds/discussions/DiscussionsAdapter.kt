package com.mymusicniche.features.feeds.discussions

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mymusicniche.R
import com.mymusicniche.common.ext.load
import com.mymusicniche.features.feeds.OnLoadItemsListener
import com.mymusicniche.features.feeds.OnProfileClickListener
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_discussions.*

class DiscussionsAdapter (
    private val discussions: MutableList<DisplayableDiscussion>,
    private val onLoadItemsListener: OnLoadItemsListener?,
    private val onProfileClickListener: OnProfileClickListener?
) : RecyclerView.Adapter<DiscussionsAdapter.ViewHolder>() {

    override fun getItemCount(): Int = discussions.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(discussions[position])
        onLoadItemsListener?.let {
            if (position == discussions.lastIndex) it.onLoadItems()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_discussions, parent, false)
        return ViewHolder(view)
    }

    fun add(position: Int, item: DisplayableDiscussion) {
        discussions.add(position, item)
        notifyItemChanged(position)
    }

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(discussion: DisplayableDiscussion) {
            nameView.text = discussion.name
            descriptionView.text = discussion.description
            minsAgoView.text = discussion.minsAgo
            locationView.text = discussion.location
            eventImgView.load(discussion.image)

            nameView.setOnClickListener({
                onProfileClickListener?.onProfileClick(discussion.id)
            })
        }
    }
}