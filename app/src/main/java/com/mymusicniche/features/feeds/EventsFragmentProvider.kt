package com.mymusicniche.features.feeds

import com.mymusicniche.features.feeds.events.EventFeedsModule
import com.mymusicniche.features.feeds.events.EventsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class EventsFragmentProvider {

    @ContributesAndroidInjector(modules = [EventFeedsModule::class])
    abstract fun providesEventsFragmentFactory(): EventsFragment
}