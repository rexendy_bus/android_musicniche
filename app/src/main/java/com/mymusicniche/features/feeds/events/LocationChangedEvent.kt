package com.mymusicniche.features.feeds.events

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

object LocationChangedEvent {

    private val subject = PublishSubject.create<Coordinates>()

    fun publish(coordinates: Coordinates) = subject.onNext(coordinates)

    fun subscribe(): Observable<Coordinates> = subject
}