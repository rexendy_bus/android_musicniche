package com.mymusicniche.features.walkthrough

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

import java.util.ArrayList

class ViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    private val mFragments: MutableList<Tab> = ArrayList()

    fun addFragment(id: Int, fragment: Fragment, title: String) {
        mFragments.add(Tab(id, fragment, title))
    }

    fun addFragment(id: Int, fragment: Fragment) {
        addFragment(id, fragment, "")
    }

    override fun getItem(position: Int): Fragment? {
        return mFragments[position].fragment
    }

    override fun getCount(): Int {
        return mFragments.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragments[position].title
    }
}