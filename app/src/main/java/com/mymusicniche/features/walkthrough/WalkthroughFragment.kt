package com.mymusicniche.features.walkthrough

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class WalkthroughFragment : Fragment() {

    companion object {
        fun newInstance(layout: Int): WalkthroughFragment {
            val fragment = WalkthroughFragment()
            val bundle = Bundle()
            bundle.putInt("layout", layout)
            fragment.arguments = bundle
            return fragment
        }


    }

    private var layoutResourceId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            layoutResourceId = it.getInt("layout", 0)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutResourceId, container, false)
    }

}