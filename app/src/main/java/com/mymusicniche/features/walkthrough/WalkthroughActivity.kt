package com.mymusicniche.features.walkthrough

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import com.mymusicniche.R
import com.mymusicniche.common.ext.invisible
import com.mymusicniche.common.ext.show
import com.mymusicniche.data.User
import com.mymusicniche.features.create_profile.CreateProfileActivity
import com.mymusicniche.features.create_profile.CreateProfileActivity.Companion.EXTRA_FIRSTNAME
import com.mymusicniche.features.create_profile.CreateProfileActivity.Companion.EXTRA_USER
import com.mymusicniche.features.create_profile.CreateProfileActivity.Companion.EXTRA_LASTNAME
import com.mymusicniche.features.create_profile.CreateProfileActivity.Companion.EXTRA_LOCATION
import com.mymusicniche.features.tos.TOSActivity
import kotlinx.android.synthetic.main.activity_walkthrough.*

class WalkthroughActivity : AppCompatActivity() {

    private lateinit var user: User
    private var fName: String? = null
    private var lName: String? = null
    private var location: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_walkthrough)
        user = intent.getParcelableExtra(EXTRA_USER)
        fName = intent.getStringExtra(EXTRA_FIRSTNAME)
        lName = intent.getStringExtra(CreateProfileActivity.EXTRA_LASTNAME)
        location = intent.getStringExtra(CreateProfileActivity.EXTRA_LOCATION)
        initialize()
    }

    private fun initialize() {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(0, WalkthroughFragment.newInstance(R.layout.fragment_first_walkthrough))
        adapter.addFragment(1, WalkthroughFragment.newInstance(R.layout.fragment_second_walkthrough))
        adapter.addFragment(2, WalkthroughFragment.newInstance(R.layout.fragment_third_walkthrough))
        adapter.addFragment(3, WalkthroughFragment.newInstance(R.layout.fragment_fourth_walkthrough))
        adapter.addFragment(4, WalkthroughFragment.newInstance(R.layout.fragment_fifth_walkthrough))
        viewPager.adapter = adapter
        circleIndicator.setViewPager(viewPager)
        viewPager.addOnPageChangeListener(onPageChangeListener)

        tvContinue.setOnClickListener {
            if (TextUtils.equals(user.acceptedTerms, "yes")) {
                showCreateProfileActivity()
            } else {
                showTOSActivity()
            }
        }
    }

    private val onPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        }

        override fun onPageSelected(position: Int) {
            if (position < 4) tvContinue.invisible() else tvContinue.show()
        }

        override fun onPageScrollStateChanged(state: Int) {
        }
    }

    private fun showTOSActivity() {
        val intent = Intent(this, TOSActivity::class.java)
        intent.putExtra(EXTRA_USER, user)
        intent.putExtra(EXTRA_FIRSTNAME, fName)
        intent.putExtra(EXTRA_LASTNAME, lName)
        intent.putExtra(EXTRA_LOCATION, location)
        startActivity(intent)
        finish()
    }

    private fun showCreateProfileActivity() {
        val intent = Intent(this, CreateProfileActivity::class.java)
        intent.putExtra(EXTRA_USER, user)
        intent.putExtra(EXTRA_FIRSTNAME, fName)
        intent.putExtra(EXTRA_LASTNAME, lName)
        intent.putExtra(EXTRA_LOCATION, location)
        startActivity(intent)
        finish()
    }
}